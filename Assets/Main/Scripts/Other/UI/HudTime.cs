﻿using UnityEngine.UI;
using Orgil;

public class HudTime : Mb {
    public Text txt;
    public void Data(float data) {
        if (txt)
            txt.text = data.TimeS();
    }
}