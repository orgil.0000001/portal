﻿using UnityEngine;
using UnityEngine.UI;
using Orgil;

public enum StageState { None, Selected, Done }
public class Stage : Mb {
    public Text levelTxt;
    public StageState state = StageState.None;
    public void SetState(StageState state) {
        if (this.state != state) {
            this.state = state;
            if (state == StageState.Selected)
                go.Child<Image>(0).color = C._orange;
            else if (state == StageState.Done)
                go.Child<Image>(0).color = C.I;
        }
    }
}