﻿using UnityEngine.UI;
using Orgil;

public class FormatTxt : Mb {
    public Text txt;
    public string format = "(data)";
    public void Data(float data) {
        if (txt)
            txt.text = format.Fmt("(data)", data);
    }
}