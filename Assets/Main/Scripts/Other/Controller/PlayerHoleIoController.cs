﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

[RequireComponent(typeof(Rigidbody))]
public class PlayerHoleIoController : Mb {
    public float spd = 10, r = 100;

    float dis;

    void Start() {
        rb.NoG();
        rb.Con(false, true, false, true, true, true);
    }

    void Update() {
        if (IsPlaying) {
            if (IsMbD)
                mp = Mp;
            if (IsMb) {
                dis = V3.Dis(Mp, mp);
                if (dis > r)
                    mp = V3.Move(Mp, mp, r);
                Tr = Q.Y(Ang.LookF(mp, Mp));
                rb.V(F * M.C01(dis / r) * spd);
            }
            if (IsMbU)
                rb.V0();
        } else
            rb.V0();
    }
}