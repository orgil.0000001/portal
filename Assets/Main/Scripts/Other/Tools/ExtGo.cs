using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Orgil {
    public static class ExtGo {
        public static T Gc<T>(this GameObject a) { return a.GetComponent<T>(); }
        public static T Gc<T>(this GameObject a, GcTp tp) where T : Component { T t = a.GetComponent<T>(); return tp == GcTp.Get ? t : tp == GcTp.Add ? a.AddComponent<T>() : t == null ? a.AddComponent<T>() : t; }
        public static Text Txt(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Text>(tp); }
        public static Button Btn(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Button>(tp); }
        public static Image Img(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Image>(tp); }
        public static Toggle Tgl(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Toggle>(tp); }
        public static Slider Sld(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Slider>(tp); }
        public static Dropdown Drp(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Dropdown>(tp); }
        public static InputField Inp(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<InputField>(tp); }
        public static ColorPicker Cp(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<ColorPicker>(tp); }
        public static TextMesh Tm(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<TextMesh>(tp); }
        public static TextMeshPro Tmp(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<TextMeshPro>(tp); }
        public static Rigidbody Rb(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Rigidbody>(tp); }
        public static Rigidbody2D Rb2(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Rigidbody2D>(tp); }
        public static Renderer Ren(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Renderer>(tp); }
        public static MeshFilter Mf(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<MeshFilter>(tp); }
        public static MeshRenderer Mr(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<MeshRenderer>(tp); }
        public static Collider Col(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Collider>(tp); }
        public static BoxCollider Bc(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<BoxCollider>(tp); }
        public static SphereCollider Sc(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<SphereCollider>(tp); }
        public static CapsuleCollider Cc(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<CapsuleCollider>(tp); }
        public static MeshCollider Mc(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<MeshCollider>(tp); }
        public static WheelCollider Wc(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<WheelCollider>(tp); }
        public static ParticleSystem Ps(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<ParticleSystem>(tp); }
        public static ParticleSystemRenderer Psr(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<ParticleSystemRenderer>(tp); }
        public static TrailRenderer Trl(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<TrailRenderer>(tp); }
        public static Animator An(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Animator>(tp); }
        public static RectTransform Rt(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<RectTransform>(tp); }
        public static LineRenderer Lr(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<LineRenderer>(tp); }
        public static Shadow Shadow(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Shadow>(tp); }
        public static Outline Outline(this GameObject a, GcTp tp = GcTp.Get) { return a.Gc<Outline>(tp); }
        public static void Active(this GameObject a, bool active) { a.SetActive(active); }
        public static void Show(this GameObject a) { a.Active(true); }
        public static void Hide(this GameObject a) { a.Active(false); }
        public static bool IsActive(this GameObject a) { return a && a.activeSelf; }
        public static bool Is<T>(this GameObject a) { return a.Gc<T>() != null; }
        public static T Ac<T>(this GameObject a) where T : Component { return a.AddComponent<T>(); }
        public static T Gcic<T>(this GameObject a) { return a.GetComponentInChildren<T>(); }
        public static T Gcip<T>(this GameObject a) { return a.GetComponentInParent<T>(); }
        public static T[] Gcs<T>(this GameObject a) { return a.GetComponents<T>(); }
        public static T[] Gcsic<T>(this GameObject a) { return a.GetComponentsInChildren<T>(); }
        public static T[] Gcsip<T>(this GameObject a) { return a.GetComponentsInParent<T>(); }
        public static List<T> Gca<T>(this GameObject a) { return a.Is<T>() ? a.Gcs<T>().List().Add(a.Gcsic<T>()) : a.Gcsic<T>().List(); }
        public static List<Transform> Childs(this GameObject a) {
            List<Transform> lis = new List<Transform>();
            for (int i = 0; i < a.transform.childCount; i++)
                lis.Add(a.Child(i));
            return lis;
        }
        public static List<GameObject> ChildGos(this GameObject a) {
            List<GameObject> lis = new List<GameObject>();
            for (int i = 0; i < a.transform.childCount; i++)
                lis.Add(a.ChildGo(i));
            return lis;
        }
        public static List<T> Childs<T>(this GameObject a) {
            List<T> lis = new List<T>();
            for (int i = 0; i < a.transform.childCount; i++)
                if (a.ChildGo(i).Is<T>())
                    lis.Add(a.Child<T>(i));
            return lis;
        }
        public static void DstChilds<T>(this GameObject a, int sta = 0, int end = -1) where T : Object {
            List<T> lis = a.Childs<T>();
            for (int i = sta, n = end < 0 ? lis.Count : end; i < n; i++)
                Mb.Dst(lis[i]);
        }
        public static void DstChilds(this GameObject a, int sta = 0, int end = -1) {
            List<GameObject> lis = a.Childs<GameObject>();
            for (int i = sta, n = end < 0 ? lis.Count : end; i < n; i++)
                Mb.Dst(lis[i]);
        }
        public static Color GetCol(this GameObject a, Color col = default) {
            Image img = a.Img(); if (img) return img.color;
            Text txt = a.Txt(); if (txt) return txt.color;
            TextMesh tm = a.Tm(); if (tm) return tm.color;
            TextMeshPro tmp = a.Tmp(); if (tmp) return tmp.color;
            return col;
        }
        public static void SetCol(this GameObject a, Color col) {
            Image img = a.Img(); if (img) { img.color = col; return; }
            Text txt = a.Txt(); if (txt) { txt.color = col; return; }
            TextMesh tm = a.Tm(); if (tm) { tm.color = col; return; }
            TextMeshPro tmp = a.Tmp(); if (tmp) { tmp.color = col; return; }
        }
        public static void ColA(this GameObject a, float alpha) {
            Image img = a.Img(); if (img) { img.color = img.color.A(alpha); return; }
            Text txt = a.Txt(); if (txt) { txt.color = txt.color.A(alpha); return; }
            TextMesh tm = a.Tm(); if (tm) { tm.color = tm.color.A(alpha); return; }
            TextMeshPro tmp = a.Tmp(); if (tmp) { tmp.color = tmp.color.A(alpha); return; }
        }
        public static string GetText(this GameObject a, string text = "") {
            Text txt = a.Txt(); if (txt) return txt.text;
            TextMesh tm = a.Tm(); if (tm) return tm.text;
            TextMeshPro tmp = a.Tmp(); if (tmp) return tmp.text;
            return text;
        }
        public static void SetText(this GameObject a, string text) {
            Text txt = a.Txt(); if (txt) { txt.text = text; return; }
            TextMesh tm = a.Tm(); if (tm) { tm.text = text; return; }
            TextMeshPro tmp = a.Tmp(); if (tmp) { tmp.text = text; }
        }
        public static bool Tag(this GameObject a, string tag) { return a.CompareTag(tag); }
        public static bool Tag(this GameObject a, Tag tag) { return a.CompareTag(tag.tS()); }
        // Transform
        public static Vector3 Tp(this GameObject a) { return a.transform.position; }
        public static void Tp(this GameObject a, Vector3 v) { a.transform.position = v; }
        public static void Tp(this GameObject a, float x, float y, float z) { a.transform.position = V3.V(x, y, z); }
        public static void TpX(this GameObject a, float x) { a.transform.position = a.transform.position.X(x); }
        public static void TpY(this GameObject a, float y) { a.transform.position = a.transform.position.Y(y); }
        public static void TpZ(this GameObject a, float z) { a.transform.position = a.transform.position.Z(z); }
        public static void TpXy(this GameObject a, float x, float y) { a.transform.position = a.transform.position.Xy(x, y); }
        public static void TpXz(this GameObject a, float x, float z) { a.transform.position = a.transform.position.Xz(x, z); }
        public static void TpYz(this GameObject a, float y, float z) { a.transform.position = a.transform.position.Yz(y, z); }
        public static void TpD(this GameObject a, Vector3 v) { a.transform.position += v; }
        public static void TpD(this GameObject a, float x, float y, float z) { a.transform.position += V3.V(x, y, z); }
        public static void TpDx(this GameObject a, float x) { a.transform.position += V3.X(x); }
        public static void TpDy(this GameObject a, float y) { a.transform.position += V3.Y(y); }
        public static void TpDz(this GameObject a, float z) { a.transform.position += V3.Z(z); }
        public static void TpDxy(this GameObject a, float x, float y) { a.transform.position += V3.Xy(x, y); }
        public static void TpDxz(this GameObject a, float x, float z) { a.transform.position += V3.Xz(x, z); }
        public static void TpDyz(this GameObject a, float y, float z) { a.transform.position += V3.Yz(y, z); }
        public static Vector3 Tlp(this GameObject a) { return a.transform.localPosition; }
        public static void Tlp(this GameObject a, Vector3 v) { a.transform.localPosition = v; }
        public static void Tlp(this GameObject a, float x, float y, float z) { a.transform.localPosition = V3.V(x, y, z); }
        public static void TlpX(this GameObject a, float x) { a.transform.localPosition = a.transform.localPosition.X(x); }
        public static void TlpY(this GameObject a, float y) { a.transform.localPosition = a.transform.localPosition.Y(y); }
        public static void TlpZ(this GameObject a, float z) { a.transform.localPosition = a.transform.localPosition.Z(z); }
        public static void TlpXy(this GameObject a, float x, float y) { a.transform.localPosition = a.transform.localPosition.Xy(x, y); }
        public static void TlpXz(this GameObject a, float x, float z) { a.transform.localPosition = a.transform.localPosition.Xz(x, z); }
        public static void TlpYz(this GameObject a, float y, float z) { a.transform.localPosition = a.transform.localPosition.Yz(y, z); }
        public static void TlpD(this GameObject a, Vector3 v) { a.transform.localPosition += v; }
        public static void TlpD(this GameObject a, float x, float y, float z) { a.transform.localPosition += V3.V(x, y, z); }
        public static void TlpDx(this GameObject a, float x) { a.transform.localPosition += V3.X(x); }
        public static void TlpDy(this GameObject a, float y) { a.transform.localPosition += V3.Y(y); }
        public static void TlpDz(this GameObject a, float z) { a.transform.localPosition += V3.Z(z); }
        public static void TlpDxy(this GameObject a, float x, float y) { a.transform.localPosition += V3.Xy(x, y); }
        public static void TlpDxz(this GameObject a, float x, float z) { a.transform.localPosition += V3.Xz(x, z); }
        public static void TlpDyz(this GameObject a, float y, float z) { a.transform.localPosition += V3.Yz(y, z); }
        public static Quaternion Tr(this GameObject a) { return a.transform.rotation; }
        public static void Tr(this GameObject a, Quaternion v) { a.transform.rotation = v; }
        public static Quaternion Tlr(this GameObject a) { return a.transform.localRotation; }
        public static void Tlr(this GameObject a, Quaternion v) { a.transform.localRotation = v; }
        public static Vector3 Ts(this GameObject a) { return a.transform.lossyScale; }
        public static void Ts(this GameObject a, float x, float y, float z) { a.transform.localScale = new Vector3(x / a.transform.lossyScale.x * a.transform.localScale.x, y / a.transform.lossyScale.y * a.transform.localScale.y, z / a.transform.lossyScale.z * a.transform.localScale.z); }
        public static void TsX(this GameObject a, float x) { a.transform.localScale = a.transform.localScale.X(x / a.transform.lossyScale.x * a.transform.localScale.x); }
        public static void TsY(this GameObject a, float y) { a.transform.localScale = a.transform.localScale.Y(y / a.transform.lossyScale.y * a.transform.localScale.y); }
        public static void TsZ(this GameObject a, float z) { a.transform.localScale = a.transform.localScale.Z(z / a.transform.lossyScale.z * a.transform.localScale.z); }
        public static void TsXy(this GameObject a, float x, float y) { a.transform.localScale = a.transform.localScale.Xy(x / a.transform.lossyScale.x * a.transform.localScale.x, y / a.transform.lossyScale.y * a.transform.localScale.y); }
        public static void TsXz(this GameObject a, float x, float z) { a.transform.localScale = a.transform.localScale.Xz(x / a.transform.lossyScale.x * a.transform.localScale.x, z / a.transform.lossyScale.z * a.transform.localScale.z); }
        public static void TsYz(this GameObject a, float y, float z) { a.transform.localScale = a.transform.localScale.Yz(y / a.transform.lossyScale.y * a.transform.localScale.y, z / a.transform.lossyScale.z * a.transform.localScale.z); }
        public static void TsD(this GameObject a, Vector3 v) { a.transform.localScale += new Vector3(v.x / a.transform.localScale.x, v.y / a.transform.localScale.y, v.z / a.transform.localScale.z); }
        public static void TsD(this GameObject a, float x, float y, float z) { a.transform.localScale += new Vector3(x / a.transform.lossyScale.x, y / a.transform.lossyScale.y, z / a.transform.lossyScale.z); }
        public static void TsDx(this GameObject a, float x) { a.transform.localScale += new Vector3(x / a.transform.lossyScale.x, 0, 0); }
        public static void TsDy(this GameObject a, float y) { a.transform.localScale += new Vector3(0, y / a.transform.lossyScale.y, 0); }
        public static void TsDz(this GameObject a, float z) { a.transform.localScale += new Vector3(0, 0, z / a.transform.lossyScale.z); }
        public static void TsDxy(this GameObject a, float x, float y) { a.transform.localScale += new Vector3(x / a.transform.lossyScale.x, y / a.transform.lossyScale.y, 0); }
        public static void TsDxz(this GameObject a, float x, float z) { a.transform.localScale += new Vector3(x / a.transform.lossyScale.x, 0, z / a.transform.lossyScale.z); }
        public static void TsDyz(this GameObject a, float y, float z) { a.transform.localScale += new Vector3(0, y / a.transform.lossyScale.y, z / a.transform.lossyScale.z); }
        public static Vector3 Tls(this GameObject a) { return a.transform.localScale; }
        public static void Tls(this GameObject a, Vector3 v) { a.transform.localScale = v; }
        public static void Tls(this GameObject a, float x, float y, float z) { a.transform.localScale = V3.V(x, y, z); }
        public static void TlsX(this GameObject a, float x) { a.transform.localScale = a.transform.localScale.X(x); }
        public static void TlsY(this GameObject a, float y) { a.transform.localScale = a.transform.localScale.Y(y); }
        public static void TlsZ(this GameObject a, float z) { a.transform.localScale = a.transform.localScale.Z(z); }
        public static void TlsXy(this GameObject a, float x, float y) { a.transform.localScale = a.transform.localScale.Xy(x, y); }
        public static void TlsXz(this GameObject a, float x, float z) { a.transform.localScale = a.transform.localScale.Xz(x, z); }
        public static void TlsYz(this GameObject a, float y, float z) { a.transform.localScale = a.transform.localScale.Yz(y, z); }
        public static void TlsD(this GameObject a, Vector3 v) { a.transform.localScale += v; }
        public static void TlsD(this GameObject a, float x, float y, float z) { a.transform.localScale += V3.V(x, y, z); }
        public static void TlsDx(this GameObject a, float x) { a.transform.localScale += V3.X(x); }
        public static void TlsDy(this GameObject a, float y) { a.transform.localScale += V3.Y(y); }
        public static void TlsDz(this GameObject a, float z) { a.transform.localScale += V3.Z(z); }
        public static void TlsDxy(this GameObject a, float x, float y) { a.transform.localScale += V3.Xy(x, y); }
        public static void TlsDxz(this GameObject a, float x, float z) { a.transform.localScale += V3.Xz(x, z); }
        public static void TlsDyz(this GameObject a, float y, float z) { a.transform.localScale += V3.Yz(y, z); }
        public static Vector3 Te(this GameObject a) { return a.transform.eulerAngles; }
        public static void Te(this GameObject a, Vector3 v) { a.transform.eulerAngles = v; }
        public static void Te(this GameObject a, float x, float y, float z) { a.transform.eulerAngles = V3.V(x, y, z); }
        public static void TeX(this GameObject a, float x) { a.transform.eulerAngles = a.transform.eulerAngles.X(x); }
        public static void TeY(this GameObject a, float y) { a.transform.eulerAngles = a.transform.eulerAngles.Y(y); }
        public static void TeZ(this GameObject a, float z) { a.transform.eulerAngles = a.transform.eulerAngles.Z(z); }
        public static void TeXy(this GameObject a, float x, float y) { a.transform.eulerAngles = a.transform.eulerAngles.Xy(x, y); }
        public static void TeXz(this GameObject a, float x, float z) { a.transform.eulerAngles = a.transform.eulerAngles.Xz(x, z); }
        public static void TeYz(this GameObject a, float y, float z) { a.transform.eulerAngles = a.transform.eulerAngles.Yz(y, z); }
        public static void TeD(this GameObject a, Vector3 v) { a.transform.eulerAngles += v; }
        public static void TeD(this GameObject a, float x, float y, float z) { a.transform.eulerAngles += V3.V(x, y, z); }
        public static void TeDx(this GameObject a, float x) { a.transform.eulerAngles += V3.X(x); }
        public static void TeDy(this GameObject a, float y) { a.transform.eulerAngles += V3.Y(y); }
        public static void TeDz(this GameObject a, float z) { a.transform.eulerAngles += V3.Z(z); }
        public static void TeDxy(this GameObject a, float x, float y) { a.transform.eulerAngles += V3.Xy(x, y); }
        public static void TeDxz(this GameObject a, float x, float z) { a.transform.eulerAngles += V3.Xz(x, z); }
        public static void TeDyz(this GameObject a, float y, float z) { a.transform.eulerAngles += V3.Yz(y, z); }
        public static Vector3 Tle(this GameObject a) { return a.transform.localEulerAngles; }
        public static void Tle(this GameObject a, Vector3 v) { a.transform.localEulerAngles = v; }
        public static void Tle(this GameObject a, float x, float y, float z) { a.transform.localEulerAngles = V3.V(x, y, z); }
        public static void TleX(this GameObject a, float x) { a.transform.localEulerAngles = a.transform.localEulerAngles.X(x); }
        public static void TleY(this GameObject a, float y) { a.transform.localEulerAngles = a.transform.localEulerAngles.Y(y); }
        public static void TleZ(this GameObject a, float z) { a.transform.localEulerAngles = a.transform.localEulerAngles.Z(z); }
        public static void TleXy(this GameObject a, float x, float y) { a.transform.localEulerAngles = a.transform.localEulerAngles.Xy(x, y); }
        public static void TleXz(this GameObject a, float x, float z) { a.transform.localEulerAngles = a.transform.localEulerAngles.Xz(x, z); }
        public static void TleYz(this GameObject a, float y, float z) { a.transform.localEulerAngles = a.transform.localEulerAngles.Yz(y, z); }
        public static void TleD(this GameObject a, Vector3 v) { a.transform.localEulerAngles += v; }
        public static void TleD(this GameObject a, float x, float y, float z) { a.transform.localEulerAngles += V3.V(x, y, z); }
        public static void TleDx(this GameObject a, float x) { a.transform.localEulerAngles += V3.X(x); }
        public static void TleDy(this GameObject a, float y) { a.transform.localEulerAngles += V3.Y(y); }
        public static void TleDz(this GameObject a, float z) { a.transform.localEulerAngles += V3.Z(z); }
        public static void TleDxy(this GameObject a, float x, float y) { a.transform.localEulerAngles += V3.Xy(x, y); }
        public static void TleDxz(this GameObject a, float x, float z) { a.transform.localEulerAngles += V3.Xz(x, z); }
        public static void TleDyz(this GameObject a, float y, float z) { a.transform.localEulerAngles += V3.Yz(y, z); }
        public static int Tcc(this GameObject a) { return a.transform.childCount; }
        // Transform Functions
        public static Vector3 TfDir(this GameObject a, float x, float y, float z) { return a.transform.TransformDirection(x, y, z); }
        public static Vector3 TfDir(this GameObject a, Vector3 dir) { return a.transform.TransformDirection(dir); }
        public static Vector3 TfInvDir(this GameObject a, float x, float y, float z) { return a.transform.InverseTransformDirection(x, y, z); }
        public static Vector3 TfInvDir(this GameObject a, Vector3 dir) { return a.transform.InverseTransformDirection(dir); }
        public static Vector3 TfPnt(this GameObject a, float x, float y, float z) { return a.transform.TransformPoint(x, y, z); }
        public static Vector3 TfPnt(this GameObject a, Vector3 pnt) { return a.transform.TransformPoint(pnt); }
        public static Vector3 TfInvPnt(this GameObject a, float x, float y, float z) { return a.transform.InverseTransformPoint(x, y, z); }
        public static Vector3 TfInvPnt(this GameObject a, Vector3 pnt) { return a.transform.InverseTransformPoint(pnt); }
        public static Vector3 TfVec(this GameObject a, float x, float y, float z) { return a.transform.TransformVector(x, y, z); }
        public static Vector3 TfVec(this GameObject a, Vector3 vec) { return a.transform.TransformVector(vec); }
        public static Vector3 TfInvVec(this GameObject a, float x, float y, float z) { return a.transform.InverseTransformVector(x, y, z); }
        public static Vector3 TfInvVec(this GameObject a, Vector3 vec) { return a.transform.InverseTransformVector(vec); }
        public static void TfLookAt(this GameObject a, Transform tf) { a.transform.LookAt(tf); }
        public static void TfLookAt(this GameObject a, Transform tf, Vector3 up) { a.transform.LookAt(tf, up); }
        public static void TfLookAt(this GameObject a, Vector3 pos) { a.transform.LookAt(pos); }
        public static void TfLookAt(this GameObject a, Vector3 pos, Vector3 up) { a.transform.LookAt(pos, up); }
        public static void TfRot(this GameObject a, float x, float y, float z) { a.transform.Rotate(x, y, z); }
        public static void TfRot(this GameObject a, float x, float y, float z, Space spc) { a.transform.Rotate(x, y, z, spc); }
        public static void TfRot(this GameObject a, Vector3 rot) { a.transform.Rotate(rot); }
        public static void TfRot(this GameObject a, Vector3 rot, Space spc) { a.transform.Rotate(rot, spc); }
        public static void TfRot(this GameObject a, Vector3 axis, float ang) { a.transform.Rotate(axis, ang); }
        public static void TfRot(this GameObject a, Vector3 axis, float ang, Space spc) { a.transform.Rotate(axis, ang, spc); }
        public static void TfRotAround(this GameObject a, Vector3 pnt, Vector3 axis, float ang) { a.transform.RotateAround(pnt, axis, ang); }
        public static void TfTra(this GameObject a, float x, float y, float z) { a.transform.Translate(x, y, z); }
        public static void TfTra(this GameObject a, float x, float y, float z, Space spc) { a.transform.Translate(x, y, z, spc); }
        public static void TfTra(this GameObject a, float x, float y, float z, Transform tf) { a.transform.Translate(x, y, z, tf); }
        public static void TfTra(this GameObject a, Vector3 tra) { a.transform.Translate(tra); }
        public static void TfTra(this GameObject a, Vector3 tra, Space spc) { a.transform.Translate(tra, spc); }
        public static void TfTra(this GameObject a, Vector3 tra, Transform tf) { a.transform.Translate(tra, tf); }
        public static Transform Fnd(this GameObject a, string name) { return a.transform.Find(name); }
        public static GameObject FndGo(this GameObject a, string name) { return a.transform.Find(name).gameObject; }
        public static T Fnd<T>(this GameObject a, string name) { return a.transform.Find(name).gameObject.Gc<T>(); }
        public static void SibIdx(this GameObject a, int i) { a.transform.SetSiblingIndex(i); }
        public static int SibIdx(this GameObject a) { return a.transform.GetSiblingIndex(); }
        public static void SibFist(this GameObject a) { a.transform.SetAsFirstSibling(); }
        public static void SibLast(this GameObject a) { a.transform.SetAsLastSibling(); }
        public static void Par(this GameObject a, Transform parTf) { a.transform.SetParent(parTf); }
        public static void Par(this GameObject a, Transform parTf, bool isWorPosStays) { a.transform.SetParent(parTf, isWorPosStays); }
        // Family
        public static Transform Child(this GameObject a, params int[] childs) {
            Transform tf = a.transform;
            for (int i = 0; i < childs.Length; i++)
                tf = tf.GetChild(childs[i]);
            return tf;
        }
        public static Transform ChildName(this GameObject a, params string[] childs) {
            Transform tf = a.transform;
            for (int i = 0; i < childs.Length; i++)
                tf = tf.Find(childs[i]);
            return tf;
        }
        public static Transform Child(this GameObject a, string childs = "") { return a.Child(childs.IntArr()); }
        public static GameObject ChildGo(this GameObject a, params int[] childs) { return a.Child(childs).gameObject; }
        public static GameObject ChildNameGo(this GameObject a, params string[] childs) { return a.ChildName(childs).gameObject; }
        public static GameObject ChildGo(this GameObject a, string childs = "") { return a.Child(childs.IntArr()).gameObject; }
        public static T Child<T>(this GameObject a, params int[] childs) { return a.ChildGo(childs).Gc<T>(); }
        public static T ChildName<T>(this GameObject a, params string[] childs) { return a.ChildNameGo(childs).Gc<T>(); }
        public static T Child<T>(this GameObject a, string childs = "") { return a.ChildGo(childs.IntArr()).Gc<T>(); }
        public static void ChildActive(this GameObject a, bool active, params int[] childs) { a.ChildGo(childs).Active(active); }
        public static void ChildNameActive(this GameObject a, bool active, params string[] childs) { a.ChildNameGo(childs).Active(active); }
        public static void ChildActive(this GameObject a, bool active, string childs = "") { a.ChildGo(childs.IntArr()).Active(active); }
        public static void ChildHide(this GameObject a, params int[] childs) { a.ChildGo(childs).Hide(); }
        public static void ChildNameHide(this GameObject a, params string[] childs) { a.ChildNameGo(childs).Hide(); }
        public static void ChildHide(this GameObject a, string childs = "") { a.ChildGo(childs.IntArr()).Hide(); }
        public static void ChildShow(this GameObject a, params int[] childs) { a.ChildGo(childs).Show(); }
        public static void ChildNameShow(this GameObject a, params string[] childs) { a.ChildNameGo(childs).Show(); }
        public static void ChildShow(this GameObject a, string childs = "") { a.ChildGo(childs.IntArr()).Show(); }
        public static Transform Par(this GameObject a, int par = 0) {
            Transform tf = a.transform.parent;
            for (int i = 0; i < par; i++)
                tf = tf.parent;
            return tf;
        }
        public static Transform ParName(this GameObject a, string par) {
            Transform tf = a.transform;
            while (tf && tf.name != par)
                tf = tf.parent;
            return tf;
        }
        public static GameObject ParGo(this GameObject a, int par = 0) { return a.Par(par).gameObject; }
        public static GameObject ParNameGo(this GameObject a, string par) { return a.ParName(par).gameObject; }
        public static T Par<T>(this GameObject a, int par = 0) { return a.ParGo(par).Gc<T>(); }
        public static T ParName<T>(this GameObject a, string par) { return a.ParNameGo(par).Gc<T>(); }
        public static void ParActive(this GameObject a, bool active, int par = 0) { a.ParGo(par).Active(active); }
        public static void ParNameActive(this GameObject a, bool active, string par) { a.ParNameGo(par).Active(active); }
        public static void ParHide(this GameObject a, int par = 0) { a.ParGo(par).Hide(); }
        public static void ParNameHide(this GameObject a, string par) { a.ParNameGo(par).Hide(); }
        public static void ParShow(this GameObject a, int par = 0) { a.ParGo(par).Show(); }
        public static void ParNameShow(this GameObject a, string par) { a.ParNameGo(par).Show(); }
        public static Transform ParChild(this GameObject a, int par = 0, params int[] childs) { return a.Par(par).gameObject.Child(childs); }
        public static Transform ParChildName(this GameObject a, string par, params string[] childs) { return a.ParName(par).gameObject.ChildName(childs); }
        public static Transform ParChild(this GameObject a, int par = 0, string childs = "") { return a.ParChild(par, childs.IntArr()); }
        public static GameObject ParChildGo(this GameObject a, int par = 0, params int[] childs) { return a.ParChild(par, childs).gameObject; }
        public static GameObject ParChildNameGo(this GameObject a, string par, params string[] childs) { return a.ParChildName(par, childs).gameObject; }
        public static GameObject ParChildGo(this GameObject a, int par = 0, string childs = "") { return a.ParChild(par, childs.IntArr()).gameObject; }
        public static T ParChild<T>(this GameObject a, int par = 0, params int[] childs) { return a.ParChildGo(par, childs).Gc<T>(); }
        public static T ParChildName<T>(this GameObject a, string par, params string[] childs) { return a.ParChildNameGo(par, childs).Gc<T>(); }
        public static T ParChild<T>(this GameObject a, int par = 0, string childs = "") { return a.ParChildGo(par, childs.IntArr()).Gc<T>(); }
        public static void ParChildActive(this GameObject a, bool active, int par = 0, params int[] childs) { a.ParChildGo(par, childs).Active(active); }
        public static void ParChildNameActive(this GameObject a, bool active, string par, params string[] childs) { a.ParChildNameGo(par, childs).Active(active); }
        public static void ParChildActive(this GameObject a, bool active, int par = 0, string childs = "") { a.ParChildGo(par, childs.IntArr()).Active(active); }
        public static void ParChildHide(this GameObject a, int par = 0, params int[] childs) { a.ParChildGo(par, childs).Hide(); }
        public static void ParChildNameHide(this GameObject a, string par, params string[] childs) { a.ParChildNameGo(par, childs).Hide(); }
        public static void ParChildHide(this GameObject a, int par = 0, string childs = "") { a.ParChildGo(par, childs.IntArr()).Hide(); }
        public static void ParChildShow(this GameObject a, int par = 0, params int[] childs) { a.ParChildGo(par, childs).Show(); }
        public static void ParChildNameShow(this GameObject a, string par, params string[] childs) { a.ParChildNameGo(par, childs).Show(); }
        public static void ParChildShow(this GameObject a, int par = 0, string childs = "") { a.ParChildGo(par, childs.IntArr()).Show(); }
        public static Transform Sib(this GameObject a, int sib = 0) { return a.transform.parent.GetChild(sib); }
        public static Transform SibName(this GameObject a, string sib) { return a.transform.parent.Find(sib); }
        public static GameObject SibGo(this GameObject a, int sib = 0) { return a.Sib(sib).gameObject; }
        public static GameObject SibNameGo(this GameObject a, string sib) { return a.SibName(sib).gameObject; }
        public static T Sib<T>(this GameObject a, int sib = 0) { return a.SibGo(sib).Gc<T>(); }
        public static T SibName<T>(this GameObject a, string sib) { return a.SibNameGo(sib).Gc<T>(); }
        public static void SibActive(this GameObject a, bool active, int sib = 0) { a.SibGo(sib).Active(active); }
        public static void SibNameActive(this GameObject a, bool active, string sib) { a.SibNameGo(sib).Active(active); }
        public static void SibHide(this GameObject a, int sib = 0) { a.SibGo(sib).Hide(); }
        public static void SibNameHide(this GameObject a, string sib) { a.SibNameGo(sib).Hide(); }
        public static void SibShow(this GameObject a, int sib = 0) { a.SibGo(sib).Show(); }
        public static void SibNameShow(this GameObject a, string sib) { a.SibNameGo(sib).Show(); }
        // Material
        public static Material RenMat(this GameObject a) { return a.Ren().material; }
        public static void RenMat(this GameObject a, Material mat) { a.Ren().material = mat; }
        public static List<Material> RenMats(this GameObject a) { return a.Ren().materials.List(); }
        public static void RenMatCol(this GameObject a, Color col) { a.Ren().material.color = col; }
        public static void RenMatCol(this GameObject a, string name, Color col) { a.Ren().material.SetColor(name, col); }
        public static Color RenMatCol(this GameObject a) { return a.Ren().material.color; }
        public static Color RenMatCol(this GameObject a, string name) { return a.Ren().material.GetColor(name); }
        public static Material MatFindName(this GameObject a, string name) { return a.Ren().materials.List().Find(x => x.name.IsS(name)); }
        public static List<Material> MatList(this GameObject a) {
            List<Renderer> renLis = a.Gca<Renderer>();
            List<Material> matLis = new List<Material>();
            for (int i = 0; i < renLis.Count; i++)
                for (int j = 0; j < renLis[i].materials.Length; j++)
                    matLis.Add(renLis[i].materials[j]);
            return matLis;
        }
        public static List<Material> MatListTransparent(this GameObject a) {
            List<Material> matLis = a.MatList();
            matLis.ForEach(x => x.RenMode(RenMd.Transparent));
            return matLis;
        }
        // Collider
        public static BoxCollider Bc(this GameObject a, Vector3 sz, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) {
            BoxCollider bc = a.Bc(tp);
            bc.isTrigger = isT;
            bc.material = mat;
            bc.center = cen;
            bc.size = sz;
            return bc;
        }
        public static SphereCollider Sc(this GameObject a, float r, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) {
            SphereCollider sc = a.Sc(tp);
            sc.isTrigger = isT;
            sc.material = mat;
            sc.center = cen;
            sc.radius = r;
            return sc;
        }
        public static CapsuleCollider Cc(this GameObject a, float r, float h = 1, int dir = 1, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) {
            CapsuleCollider cc = a.Cc(tp);
            cc.isTrigger = isT;
            cc.material = mat;
            cc.center = cen;
            cc.radius = r;
            cc.height = h;
            cc.direction = dir;
            return cc;
        }
        public static void ColIsT(this GameObject a, bool isT) { a.Col().isTrigger = isT; }
        public static bool ColIsT(this GameObject a) { return a.Col().isTrigger; }
        public static void ColMat(this GameObject a, PhysicMaterial mat) { a.Col().material = mat; }
        public static PhysicMaterial ColMat(this GameObject a) { return a.Col().material; }
        public static void BcCen(this GameObject a, Vector3 cen) { a.Bc().center = cen; }
        public static Vector3 BcCen(this GameObject a) { return a.Bc().center; }
        public static void BcSz(this GameObject a, Vector3 sz) { a.Bc().size = sz; }
        public static Vector3 BcSz(this GameObject a) { return a.Bc().size; }
        public static void ScCen(this GameObject a, Vector3 cen) { a.Sc().center = cen; }
        public static Vector3 ScCen(this GameObject a) { return a.Sc().center; }
        public static void ScR(this GameObject a, float r) { a.Sc().radius = r; }
        public static float ScR(this GameObject a) { return a.Sc().radius; }
        public static void CcCen(this GameObject a, Vector3 cen) { a.Cc().center = cen; }
        public static Vector3 CcCen(this GameObject a) { return a.Cc().center; }
        public static void CcR(this GameObject a, float r) { a.Cc().radius = r; }
        public static float CcR(this GameObject a) { return a.Cc().radius; }
        public static void CcH(this GameObject a, float h) { a.Cc().height = h; }
        public static float CcH(this GameObject a) { return a.Cc().height; }
        public static void CcDir(this GameObject a, int dir) { a.Cc().direction = dir; }
        public static int CcDir(this GameObject a) { return a.Cc().direction; }
        public static void McCon(this GameObject a, bool isCon) { a.Mc().convex = isCon; }
        public static bool McCon(this GameObject a) { return a.Mc().convex; }
        public static void McMesh(this GameObject a, Mesh msh) { a.Mc().sharedMesh = msh; }
        public static Mesh McMesh(this GameObject a) { return a.Mc().sharedMesh; }
        public static void WcM(this GameObject a, float m) { a.Wc().mass = m; }
        public static float WcM(this GameObject a) { return a.Wc().mass; }
        public static void WcR(this GameObject a, float r) { a.Wc().radius = r; }
        public static float WcR(this GameObject a) { return a.Wc().radius; }
        public static void WcCen(this GameObject a, Vector3 cen) { a.Wc().center = cen; }
        public static Vector3 WcCen(this GameObject a) { return a.Wc().center; }
        // TrailRenderer
        public static void TrlTm(this GameObject a, float tm) { a.Trl().time = tm; }
        public static float TrlTm(this GameObject a) { return a.Trl().time; }
        public static void TrlMinVerDis(this GameObject a, float minVerDis) { a.Trl().minVertexDistance = minVerDis; }
        public static float TrlMinVerDis(this GameObject a) { return a.Trl().minVertexDistance; }
        public static void TrlAutoDes(this GameObject a, bool autoDes) { a.Trl().autodestruct = autoDes; }
        public static bool TrlAutoDes(this GameObject a) { return a.Trl().autodestruct; }
        public static void TrlEm(this GameObject a, bool isEm) { a.Trl().emitting = isEm; }
        public static bool TrlEm(this GameObject a) { return a.Trl().emitting; }
        public static void TrlCg(this GameObject a, Gradient cg) { a.Trl().colorGradient = cg; }
        public static Gradient TrlCg(this GameObject a) { return a.Trl().colorGradient; }
        public static void TrlNumCorVer(this GameObject a, int numCorVer) { a.Trl().numCornerVertices = numCorVer; }
        public static int TrlNumCorVer(this GameObject a) { return a.Trl().numCornerVertices; }
        // ParticleSystem
        public static ParticleSystem.MainModule PsMain(this GameObject a) { return a.Ps().main; }
        public static void PsMainDur(this GameObject a, float dur) { var v = a.PsMain(); v.duration = dur; }
        public static void PsMainLoop(this GameObject a, bool loop) { var v = a.PsMain(); v.loop = loop; }
        public static void PsMainPrewarm(this GameObject a, bool prewarm) { var v = a.PsMain(); v.prewarm = prewarm; }
        public static void PsMainStaDelay(this GameObject a, ParticleSystem.MinMaxCurve delay, float mul) { var v = a.PsMain(); v.startDelay = delay; v.startDelayMultiplier = mul; }
        public static void PsMainStaLt(this GameObject a, ParticleSystem.MinMaxCurve lt, float mul) { var v = a.PsMain(); v.startLifetime = lt; v.startLifetimeMultiplier = mul; }
        public static void PsMainStaSpd(this GameObject a, ParticleSystem.MinMaxCurve spd, float mul) { var v = a.PsMain(); v.startSpeed = spd; v.startSpeedMultiplier = mul; }
        public static void PsMainStaSz3D(this GameObject a, bool sz) { var v = a.PsMain(); v.startSize3D = sz; }
        public static void PsMainStaSz(this GameObject a, ParticleSystem.MinMaxCurve sz, float mul) { var v = a.PsMain(); v.startSize = sz; v.startSizeMultiplier = mul; }
        public static void PsMainStaSzX(this GameObject a, ParticleSystem.MinMaxCurve sz, float mul) { var v = a.PsMain(); v.startSizeX = sz; v.startSizeXMultiplier = mul; }
        public static void PsMainStaSzY(this GameObject a, ParticleSystem.MinMaxCurve sz, float mul) { var v = a.PsMain(); v.startSizeY = sz; v.startSizeYMultiplier = mul; }
        public static void PsMainStaSzZ(this GameObject a, ParticleSystem.MinMaxCurve sz, float mul) { var v = a.PsMain(); v.startSizeZ = sz; v.startSizeZMultiplier = mul; }
        public static void PsMainStaRot3D(this GameObject a, bool rot) { var v = a.PsMain(); v.startRotation3D = rot; }
        public static void PsMainStaRot(this GameObject a, ParticleSystem.MinMaxCurve rot, float mul) { var v = a.PsMain(); v.startRotation = rot; v.startRotationMultiplier = mul; }
        public static void PsMainStaRotX(this GameObject a, ParticleSystem.MinMaxCurve rot, float mul) { var v = a.PsMain(); v.startRotationX = rot; v.startRotationXMultiplier = mul; }
        public static void PsMainStaRotY(this GameObject a, ParticleSystem.MinMaxCurve rot, float mul) { var v = a.PsMain(); v.startRotationY = rot; v.startRotationYMultiplier = mul; }
        public static void PsMainStaRotZ(this GameObject a, ParticleSystem.MinMaxCurve rot, float mul) { var v = a.PsMain(); v.startRotationZ = rot; v.startRotationZMultiplier = mul; }
        public static void PsMainFlipRot(this GameObject a, float rot) { var v = a.PsMain(); v.flipRotation = rot; }
        public static void PsMainStaC(this GameObject a, ParticleSystem.MinMaxGradient c) { var v = a.PsMain(); v.startColor = c; }
        public static void PsMainGMod(this GameObject a, ParticleSystem.MinMaxCurve gMod, float mul) { var v = a.PsMain(); v.gravityModifier = gMod; v.gravityModifierMultiplier = mul; }
        public static void PsMainSimSpc(this GameObject a, ParticleSystemSimulationSpace spc) { var v = a.PsMain(); v.simulationSpace = spc; }
        public static void PsMainCusSimSpc(this GameObject a, Transform tf) { var v = a.PsMain(); v.customSimulationSpace = tf; }
        public static void PsMainSimSpd(this GameObject a, float spd) { var v = a.PsMain(); v.simulationSpeed = spd; }
        public static void PsMainUseUnSclTm(this GameObject a, bool isUse) { var v = a.PsMain(); v.useUnscaledTime = isUse; }
        public static void PsMainDt(this GameObject a, bool isScl) { a.PsMainUseUnSclTm(!isScl); }
        public static void PsMainSclMd(this GameObject a, ParticleSystemScalingMode md) { var v = a.PsMain(); v.scalingMode = md; }
        public static void PsMainPlayOnAwake(this GameObject a, bool playOnAwake) { var v = a.PsMain(); v.playOnAwake = playOnAwake; }
        public static void PsMainEmVelMd(this GameObject a, ParticleSystemEmitterVelocityMode md) { var v = a.PsMain(); v.emitterVelocityMode = md; }
        public static void PsMainMaxP(this GameObject a, int maxP) { var v = a.PsMain(); v.maxParticles = maxP; }
        public static void PsMainAutoRndSeed(this GameObject a, bool isUse) { a.Ps().useAutoRandomSeed = isUse; }
        public static void PsMainRndSeed(this GameObject a, uint seed) { a.Ps().randomSeed = seed; }
        public static void PsMainStopAct(this GameObject a, ParticleSystemStopAction act) { var v = a.PsMain(); v.stopAction = act; }
        public static void PsMainCulMd(this GameObject a, ParticleSystemCullingMode md) { var v = a.PsMain(); v.cullingMode = md; }
        public static void PsMainRingBufMd(this GameObject a, ParticleSystemRingBufferMode md) { var v = a.PsMain(); v.ringBufferMode = md; }
        public static void PsMainRingBufLoopRng(this GameObject a, Vector2 rng) { var v = a.PsMain(); v.ringBufferLoopRange = rng; }
        public static ParticleSystem.EmissionModule PsEm(this GameObject a) { return a.Ps().emission; }
        public static void PsEmRot(this GameObject a, float tm) { var v = a.PsEm(); v.rateOverTime = tm; }
        public static void PsEmRot(this GameObject a, ParticleSystem.MinMaxCurve tm, float mul) { var v = a.PsEm(); v.rateOverTime = tm; v.rateOverTimeMultiplier = mul; }
        public static void PsEmRod(this GameObject a, float dis) { var v = a.PsEm(); v.rateOverDistance = dis; }
        public static void PsEmRod(this GameObject a, ParticleSystem.MinMaxCurve dis, float mul) { var v = a.PsEm(); v.rateOverDistance = dis; v.rateOverDistanceMultiplier = mul; }
        public static void PsEmSetBursts(this GameObject a, params ParticleSystem.Burst[] bursts) { var v = a.PsEm(); v.SetBursts(bursts); }
        public static ParticleSystem.ShapeModule PsShp(this GameObject a) { return a.Ps().shape; }
        public static ParticleSystem.VelocityOverLifetimeModule PsVol(this GameObject a) { return a.Ps().velocityOverLifetime; }
        public static void PsVolX(this GameObject a, ParticleSystem.MinMaxCurve x, float mul) { var v = a.PsVol(); v.x = x; v.xMultiplier = mul; }
        public static void PsVolY(this GameObject a, ParticleSystem.MinMaxCurve y, float mul) { var v = a.PsVol(); v.y = y; v.yMultiplier = mul; }
        public static void PsVolZ(this GameObject a, ParticleSystem.MinMaxCurve z, float mul) { var v = a.PsVol(); v.z = z; v.zMultiplier = mul; }
        public static void PsVolOrbX(this GameObject a, ParticleSystem.MinMaxCurve x, float mul) { var v = a.PsVol(); v.orbitalX = x; v.orbitalXMultiplier = mul; }
        public static void PsVolOrbY(this GameObject a, ParticleSystem.MinMaxCurve y, float mul) { var v = a.PsVol(); v.orbitalY = y; v.orbitalYMultiplier = mul; }
        public static void PsVolOrbZ(this GameObject a, ParticleSystem.MinMaxCurve z, float mul) { var v = a.PsVol(); v.orbitalZ = z; v.orbitalZMultiplier = mul; }
        public static void PsVolOrbOffX(this GameObject a, ParticleSystem.MinMaxCurve x, float mul) { var v = a.PsVol(); v.orbitalOffsetX = x; v.orbitalOffsetXMultiplier = mul; }
        public static void PsVolOrbOffY(this GameObject a, ParticleSystem.MinMaxCurve y, float mul) { var v = a.PsVol(); v.orbitalOffsetY = y; v.orbitalOffsetYMultiplier = mul; }
        public static void PsVolOrbOffZ(this GameObject a, ParticleSystem.MinMaxCurve z, float mul) { var v = a.PsVol(); v.orbitalOffsetZ = z; v.orbitalOffsetZMultiplier = mul; }
        public static void PsVolRadial(this GameObject a, ParticleSystem.MinMaxCurve radial, float mul) { var v = a.PsVol(); v.radial = radial; v.radialMultiplier = mul; }
        public static void PsVolSpdMod(this GameObject a, ParticleSystem.MinMaxCurve mod, float mul) { var v = a.PsVol(); v.speedModifier = mod; v.speedModifierMultiplier = mul; }
        public static void PsVolSpc(this GameObject a, ParticleSystemSimulationSpace spc) { var v = a.PsVol(); v.space = spc; }
        public static ParticleSystem.LimitVelocityOverLifetimeModule PsLvol(this GameObject a) { return a.Ps().limitVelocityOverLifetime; }
        public static void PsLvolLim(this GameObject a, ParticleSystem.MinMaxCurve lim, float mul) { var v = a.PsLvol(); v.limit = lim; v.limitMultiplier = mul; }
        public static void PsLvolLimX(this GameObject a, ParticleSystem.MinMaxCurve lim, float mul) { var v = a.PsLvol(); v.limitX = lim; v.limitXMultiplier = mul; }
        public static void PsLvolLimY(this GameObject a, ParticleSystem.MinMaxCurve lim, float mul) { var v = a.PsLvol(); v.limitY = lim; v.limitYMultiplier = mul; }
        public static void PsLvolLimZ(this GameObject a, ParticleSystem.MinMaxCurve lim, float mul) { var v = a.PsLvol(); v.limitZ = lim; v.limitZMultiplier = mul; }
        public static void PsLvolDrag(this GameObject a, ParticleSystem.MinMaxCurve drag, float mul) { var v = a.PsLvol(); v.drag = drag; v.dragMultiplier = mul; }
        public static void PsLvolSpc(this GameObject a, ParticleSystemSimulationSpace spc) { var v = a.PsLvol(); v.space = spc; }
        public static void PsLvolDampen(this GameObject a, float isDampen) { var v = a.PsLvol(); v.dampen = isDampen; }
        public static void PsLvolSepAxes(this GameObject a, bool isAxes) { var v = a.PsLvol(); v.separateAxes = isAxes; }
        public static void PsLvolMulDragByPSz(this GameObject a, bool isSz) { var v = a.PsLvol(); v.multiplyDragByParticleSize = isSz; }
        public static void PsLvolMulDragByPVel(this GameObject a, bool isVel) { var v = a.PsLvol(); v.multiplyDragByParticleVelocity = isVel; }
        public static ParticleSystem.InheritVelocityModule PsIv(this GameObject a) { return a.Ps().inheritVelocity; }
        public static void PsIvMode(this GameObject a, ParticleSystemInheritVelocityMode md) { var v = a.PsIv(); v.mode = md; }
        public static void PsIvCrv(this GameObject a, ParticleSystem.MinMaxCurve crv, float mul) { var v = a.PsIv(); v.curve = crv; v.curveMultiplier = mul; }
        public static ParticleSystem.ForceOverLifetimeModule PsFol(this GameObject a) { return a.Ps().forceOverLifetime; }
        public static void PsFolX(this GameObject a, ParticleSystem.MinMaxCurve x, float mul) { var v = a.PsFol(); v.x = x; v.xMultiplier = mul; }
        public static void PsFolY(this GameObject a, ParticleSystem.MinMaxCurve y, float mul) { var v = a.PsFol(); v.y = y; v.yMultiplier = mul; }
        public static void PsFolZ(this GameObject a, ParticleSystem.MinMaxCurve z, float mul) { var v = a.PsFol(); v.z = z; v.zMultiplier = mul; }
        public static void PsFolSpc(this GameObject a, ParticleSystemSimulationSpace spc) { var v = a.PsFol(); v.space = spc; }
        public static void PsFolRnd(this GameObject a, bool isRnd) { var v = a.PsFol(); v.randomized = isRnd; }
        public static ParticleSystem.ColorOverLifetimeModule PsCol(this GameObject a) { return a.Ps().colorOverLifetime; }
        public static void PsColC(this GameObject a, ParticleSystem.MinMaxGradient c) { var v = a.PsCol(); v.color = c; }
        public static ParticleSystem.ColorBySpeedModule PsCbs(this GameObject a) { return a.Ps().colorBySpeed; }
        public static void PsCbsC(this GameObject a, ParticleSystem.MinMaxGradient c) { var v = a.PsCbs(); v.color = c; }
        public static void PsCbsRng(this GameObject a, Vector2 rng) { var v = a.PsCbs(); v.range = rng; }
        public static ParticleSystem.SizeOverLifetimeModule PsSol(this GameObject a) { return a.Ps().sizeOverLifetime; }
        public static void PsSolSepAxes(this GameObject a, bool isAxes) { var v = a.PsSol(); v.separateAxes = isAxes; }
        public static void PsSolSz(this GameObject a, ParticleSystem.MinMaxCurve sz, float mul) { var v = a.PsSol(); v.size = sz; v.sizeMultiplier = mul; }
        public static void PsSolX(this GameObject a, ParticleSystem.MinMaxCurve x, float mul) { var v = a.PsSol(); v.x = x; v.xMultiplier = mul; }
        public static void PsSolY(this GameObject a, ParticleSystem.MinMaxCurve y, float mul) { var v = a.PsSol(); v.y = y; v.yMultiplier = mul; }
        public static void PsSolZ(this GameObject a, ParticleSystem.MinMaxCurve z, float mul) { var v = a.PsSol(); v.z = z; v.zMultiplier = mul; }
        public static ParticleSystem.SizeBySpeedModule PsSbs(this GameObject a) { return a.Ps().sizeBySpeed; }
        public static void PsSbsSepAxes(this GameObject a, bool isAxes) { var v = a.PsSbs(); v.separateAxes = isAxes; }
        public static void PsSbsSz(this GameObject a, ParticleSystem.MinMaxCurve sz, float mul) { var v = a.PsSbs(); v.size = sz; v.sizeMultiplier = mul; }
        public static void PsSbsX(this GameObject a, ParticleSystem.MinMaxCurve x, float mul) { var v = a.PsSbs(); v.x = x; v.xMultiplier = mul; }
        public static void PsSbsY(this GameObject a, ParticleSystem.MinMaxCurve y, float mul) { var v = a.PsSbs(); v.y = y; v.yMultiplier = mul; }
        public static void PsSbsZ(this GameObject a, ParticleSystem.MinMaxCurve z, float mul) { var v = a.PsSbs(); v.z = z; v.zMultiplier = mul; }
        public static void PsSbsRng(this GameObject a, Vector2 rng) { var v = a.PsSbs(); v.range = rng; }
        public static ParticleSystem.RotationOverLifetimeModule PsRol(this GameObject a) { return a.Ps().rotationOverLifetime; }
        public static void PsRolSepAxes(this GameObject a, bool isAxes) { var v = a.PsRol(); v.separateAxes = isAxes; }
        public static void PsRolX(this GameObject a, ParticleSystem.MinMaxCurve x, float mul) { var v = a.PsRol(); v.x = x; v.xMultiplier = mul; }
        public static void PsRolY(this GameObject a, ParticleSystem.MinMaxCurve y, float mul) { var v = a.PsRol(); v.y = y; v.yMultiplier = mul; }
        public static void PsRolZ(this GameObject a, ParticleSystem.MinMaxCurve z, float mul) { var v = a.PsRol(); v.z = z; v.zMultiplier = mul; }
        public static ParticleSystem.RotationBySpeedModule PsRbs(this GameObject a) { return a.Ps().rotationBySpeed; }
        public static void PsRbsSepAxes(this GameObject a, bool isAxes) { var v = a.PsRbs(); v.separateAxes = isAxes; }
        public static void PsRbsX(this GameObject a, ParticleSystem.MinMaxCurve x, float mul) { var v = a.PsRbs(); v.x = x; v.xMultiplier = mul; }
        public static void PsRbsY(this GameObject a, ParticleSystem.MinMaxCurve y, float mul) { var v = a.PsRbs(); v.y = y; v.yMultiplier = mul; }
        public static void PsRbsZ(this GameObject a, ParticleSystem.MinMaxCurve z, float mul) { var v = a.PsRbs(); v.z = z; v.zMultiplier = mul; }
        public static void PsRbsRng(this GameObject a, Vector2 rng) { var v = a.PsRbs(); v.range = rng; }
        public static ParticleSystem.ExternalForcesModule PsEf(this GameObject a) { return a.Ps().externalForces; }
        public static void PsEfMulCrv(this GameObject a, ParticleSystem.MinMaxCurve crv, float mul) { var v = a.PsEf(); v.multiplierCurve = crv; v.multiplier = mul; }
        public static void PsEfInfFilter(this GameObject a, ParticleSystemGameObjectFilter filter) { var v = a.PsEf(); v.influenceFilter = filter; }
        public static void PsEfInfMask(this GameObject a, LayerMask lm) { var v = a.PsEf(); v.influenceMask = lm; }
        public static ParticleSystem.NoiseModule PsNoise(this GameObject a) { return a.Ps().noise; }
        public static ParticleSystem.CollisionModule PsCollision(this GameObject a) { return a.Ps().collision; }
        public static ParticleSystem.TriggerModule PsTrigger(this GameObject a) { return a.Ps().trigger; }
        public static ParticleSystem.SubEmittersModule PsSubEm(this GameObject a) { return a.Ps().subEmitters; }
        public static ParticleSystem.TextureSheetAnimationModule PsTexSheetAnim(this GameObject a) { return a.Ps().textureSheetAnimation; }
        public static ParticleSystem.LightsModule PsLights(this GameObject a) { return a.Ps().lights; }
        public static ParticleSystem.TrailModule PsTrails(this GameObject a) { return a.Ps().trails; }
        public static ParticleSystem.CustomDataModule PsCusDat(this GameObject a) { return a.Ps().customData; }
        // Rigidbody
        public static void RbG(this GameObject a, bool useGravity = true) { a.Rb().G(useGravity); }
        public static void RbNoG(this GameObject a) { a.Rb().NoG(); }
        public static void RbK(this GameObject a, bool isKinematic = true) { a.Rb().K(isKinematic); }
        public static void RbNoK(this GameObject a) { a.Rb().NoK(); }
        public static void RbInt(this GameObject a, int interpolate) { a.Rb().Int(interpolate); }
        public static void RbInt(this GameObject a, RigidbodyInterpolation interpolate) { a.Rb().Int(interpolate); }
        public static void RbInt0(this GameObject a) { a.Rb().Int0(); }
        public static void RbIntInt(this GameObject a) { a.Rb().IntInt(); }
        public static void RbIntExt(this GameObject a) { a.Rb().IntExt(); }
        public static void RbColDet(this GameObject a, int collisionDetection) { a.Rb().ColDet(collisionDetection); }
        public static void RbColDet(this GameObject a, CollisionDetectionMode collisionDetection) { a.Rb().ColDet(collisionDetection); }
        public static void RbColDetDis(this GameObject a) { a.Rb().ColDetDis(); }
        public static void RbColDetCon(this GameObject a) { a.Rb().ColDetCon(); }
        public static void RbColDetConD(this GameObject a) { a.Rb().ColDetConD(); }
        public static void RbColDetConS(this GameObject a) { a.Rb().ColDetConS(); }
        public static void RbCon(this GameObject a, bool isFrzPosX = false, bool isFrzPosY = false, bool isFrzPosZ = false, bool isFrzRotX = false, bool isFrzRotY = false, bool isFrzRotZ = false) { a.Rb().Con(isFrzPosX, isFrzPosY, isFrzPosZ, isFrzRotX, isFrzRotY, isFrzRotZ); }
        public static void RbCon0(this GameObject a) { a.Rb().Con0(); }
        public static void RbConFrzPos(this GameObject a) { a.Rb().ConFrzPos(); }
        public static void RbConFrzRot(this GameObject a) { a.Rb().ConFrzRot(); }
        public static void RbConFrzAll(this GameObject a) { a.Rb().ConFrzAll(); }
        public static void RbAv0(this GameObject a) { a.Rb().Av0(); }
        public static void RbAv(this GameObject a, Vector3 v) { a.Rb().Av(v); }
        public static void RbAvx(this GameObject a, float x) { a.Rb().Avx(x); }
        public static void RbAvy(this GameObject a, float y) { a.Rb().Avy(y); }
        public static void RbAvz(this GameObject a, float z) { a.Rb().Avz(z); }
        public static void RbAvxy(this GameObject a, float x, float y) { a.Rb().Avxy(x, y); }
        public static void RbAvxz(this GameObject a, float x, float z) { a.Rb().Avxz(x, z); }
        public static void RbAvyz(this GameObject a, float y, float z) { a.Rb().Avyz(y, z); }
        public static void RbAv(this GameObject a, float x, float y, float z) { a.Rb().Av(x, y, z); }
        public static void RbV0(this GameObject a) { a.Rb().V0(); }
        public static void RbV(this GameObject a, Vector3 v) { a.Rb().V(v); }
        public static void RbVx(this GameObject a, float x) { a.Rb().Vx(x); }
        public static void RbVy(this GameObject a, float y) { a.Rb().Vy(y); }
        public static void RbVz(this GameObject a, float z) { a.Rb().Vz(z); }
        public static void RbVxy(this GameObject a, float x, float y) { a.Rb().Vxy(x, y); }
        public static void RbVxz(this GameObject a, float x, float z) { a.Rb().Vxz(x, z); }
        public static void RbVyz(this GameObject a, float y, float z) { a.Rb().Vyz(y, z); }
        public static void RbV(this GameObject a, float x, float y, float z) { a.Rb().V(x, y, z); }
    }
}