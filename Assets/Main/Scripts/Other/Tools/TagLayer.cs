namespace Orgil {
	public enum Tag { Untagged, Respawn, Finish, EditorOnly, MainCamera, Player, GameController, Diamond, Bot, Coin, Pipe, Floor }
	public class Lyr { public const int Default = 0, TransparentFX = 1, IgnoreRaycast = 2, Floor = 3, Water = 4, UI = 5, Pipe = 6; }
	public class Lm { public const int Default = 1, TransparentFX = 2, IgnoreRaycast = 4, Floor = 8, Water = 16, UI = 32, Pipe = 64; }
}