using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Orgil {
	public static class ExtComCol {
		public static T Gc<T>(this Component a) { return a.gameObject.Gc<T>(); }
		public static T Gc<T>(this Component a, GcTp tp) where T : Component { return a.gameObject.Gc<T>(tp); }
		public static Text Txt(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Txt(tp); }
		public static Button Btn(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Btn(tp); }
		public static Image Img(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Img(tp); }
		public static Toggle Tgl(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Tgl(tp); }
		public static Slider Sld(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Sld(tp); }
		public static Dropdown Drp(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Drp(tp); }
		public static InputField Inp(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Inp(tp); }
		public static ColorPicker Cp(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Cp(tp); }
		public static TextMesh Tm(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Tm(tp); }
		public static TextMeshPro Tmp(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Tmp(tp); }
		public static Rigidbody Rb(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Rb(tp); }
		public static Rigidbody2D Rb2(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Rb2(tp); }
		public static Renderer Ren(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Ren(tp); }
		public static MeshFilter Mf(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Mf(tp); }
		public static MeshRenderer Mr(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Mr(tp); }
		public static Collider Col(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Col(tp); }
		public static BoxCollider Bc(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Bc(tp); }
		public static SphereCollider Sc(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Sc(tp); }
		public static CapsuleCollider Cc(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Cc(tp); }
		public static MeshCollider Mc(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Mc(tp); }
		public static WheelCollider Wc(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Wc(tp); }
		public static ParticleSystem Ps(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Ps(tp); }
		public static ParticleSystemRenderer Psr(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Psr(tp); }
		public static TrailRenderer Trl(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Trl(tp); }
		public static Animator An(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.An(tp); }
		public static RectTransform Rt(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Rt(tp); }
		public static LineRenderer Lr(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Lr(tp); }
		public static Shadow Shadow(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Shadow(tp); }
		public static Outline Outline(this Component a, GcTp tp = GcTp.Get) { return a.gameObject.Outline(tp); }
		public static void Active(this Component a, bool active) { a.gameObject.Active(active); }
		public static void Show(this Component a) { a.gameObject.Show(); }
		public static void Hide(this Component a) { a.gameObject.Hide(); }
		public static bool IsActive(this Component a) { return a.gameObject.IsActive(); }
		public static bool Is<T>(this Component a) { return a.gameObject.Is<T>(); }
		public static T Ac<T>(this Component a) where T : Component { return a.gameObject.Ac<T>(); }
		public static T Gcic<T>(this Component a) { return a.gameObject.Gcic<T>(); }
		public static T Gcip<T>(this Component a) { return a.gameObject.Gcip<T>(); }
		public static T[] Gcs<T>(this Component a) { return a.gameObject.Gcs<T>(); }
		public static T[] Gcsic<T>(this Component a) { return a.gameObject.Gcsic<T>(); }
		public static T[] Gcsip<T>(this Component a) { return a.gameObject.Gcsip<T>(); }
		public static List<T> Gca<T>(this Component a) { return a.gameObject.Gca<T>(); }
		public static List<Transform> Childs(this Component a) { return a.gameObject.Childs(); }
		public static List<GameObject> ChildGos(this Component a) { return a.gameObject.ChildGos(); }
		public static List<T> Childs<T>(this Component a) { return a.gameObject.Childs<T>(); }
		public static void DstChilds<T>(this Component a, int sta = 0, int end = -1) where T : Object { a.gameObject.DstChilds<T>(sta, end); }
		public static void DstChilds(this Component a, int sta = 0, int end = -1) { a.gameObject.DstChilds(sta, end); }
		public static Color GetCol(this Component a, Color col = default) { return a.gameObject.GetCol(col); }
		public static void SetCol(this Component a, Color col) { a.gameObject.SetCol(col); }
		public static void ColA(this Component a, float alpha) { a.gameObject.ColA(alpha); }
		public static string GetText(this Component a, string text = "") { return a.gameObject.GetText(text); }
		public static void SetText(this Component a, string text) { a.gameObject.SetText(text); }
		public static bool Tag(this Component a, string tag) { return a.gameObject.Tag(tag); }
		public static bool Tag(this Component a, Tag tag) { return a.gameObject.Tag(tag); }
		public static Vector3 Tp(this Component a) { return a.gameObject.Tp(); }
		public static void Tp(this Component a, Vector3 v) { a.gameObject.Tp(v); }
		public static void Tp(this Component a, float x, float y, float z) { a.gameObject.Tp(x, y, z); }
		public static void TpX(this Component a, float x) { a.gameObject.TpX(x); }
		public static void TpY(this Component a, float y) { a.gameObject.TpY(y); }
		public static void TpZ(this Component a, float z) { a.gameObject.TpZ(z); }
		public static void TpXy(this Component a, float x, float y) { a.gameObject.TpXy(x, y); }
		public static void TpXz(this Component a, float x, float z) { a.gameObject.TpXz(x, z); }
		public static void TpYz(this Component a, float y, float z) { a.gameObject.TpYz(y, z); }
		public static void TpD(this Component a, Vector3 v) { a.gameObject.TpD(v); }
		public static void TpD(this Component a, float x, float y, float z) { a.gameObject.TpD(x, y, z); }
		public static void TpDx(this Component a, float x) { a.gameObject.TpDx(x); }
		public static void TpDy(this Component a, float y) { a.gameObject.TpDy(y); }
		public static void TpDz(this Component a, float z) { a.gameObject.TpDz(z); }
		public static void TpDxy(this Component a, float x, float y) { a.gameObject.TpDxy(x, y); }
		public static void TpDxz(this Component a, float x, float z) { a.gameObject.TpDxz(x, z); }
		public static void TpDyz(this Component a, float y, float z) { a.gameObject.TpDyz(y, z); }
		public static Vector3 Tlp(this Component a) { return a.gameObject.Tlp(); }
		public static void Tlp(this Component a, Vector3 v) { a.gameObject.Tlp(v); }
		public static void Tlp(this Component a, float x, float y, float z) { a.gameObject.Tlp(x, y, z); }
		public static void TlpX(this Component a, float x) { a.gameObject.TlpX(x); }
		public static void TlpY(this Component a, float y) { a.gameObject.TlpY(y); }
		public static void TlpZ(this Component a, float z) { a.gameObject.TlpZ(z); }
		public static void TlpXy(this Component a, float x, float y) { a.gameObject.TlpXy(x, y); }
		public static void TlpXz(this Component a, float x, float z) { a.gameObject.TlpXz(x, z); }
		public static void TlpYz(this Component a, float y, float z) { a.gameObject.TlpYz(y, z); }
		public static void TlpD(this Component a, Vector3 v) { a.gameObject.TlpD(v); }
		public static void TlpD(this Component a, float x, float y, float z) { a.gameObject.TlpD(x, y, z); }
		public static void TlpDx(this Component a, float x) { a.gameObject.TlpDx(x); }
		public static void TlpDy(this Component a, float y) { a.gameObject.TlpDy(y); }
		public static void TlpDz(this Component a, float z) { a.gameObject.TlpDz(z); }
		public static void TlpDxy(this Component a, float x, float y) { a.gameObject.TlpDxy(x, y); }
		public static void TlpDxz(this Component a, float x, float z) { a.gameObject.TlpDxz(x, z); }
		public static void TlpDyz(this Component a, float y, float z) { a.gameObject.TlpDyz(y, z); }
		public static Quaternion Tr(this Component a) { return a.gameObject.Tr(); }
		public static void Tr(this Component a, Quaternion v) { a.gameObject.Tr(v); }
		public static Quaternion Tlr(this Component a) { return a.gameObject.Tlr(); }
		public static void Tlr(this Component a, Quaternion v) { a.gameObject.Tlr(v); }
		public static Vector3 Ts(this Component a) { return a.gameObject.Ts(); }
		public static void Ts(this Component a, float x, float y, float z) { a.gameObject.Ts(x, y, z); }
		public static void TsX(this Component a, float x) { a.gameObject.TsX(x); }
		public static void TsY(this Component a, float y) { a.gameObject.TsY(y); }
		public static void TsZ(this Component a, float z) { a.gameObject.TsZ(z); }
		public static void TsXy(this Component a, float x, float y) { a.gameObject.TsXy(x, y); }
		public static void TsXz(this Component a, float x, float z) { a.gameObject.TsXz(x, z); }
		public static void TsYz(this Component a, float y, float z) { a.gameObject.TsYz(y, z); }
		public static void TsD(this Component a, Vector3 v) { a.gameObject.TsD(v); }
		public static void TsD(this Component a, float x, float y, float z) { a.gameObject.TsD(x, y, z); }
		public static void TsDx(this Component a, float x) { a.gameObject.TsDx(x); }
		public static void TsDy(this Component a, float y) { a.gameObject.TsDy(y); }
		public static void TsDz(this Component a, float z) { a.gameObject.TsDz(z); }
		public static void TsDxy(this Component a, float x, float y) { a.gameObject.TsDxy(x, y); }
		public static void TsDxz(this Component a, float x, float z) { a.gameObject.TsDxz(x, z); }
		public static void TsDyz(this Component a, float y, float z) { a.gameObject.TsDyz(y, z); }
		public static Vector3 Tls(this Component a) { return a.gameObject.Tls(); }
		public static void Tls(this Component a, Vector3 v) { a.gameObject.Tls(v); }
		public static void Tls(this Component a, float x, float y, float z) { a.gameObject.Tls(x, y, z); }
		public static void TlsX(this Component a, float x) { a.gameObject.TlsX(x); }
		public static void TlsY(this Component a, float y) { a.gameObject.TlsY(y); }
		public static void TlsZ(this Component a, float z) { a.gameObject.TlsZ(z); }
		public static void TlsXy(this Component a, float x, float y) { a.gameObject.TlsXy(x, y); }
		public static void TlsXz(this Component a, float x, float z) { a.gameObject.TlsXz(x, z); }
		public static void TlsYz(this Component a, float y, float z) { a.gameObject.TlsYz(y, z); }
		public static void TlsD(this Component a, Vector3 v) { a.gameObject.TlsD(v); }
		public static void TlsD(this Component a, float x, float y, float z) { a.gameObject.TlsD(x, y, z); }
		public static void TlsDx(this Component a, float x) { a.gameObject.TlsDx(x); }
		public static void TlsDy(this Component a, float y) { a.gameObject.TlsDy(y); }
		public static void TlsDz(this Component a, float z) { a.gameObject.TlsDz(z); }
		public static void TlsDxy(this Component a, float x, float y) { a.gameObject.TlsDxy(x, y); }
		public static void TlsDxz(this Component a, float x, float z) { a.gameObject.TlsDxz(x, z); }
		public static void TlsDyz(this Component a, float y, float z) { a.gameObject.TlsDyz(y, z); }
		public static Vector3 Te(this Component a) { return a.gameObject.Te(); }
		public static void Te(this Component a, Vector3 v) { a.gameObject.Te(v); }
		public static void Te(this Component a, float x, float y, float z) { a.gameObject.Te(x, y, z); }
		public static void TeX(this Component a, float x) { a.gameObject.TeX(x); }
		public static void TeY(this Component a, float y) { a.gameObject.TeY(y); }
		public static void TeZ(this Component a, float z) { a.gameObject.TeZ(z); }
		public static void TeXy(this Component a, float x, float y) { a.gameObject.TeXy(x, y); }
		public static void TeXz(this Component a, float x, float z) { a.gameObject.TeXz(x, z); }
		public static void TeYz(this Component a, float y, float z) { a.gameObject.TeYz(y, z); }
		public static void TeD(this Component a, Vector3 v) { a.gameObject.TeD(v); }
		public static void TeD(this Component a, float x, float y, float z) { a.gameObject.TeD(x, y, z); }
		public static void TeDx(this Component a, float x) { a.gameObject.TeDx(x); }
		public static void TeDy(this Component a, float y) { a.gameObject.TeDy(y); }
		public static void TeDz(this Component a, float z) { a.gameObject.TeDz(z); }
		public static void TeDxy(this Component a, float x, float y) { a.gameObject.TeDxy(x, y); }
		public static void TeDxz(this Component a, float x, float z) { a.gameObject.TeDxz(x, z); }
		public static void TeDyz(this Component a, float y, float z) { a.gameObject.TeDyz(y, z); }
		public static Vector3 Tle(this Component a) { return a.gameObject.Tle(); }
		public static void Tle(this Component a, Vector3 v) { a.gameObject.Tle(v); }
		public static void Tle(this Component a, float x, float y, float z) { a.gameObject.Tle(x, y, z); }
		public static void TleX(this Component a, float x) { a.gameObject.TleX(x); }
		public static void TleY(this Component a, float y) { a.gameObject.TleY(y); }
		public static void TleZ(this Component a, float z) { a.gameObject.TleZ(z); }
		public static void TleXy(this Component a, float x, float y) { a.gameObject.TleXy(x, y); }
		public static void TleXz(this Component a, float x, float z) { a.gameObject.TleXz(x, z); }
		public static void TleYz(this Component a, float y, float z) { a.gameObject.TleYz(y, z); }
		public static void TleD(this Component a, Vector3 v) { a.gameObject.TleD(v); }
		public static void TleD(this Component a, float x, float y, float z) { a.gameObject.TleD(x, y, z); }
		public static void TleDx(this Component a, float x) { a.gameObject.TleDx(x); }
		public static void TleDy(this Component a, float y) { a.gameObject.TleDy(y); }
		public static void TleDz(this Component a, float z) { a.gameObject.TleDz(z); }
		public static void TleDxy(this Component a, float x, float y) { a.gameObject.TleDxy(x, y); }
		public static void TleDxz(this Component a, float x, float z) { a.gameObject.TleDxz(x, z); }
		public static void TleDyz(this Component a, float y, float z) { a.gameObject.TleDyz(y, z); }
		public static int Tcc(this Component a) { return a.gameObject.Tcc(); }
		public static Vector3 TfDir(this Component a, float x, float y, float z) { return a.gameObject.TfDir(x, y, z); }
		public static Vector3 TfDir(this Component a, Vector3 dir) { return a.gameObject.TfDir(dir); }
		public static Vector3 TfInvDir(this Component a, float x, float y, float z) { return a.gameObject.TfInvDir(x, y, z); }
		public static Vector3 TfInvDir(this Component a, Vector3 dir) { return a.gameObject.TfInvDir(dir); }
		public static Vector3 TfPnt(this Component a, float x, float y, float z) { return a.gameObject.TfPnt(x, y, z); }
		public static Vector3 TfPnt(this Component a, Vector3 pnt) { return a.gameObject.TfPnt(pnt); }
		public static Vector3 TfInvPnt(this Component a, float x, float y, float z) { return a.gameObject.TfInvPnt(x, y, z); }
		public static Vector3 TfInvPnt(this Component a, Vector3 pnt) { return a.gameObject.TfInvPnt(pnt); }
		public static Vector3 TfVec(this Component a, float x, float y, float z) { return a.gameObject.TfVec(x, y, z); }
		public static Vector3 TfVec(this Component a, Vector3 vec) { return a.gameObject.TfVec(vec); }
		public static Vector3 TfInvVec(this Component a, float x, float y, float z) { return a.gameObject.TfInvVec(x, y, z); }
		public static Vector3 TfInvVec(this Component a, Vector3 vec) { return a.gameObject.TfInvVec(vec); }
		public static void TfLookAt(this Component a, Transform tf) { a.gameObject.TfLookAt(tf); }
		public static void TfLookAt(this Component a, Transform tf, Vector3 up) { a.gameObject.TfLookAt(tf, up); }
		public static void TfLookAt(this Component a, Vector3 pos) { a.gameObject.TfLookAt(pos); }
		public static void TfLookAt(this Component a, Vector3 pos, Vector3 up) { a.gameObject.TfLookAt(pos, up); }
		public static void TfRot(this Component a, float x, float y, float z) { a.gameObject.TfRot(x, y, z); }
		public static void TfRot(this Component a, float x, float y, float z, Space spc) { a.gameObject.TfRot(x, y, z, spc); }
		public static void TfRot(this Component a, Vector3 rot) { a.gameObject.TfRot(rot); }
		public static void TfRot(this Component a, Vector3 rot, Space spc) { a.gameObject.TfRot(rot, spc); }
		public static void TfRot(this Component a, Vector3 axis, float ang) { a.gameObject.TfRot(axis, ang); }
		public static void TfRot(this Component a, Vector3 axis, float ang, Space spc) { a.gameObject.TfRot(axis, ang, spc); }
		public static void TfRotAround(this Component a, Vector3 pnt, Vector3 axis, float ang) { a.gameObject.TfRotAround(pnt, axis, ang); }
		public static void TfTra(this Component a, float x, float y, float z) { a.gameObject.TfTra(x, y, z); }
		public static void TfTra(this Component a, float x, float y, float z, Space spc) { a.gameObject.TfTra(x, y, z, spc); }
		public static void TfTra(this Component a, float x, float y, float z, Transform tf) { a.gameObject.TfTra(x, y, z, tf); }
		public static void TfTra(this Component a, Vector3 tra) { a.gameObject.TfTra(tra); }
		public static void TfTra(this Component a, Vector3 tra, Space spc) { a.gameObject.TfTra(tra, spc); }
		public static void TfTra(this Component a, Vector3 tra, Transform tf) { a.gameObject.TfTra(tra, tf); }
		public static Transform Fnd(this Component a, string name) { return a.gameObject.Fnd(name); }
		public static GameObject FndGo(this Component a, string name) { return a.gameObject.FndGo(name); }
		public static T Fnd<T>(this Component a, string name) { return a.gameObject.Fnd<T>(name); }
		public static void SibIdx(this Component a, int i) { a.gameObject.SibIdx(i); }
		public static int SibIdx(this Component a) { return a.gameObject.SibIdx(); }
		public static void SibFist(this Component a) { a.gameObject.SibFist(); }
		public static void SibLast(this Component a) { a.gameObject.SibLast(); }
		public static void Par(this Component a, Transform parTf) { a.gameObject.Par(parTf); }
		public static void Par(this Component a, Transform parTf, bool isWorPosStays) { a.gameObject.Par(parTf, isWorPosStays); }
		public static Transform Child(this Component a, params int[] childs) { return a.gameObject.Child(childs); }
		public static Transform ChildName(this Component a, params string[] childs) { return a.gameObject.ChildName(childs); }
		public static Transform Child(this Component a, string childs = "") { return a.gameObject.Child(childs); }
		public static GameObject ChildGo(this Component a, params int[] childs) { return a.gameObject.ChildGo(childs); }
		public static GameObject ChildNameGo(this Component a, params string[] childs) { return a.gameObject.ChildNameGo(childs); }
		public static GameObject ChildGo(this Component a, string childs = "") { return a.gameObject.ChildGo(childs); }
		public static T Child<T>(this Component a, params int[] childs) { return a.gameObject.Child<T>(childs); }
		public static T ChildName<T>(this Component a, params string[] childs) { return a.gameObject.ChildName<T>(childs); }
		public static T Child<T>(this Component a, string childs = "") { return a.gameObject.Child<T>(childs); }
		public static void ChildActive(this Component a, bool active, params int[] childs) { a.gameObject.ChildActive(active, childs); }
		public static void ChildNameActive(this Component a, bool active, params string[] childs) { a.gameObject.ChildNameActive(active, childs); }
		public static void ChildActive(this Component a, bool active, string childs = "") { a.gameObject.ChildActive(active, childs); }
		public static void ChildHide(this Component a, params int[] childs) { a.gameObject.ChildHide(childs); }
		public static void ChildNameHide(this Component a, params string[] childs) { a.gameObject.ChildNameHide(childs); }
		public static void ChildHide(this Component a, string childs = "") { a.gameObject.ChildHide(childs); }
		public static void ChildShow(this Component a, params int[] childs) { a.gameObject.ChildShow(childs); }
		public static void ChildNameShow(this Component a, params string[] childs) { a.gameObject.ChildNameShow(childs); }
		public static void ChildShow(this Component a, string childs = "") { a.gameObject.ChildShow(childs); }
		public static Transform Par(this Component a, int par = 0) { return a.gameObject.Par(par); }
		public static Transform ParName(this Component a, string par) { return a.gameObject.ParName(par); }
		public static GameObject ParGo(this Component a, int par = 0) { return a.gameObject.ParGo(par); }
		public static GameObject ParNameGo(this Component a, string par) { return a.gameObject.ParNameGo(par); }
		public static T Par<T>(this Component a, int par = 0) { return a.gameObject.Par<T>(par); }
		public static T ParName<T>(this Component a, string par) { return a.gameObject.ParName<T>(par); }
		public static void ParActive(this Component a, bool active, int par = 0) { a.gameObject.ParActive(active, par); }
		public static void ParNameActive(this Component a, bool active, string par) { a.gameObject.ParNameActive(active, par); }
		public static void ParHide(this Component a, int par = 0) { a.gameObject.ParHide(par); }
		public static void ParNameHide(this Component a, string par) { a.gameObject.ParNameHide(par); }
		public static void ParShow(this Component a, int par = 0) { a.gameObject.ParShow(par); }
		public static void ParNameShow(this Component a, string par) { a.gameObject.ParNameShow(par); }
		public static Transform ParChild(this Component a, int par = 0, params int[] childs) { return a.gameObject.ParChild(par, childs); }
		public static Transform ParChildName(this Component a, string par, params string[] childs) { return a.gameObject.ParChildName(par, childs); }
		public static Transform ParChild(this Component a, int par = 0, string childs = "") { return a.gameObject.ParChild(par, childs); }
		public static GameObject ParChildGo(this Component a, int par = 0, params int[] childs) { return a.gameObject.ParChildGo(par, childs); }
		public static GameObject ParChildNameGo(this Component a, string par, params string[] childs) { return a.gameObject.ParChildNameGo(par, childs); }
		public static GameObject ParChildGo(this Component a, int par = 0, string childs = "") { return a.gameObject.ParChildGo(par, childs); }
		public static T ParChild<T>(this Component a, int par = 0, params int[] childs) { return a.gameObject.ParChild<T>(par, childs); }
		public static T ParChildName<T>(this Component a, string par, params string[] childs) { return a.gameObject.ParChildName<T>(par, childs); }
		public static T ParChild<T>(this Component a, int par = 0, string childs = "") { return a.gameObject.ParChild<T>(par, childs); }
		public static void ParChildActive(this Component a, bool active, int par = 0, params int[] childs) { a.gameObject.ParChildActive(active, par, childs); }
		public static void ParChildNameActive(this Component a, bool active, string par, params string[] childs) { a.gameObject.ParChildNameActive(active, par, childs); }
		public static void ParChildActive(this Component a, bool active, int par = 0, string childs = "") { a.gameObject.ParChildActive(active, par, childs); }
		public static void ParChildHide(this Component a, int par = 0, params int[] childs) { a.gameObject.ParChildHide(par, childs); }
		public static void ParChildNameHide(this Component a, string par, params string[] childs) { a.gameObject.ParChildNameHide(par, childs); }
		public static void ParChildHide(this Component a, int par = 0, string childs = "") { a.gameObject.ParChildHide(par, childs); }
		public static void ParChildShow(this Component a, int par = 0, params int[] childs) { a.gameObject.ParChildShow(par, childs); }
		public static void ParChildNameShow(this Component a, string par, params string[] childs) { a.gameObject.ParChildNameShow(par, childs); }
		public static void ParChildShow(this Component a, int par = 0, string childs = "") { a.gameObject.ParChildShow(par, childs); }
		public static Transform Sib(this Component a, int sib = 0) { return a.gameObject.Sib(sib); }
		public static Transform SibName(this Component a, string sib) { return a.gameObject.SibName(sib); }
		public static GameObject SibGo(this Component a, int sib = 0) { return a.gameObject.SibGo(sib); }
		public static GameObject SibNameGo(this Component a, string sib) { return a.gameObject.SibNameGo(sib); }
		public static T Sib<T>(this Component a, int sib = 0) { return a.gameObject.Sib<T>(sib); }
		public static T SibName<T>(this Component a, string sib) { return a.gameObject.SibName<T>(sib); }
		public static void SibActive(this Component a, bool active, int sib = 0) { a.gameObject.SibActive(active, sib); }
		public static void SibNameActive(this Component a, bool active, string sib) { a.gameObject.SibNameActive(active, sib); }
		public static void SibHide(this Component a, int sib = 0) { a.gameObject.SibHide(sib); }
		public static void SibNameHide(this Component a, string sib) { a.gameObject.SibNameHide(sib); }
		public static void SibShow(this Component a, int sib = 0) { a.gameObject.SibShow(sib); }
		public static void SibNameShow(this Component a, string sib) { a.gameObject.SibNameShow(sib); }
		public static Material RenMat(this Component a) { return a.gameObject.RenMat(); }
		public static void RenMat(this Component a, Material mat) { a.gameObject.RenMat(mat); }
		public static List<Material> RenMats(this Component a) { return a.gameObject.RenMats(); }
		public static void RenMatCol(this Component a, Color col) { a.gameObject.RenMatCol(col); }
		public static void RenMatCol(this Component a, string name, Color col) { a.gameObject.RenMatCol(name, col); }
		public static Color RenMatCol(this Component a) { return a.gameObject.RenMatCol(); }
		public static Color RenMatCol(this Component a, string name) { return a.gameObject.RenMatCol(name); }
		public static Material MatFindName(this Component a, string name) { return a.gameObject.MatFindName(name); }
		public static List<Material> MatList(this Component a) { return a.gameObject.MatList(); }
		public static List<Material> MatListTransparent(this Component a) { return a.gameObject.MatListTransparent(); }
		public static BoxCollider Bc(this Component a, Vector3 sz, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) { return a.gameObject.Bc(sz, cen, isT, mat, tp); }
		public static SphereCollider Sc(this Component a, float r, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) { return a.gameObject.Sc(r, cen, isT, mat, tp); }
		public static CapsuleCollider Cc(this Component a, float r, float h = 1, int dir = 1, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) { return a.gameObject.Cc(r, h, dir, cen, isT, mat, tp); }
		public static void ColIsT(this Component a, bool isT) { a.gameObject.ColIsT(isT); }
		public static bool ColIsT(this Component a) { return a.gameObject.ColIsT(); }
		public static void ColMat(this Component a, PhysicMaterial mat) { a.gameObject.ColMat(mat); }
		public static PhysicMaterial ColMat(this Component a) { return a.gameObject.ColMat(); }
		public static void BcCen(this Component a, Vector3 cen) { a.gameObject.BcCen(cen); }
		public static Vector3 BcCen(this Component a) { return a.gameObject.BcCen(); }
		public static void BcSz(this Component a, Vector3 sz) { a.gameObject.BcSz(sz); }
		public static Vector3 BcSz(this Component a) { return a.gameObject.BcSz(); }
		public static void ScCen(this Component a, Vector3 cen) { a.gameObject.ScCen(cen); }
		public static Vector3 ScCen(this Component a) { return a.gameObject.ScCen(); }
		public static void ScR(this Component a, float r) { a.gameObject.ScR(r); }
		public static float ScR(this Component a) { return a.gameObject.ScR(); }
		public static void CcCen(this Component a, Vector3 cen) { a.gameObject.CcCen(cen); }
		public static Vector3 CcCen(this Component a) { return a.gameObject.CcCen(); }
		public static void CcR(this Component a, float r) { a.gameObject.CcR(r); }
		public static float CcR(this Component a) { return a.gameObject.CcR(); }
		public static void CcH(this Component a, float h) { a.gameObject.CcH(h); }
		public static float CcH(this Component a) { return a.gameObject.CcH(); }
		public static void CcDir(this Component a, int dir) { a.gameObject.CcDir(dir); }
		public static int CcDir(this Component a) { return a.gameObject.CcDir(); }
		public static void McCon(this Component a, bool isCon) { a.gameObject.McCon(isCon); }
		public static bool McCon(this Component a) { return a.gameObject.McCon(); }
		public static void McMesh(this Component a, Mesh msh) { a.gameObject.McMesh(msh); }
		public static Mesh McMesh(this Component a) { return a.gameObject.McMesh(); }
		public static void WcM(this Component a, float m) { a.gameObject.WcM(m); }
		public static float WcM(this Component a) { return a.gameObject.WcM(); }
		public static void WcR(this Component a, float r) { a.gameObject.WcR(r); }
		public static float WcR(this Component a) { return a.gameObject.WcR(); }
		public static void WcCen(this Component a, Vector3 cen) { a.gameObject.WcCen(cen); }
		public static Vector3 WcCen(this Component a) { return a.gameObject.WcCen(); }
		public static void TrlTm(this Component a, float tm) { a.gameObject.TrlTm(tm); }
		public static float TrlTm(this Component a) { return a.gameObject.TrlTm(); }
		public static void TrlMinVerDis(this Component a, float minVerDis) { a.gameObject.TrlMinVerDis(minVerDis); }
		public static float TrlMinVerDis(this Component a) { return a.gameObject.TrlMinVerDis(); }
		public static void TrlAutoDes(this Component a, bool autoDes) { a.gameObject.TrlAutoDes(autoDes); }
		public static bool TrlAutoDes(this Component a) { return a.gameObject.TrlAutoDes(); }
		public static void TrlEm(this Component a, bool isEm) { a.gameObject.TrlEm(isEm); }
		public static bool TrlEm(this Component a) { return a.gameObject.TrlEm(); }
		public static void TrlCg(this Component a, Gradient cg) { a.gameObject.TrlCg(cg); }
		public static Gradient TrlCg(this Component a) { return a.gameObject.TrlCg(); }
		public static void TrlNumCorVer(this Component a, int numCorVer) { a.gameObject.TrlNumCorVer(numCorVer); }
		public static int TrlNumCorVer(this Component a) { return a.gameObject.TrlNumCorVer(); }
		public static ParticleSystem.MainModule PsMain(this Component a) { return a.gameObject.PsMain(); }
		public static void PsMainDur(this Component a, float dur) { a.gameObject.PsMainDur(dur); }
		public static void PsMainLoop(this Component a, bool loop) { a.gameObject.PsMainLoop(loop); }
		public static void PsMainPrewarm(this Component a, bool prewarm) { a.gameObject.PsMainPrewarm(prewarm); }
		public static void PsMainStaDelay(this Component a, ParticleSystem.MinMaxCurve delay, float mul) { a.gameObject.PsMainStaDelay(delay, mul); }
		public static void PsMainStaLt(this Component a, ParticleSystem.MinMaxCurve lt, float mul) { a.gameObject.PsMainStaLt(lt, mul); }
		public static void PsMainStaSpd(this Component a, ParticleSystem.MinMaxCurve spd, float mul) { a.gameObject.PsMainStaSpd(spd, mul); }
		public static void PsMainStaSz3D(this Component a, bool sz) { a.gameObject.PsMainStaSz3D(sz); }
		public static void PsMainStaSz(this Component a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSz(sz, mul); }
		public static void PsMainStaSzX(this Component a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSzX(sz, mul); }
		public static void PsMainStaSzY(this Component a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSzY(sz, mul); }
		public static void PsMainStaSzZ(this Component a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSzZ(sz, mul); }
		public static void PsMainStaRot3D(this Component a, bool rot) { a.gameObject.PsMainStaRot3D(rot); }
		public static void PsMainStaRot(this Component a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRot(rot, mul); }
		public static void PsMainStaRotX(this Component a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRotX(rot, mul); }
		public static void PsMainStaRotY(this Component a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRotY(rot, mul); }
		public static void PsMainStaRotZ(this Component a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRotZ(rot, mul); }
		public static void PsMainFlipRot(this Component a, float rot) { a.gameObject.PsMainFlipRot(rot); }
		public static void PsMainStaC(this Component a, ParticleSystem.MinMaxGradient c) { a.gameObject.PsMainStaC(c); }
		public static void PsMainGMod(this Component a, ParticleSystem.MinMaxCurve gMod, float mul) { a.gameObject.PsMainGMod(gMod, mul); }
		public static void PsMainSimSpc(this Component a, ParticleSystemSimulationSpace spc) { a.gameObject.PsMainSimSpc(spc); }
		public static void PsMainCusSimSpc(this Component a, Transform tf) { a.gameObject.PsMainCusSimSpc(tf); }
		public static void PsMainSimSpd(this Component a, float spd) { a.gameObject.PsMainSimSpd(spd); }
		public static void PsMainUseUnSclTm(this Component a, bool isUse) { a.gameObject.PsMainUseUnSclTm(isUse); }
		public static void PsMainDt(this Component a, bool isScl) { a.gameObject.PsMainDt(isScl); }
		public static void PsMainSclMd(this Component a, ParticleSystemScalingMode md) { a.gameObject.PsMainSclMd(md); }
		public static void PsMainPlayOnAwake(this Component a, bool playOnAwake) { a.gameObject.PsMainPlayOnAwake(playOnAwake); }
		public static void PsMainEmVelMd(this Component a, ParticleSystemEmitterVelocityMode md) { a.gameObject.PsMainEmVelMd(md); }
		public static void PsMainMaxP(this Component a, int maxP) { a.gameObject.PsMainMaxP(maxP); }
		public static void PsMainAutoRndSeed(this Component a, bool isUse) { a.gameObject.PsMainAutoRndSeed(isUse); }
		public static void PsMainRndSeed(this Component a, uint seed) { a.gameObject.PsMainRndSeed(seed); }
		public static void PsMainStopAct(this Component a, ParticleSystemStopAction act) { a.gameObject.PsMainStopAct(act); }
		public static void PsMainCulMd(this Component a, ParticleSystemCullingMode md) { a.gameObject.PsMainCulMd(md); }
		public static void PsMainRingBufMd(this Component a, ParticleSystemRingBufferMode md) { a.gameObject.PsMainRingBufMd(md); }
		public static void PsMainRingBufLoopRng(this Component a, Vector2 rng) { a.gameObject.PsMainRingBufLoopRng(rng); }
		public static ParticleSystem.EmissionModule PsEm(this Component a) { return a.gameObject.PsEm(); }
		public static void PsEmRot(this Component a, float tm) { a.gameObject.PsEmRot(tm); }
		public static void PsEmRot(this Component a, ParticleSystem.MinMaxCurve tm, float mul) { a.gameObject.PsEmRot(tm, mul); }
		public static void PsEmRod(this Component a, float dis) { a.gameObject.PsEmRod(dis); }
		public static void PsEmRod(this Component a, ParticleSystem.MinMaxCurve dis, float mul) { a.gameObject.PsEmRod(dis, mul); }
		public static void PsEmSetBursts(this Component a, params ParticleSystem.Burst[] bursts) { a.gameObject.PsEmSetBursts(bursts); }
		public static ParticleSystem.ShapeModule PsShp(this Component a) { return a.gameObject.PsShp(); }
		public static ParticleSystem.VelocityOverLifetimeModule PsVol(this Component a) { return a.gameObject.PsVol(); }
		public static void PsVolX(this Component a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsVolX(x, mul); }
		public static void PsVolY(this Component a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsVolY(y, mul); }
		public static void PsVolZ(this Component a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsVolZ(z, mul); }
		public static void PsVolOrbX(this Component a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsVolOrbX(x, mul); }
		public static void PsVolOrbY(this Component a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsVolOrbY(y, mul); }
		public static void PsVolOrbZ(this Component a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsVolOrbZ(z, mul); }
		public static void PsVolOrbOffX(this Component a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsVolOrbOffX(x, mul); }
		public static void PsVolOrbOffY(this Component a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsVolOrbOffY(y, mul); }
		public static void PsVolOrbOffZ(this Component a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsVolOrbOffZ(z, mul); }
		public static void PsVolRadial(this Component a, ParticleSystem.MinMaxCurve radial, float mul) { a.gameObject.PsVolRadial(radial, mul); }
		public static void PsVolSpdMod(this Component a, ParticleSystem.MinMaxCurve mod, float mul) { a.gameObject.PsVolSpdMod(mod, mul); }
		public static void PsVolSpc(this Component a, ParticleSystemSimulationSpace spc) { a.gameObject.PsVolSpc(spc); }
		public static ParticleSystem.LimitVelocityOverLifetimeModule PsLvol(this Component a) { return a.gameObject.PsLvol(); }
		public static void PsLvolLim(this Component a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLim(lim, mul); }
		public static void PsLvolLimX(this Component a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLimX(lim, mul); }
		public static void PsLvolLimY(this Component a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLimY(lim, mul); }
		public static void PsLvolLimZ(this Component a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLimZ(lim, mul); }
		public static void PsLvolDrag(this Component a, ParticleSystem.MinMaxCurve drag, float mul) { a.gameObject.PsLvolDrag(drag, mul); }
		public static void PsLvolSpc(this Component a, ParticleSystemSimulationSpace spc) { a.gameObject.PsLvolSpc(spc); }
		public static void PsLvolDampen(this Component a, float isDampen) { a.gameObject.PsLvolDampen(isDampen); }
		public static void PsLvolSepAxes(this Component a, bool isAxes) { a.gameObject.PsLvolSepAxes(isAxes); }
		public static void PsLvolMulDragByPSz(this Component a, bool isSz) { a.gameObject.PsLvolMulDragByPSz(isSz); }
		public static void PsLvolMulDragByPVel(this Component a, bool isVel) { a.gameObject.PsLvolMulDragByPVel(isVel); }
		public static ParticleSystem.InheritVelocityModule PsIv(this Component a) { return a.gameObject.PsIv(); }
		public static void PsIvMode(this Component a, ParticleSystemInheritVelocityMode md) { a.gameObject.PsIvMode(md); }
		public static void PsIvCrv(this Component a, ParticleSystem.MinMaxCurve crv, float mul) { a.gameObject.PsIvCrv(crv, mul); }
		public static ParticleSystem.ForceOverLifetimeModule PsFol(this Component a) { return a.gameObject.PsFol(); }
		public static void PsFolX(this Component a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsFolX(x, mul); }
		public static void PsFolY(this Component a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsFolY(y, mul); }
		public static void PsFolZ(this Component a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsFolZ(z, mul); }
		public static void PsFolSpc(this Component a, ParticleSystemSimulationSpace spc) { a.gameObject.PsFolSpc(spc); }
		public static void PsFolRnd(this Component a, bool isRnd) { a.gameObject.PsFolRnd(isRnd); }
		public static ParticleSystem.ColorOverLifetimeModule PsCol(this Component a) { return a.gameObject.PsCol(); }
		public static void PsColC(this Component a, ParticleSystem.MinMaxGradient c) { a.gameObject.PsColC(c); }
		public static ParticleSystem.ColorBySpeedModule PsCbs(this Component a) { return a.gameObject.PsCbs(); }
		public static void PsCbsC(this Component a, ParticleSystem.MinMaxGradient c) { a.gameObject.PsCbsC(c); }
		public static void PsCbsRng(this Component a, Vector2 rng) { a.gameObject.PsCbsRng(rng); }
		public static ParticleSystem.SizeOverLifetimeModule PsSol(this Component a) { return a.gameObject.PsSol(); }
		public static void PsSolSepAxes(this Component a, bool isAxes) { a.gameObject.PsSolSepAxes(isAxes); }
		public static void PsSolSz(this Component a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsSolSz(sz, mul); }
		public static void PsSolX(this Component a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsSolX(x, mul); }
		public static void PsSolY(this Component a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsSolY(y, mul); }
		public static void PsSolZ(this Component a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsSolZ(z, mul); }
		public static ParticleSystem.SizeBySpeedModule PsSbs(this Component a) { return a.gameObject.PsSbs(); }
		public static void PsSbsSepAxes(this Component a, bool isAxes) { a.gameObject.PsSbsSepAxes(isAxes); }
		public static void PsSbsSz(this Component a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsSbsSz(sz, mul); }
		public static void PsSbsX(this Component a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsSbsX(x, mul); }
		public static void PsSbsY(this Component a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsSbsY(y, mul); }
		public static void PsSbsZ(this Component a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsSbsZ(z, mul); }
		public static void PsSbsRng(this Component a, Vector2 rng) { a.gameObject.PsSbsRng(rng); }
		public static ParticleSystem.RotationOverLifetimeModule PsRol(this Component a) { return a.gameObject.PsRol(); }
		public static void PsRolSepAxes(this Component a, bool isAxes) { a.gameObject.PsRolSepAxes(isAxes); }
		public static void PsRolX(this Component a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsRolX(x, mul); }
		public static void PsRolY(this Component a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsRolY(y, mul); }
		public static void PsRolZ(this Component a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsRolZ(z, mul); }
		public static ParticleSystem.RotationBySpeedModule PsRbs(this Component a) { return a.gameObject.PsRbs(); }
		public static void PsRbsSepAxes(this Component a, bool isAxes) { a.gameObject.PsRbsSepAxes(isAxes); }
		public static void PsRbsX(this Component a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsRbsX(x, mul); }
		public static void PsRbsY(this Component a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsRbsY(y, mul); }
		public static void PsRbsZ(this Component a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsRbsZ(z, mul); }
		public static void PsRbsRng(this Component a, Vector2 rng) { a.gameObject.PsRbsRng(rng); }
		public static ParticleSystem.ExternalForcesModule PsEf(this Component a) { return a.gameObject.PsEf(); }
		public static void PsEfMulCrv(this Component a, ParticleSystem.MinMaxCurve crv, float mul) { a.gameObject.PsEfMulCrv(crv, mul); }
		public static void PsEfInfFilter(this Component a, ParticleSystemGameObjectFilter filter) { a.gameObject.PsEfInfFilter(filter); }
		public static void PsEfInfMask(this Component a, LayerMask lm) { a.gameObject.PsEfInfMask(lm); }
		public static ParticleSystem.NoiseModule PsNoise(this Component a) { return a.gameObject.PsNoise(); }
		public static ParticleSystem.CollisionModule PsCollision(this Component a) { return a.gameObject.PsCollision(); }
		public static ParticleSystem.TriggerModule PsTrigger(this Component a) { return a.gameObject.PsTrigger(); }
		public static ParticleSystem.SubEmittersModule PsSubEm(this Component a) { return a.gameObject.PsSubEm(); }
		public static ParticleSystem.TextureSheetAnimationModule PsTexSheetAnim(this Component a) { return a.gameObject.PsTexSheetAnim(); }
		public static ParticleSystem.LightsModule PsLights(this Component a) { return a.gameObject.PsLights(); }
		public static ParticleSystem.TrailModule PsTrails(this Component a) { return a.gameObject.PsTrails(); }
		public static ParticleSystem.CustomDataModule PsCusDat(this Component a) { return a.gameObject.PsCusDat(); }
		public static void RbG(this Component a, bool useGravity = true) { a.gameObject.RbG(useGravity); }
		public static void RbNoG(this Component a) { a.gameObject.RbNoG(); }
		public static void RbK(this Component a, bool isKinematic = true) { a.gameObject.RbK(isKinematic); }
		public static void RbNoK(this Component a) { a.gameObject.RbNoK(); }
		public static void RbInt(this Component a, int interpolate) { a.gameObject.RbInt(interpolate); }
		public static void RbInt(this Component a, RigidbodyInterpolation interpolate) { a.gameObject.RbInt(interpolate); }
		public static void RbInt0(this Component a) { a.gameObject.RbInt0(); }
		public static void RbIntInt(this Component a) { a.gameObject.RbIntInt(); }
		public static void RbIntExt(this Component a) { a.gameObject.RbIntExt(); }
		public static void RbColDet(this Component a, int collisionDetection) { a.gameObject.RbColDet(collisionDetection); }
		public static void RbColDet(this Component a, CollisionDetectionMode collisionDetection) { a.gameObject.RbColDet(collisionDetection); }
		public static void RbColDetDis(this Component a) { a.gameObject.RbColDetDis(); }
		public static void RbColDetCon(this Component a) { a.gameObject.RbColDetCon(); }
		public static void RbColDetConD(this Component a) { a.gameObject.RbColDetConD(); }
		public static void RbColDetConS(this Component a) { a.gameObject.RbColDetConS(); }
		public static void RbCon(this Component a, bool isFrzPosX = false, bool isFrzPosY = false, bool isFrzPosZ = false, bool isFrzRotX = false, bool isFrzRotY = false, bool isFrzRotZ = false) { a.gameObject.RbCon(isFrzPosX, isFrzPosY, isFrzPosZ, isFrzRotX, isFrzRotY, isFrzRotZ); }
		public static void RbCon0(this Component a) { a.gameObject.RbCon0(); }
		public static void RbConFrzPos(this Component a) { a.gameObject.RbConFrzPos(); }
		public static void RbConFrzRot(this Component a) { a.gameObject.RbConFrzRot(); }
		public static void RbConFrzAll(this Component a) { a.gameObject.RbConFrzAll(); }
		public static void RbAv0(this Component a) { a.gameObject.RbAv0(); }
		public static void RbAv(this Component a, Vector3 v) { a.gameObject.RbAv(v); }
		public static void RbAvx(this Component a, float x) { a.gameObject.RbAvx(x); }
		public static void RbAvy(this Component a, float y) { a.gameObject.RbAvy(y); }
		public static void RbAvz(this Component a, float z) { a.gameObject.RbAvz(z); }
		public static void RbAvxy(this Component a, float x, float y) { a.gameObject.RbAvxy(x, y); }
		public static void RbAvxz(this Component a, float x, float z) { a.gameObject.RbAvxz(x, z); }
		public static void RbAvyz(this Component a, float y, float z) { a.gameObject.RbAvyz(y, z); }
		public static void RbAv(this Component a, float x, float y, float z) { a.gameObject.RbAv(x, y, z); }
		public static void RbV0(this Component a) { a.gameObject.RbV0(); }
		public static void RbV(this Component a, Vector3 v) { a.gameObject.RbV(v); }
		public static void RbVx(this Component a, float x) { a.gameObject.RbVx(x); }
		public static void RbVy(this Component a, float y) { a.gameObject.RbVy(y); }
		public static void RbVz(this Component a, float z) { a.gameObject.RbVz(z); }
		public static void RbVxy(this Component a, float x, float y) { a.gameObject.RbVxy(x, y); }
		public static void RbVxz(this Component a, float x, float z) { a.gameObject.RbVxz(x, z); }
		public static void RbVyz(this Component a, float y, float z) { a.gameObject.RbVyz(y, z); }
		public static void RbV(this Component a, float x, float y, float z) { a.gameObject.RbV(x, y, z); }
		public static T Gc<T>(this Collision a) { return a.gameObject.Gc<T>(); }
		public static T Gc<T>(this Collision a, GcTp tp) where T : Component { return a.gameObject.Gc<T>(tp); }
		public static Text Txt(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Txt(tp); }
		public static Button Btn(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Btn(tp); }
		public static Image Img(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Img(tp); }
		public static Toggle Tgl(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Tgl(tp); }
		public static Slider Sld(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Sld(tp); }
		public static Dropdown Drp(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Drp(tp); }
		public static InputField Inp(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Inp(tp); }
		public static ColorPicker Cp(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Cp(tp); }
		public static TextMesh Tm(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Tm(tp); }
		public static TextMeshPro Tmp(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Tmp(tp); }
		public static Rigidbody Rb(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Rb(tp); }
		public static Rigidbody2D Rb2(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Rb2(tp); }
		public static Renderer Ren(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Ren(tp); }
		public static MeshFilter Mf(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Mf(tp); }
		public static MeshRenderer Mr(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Mr(tp); }
		public static Collider Col(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Col(tp); }
		public static BoxCollider Bc(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Bc(tp); }
		public static SphereCollider Sc(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Sc(tp); }
		public static CapsuleCollider Cc(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Cc(tp); }
		public static MeshCollider Mc(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Mc(tp); }
		public static WheelCollider Wc(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Wc(tp); }
		public static ParticleSystem Ps(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Ps(tp); }
		public static ParticleSystemRenderer Psr(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Psr(tp); }
		public static TrailRenderer Trl(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Trl(tp); }
		public static Animator An(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.An(tp); }
		public static RectTransform Rt(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Rt(tp); }
		public static LineRenderer Lr(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Lr(tp); }
		public static Shadow Shadow(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Shadow(tp); }
		public static Outline Outline(this Collision a, GcTp tp = GcTp.Get) { return a.gameObject.Outline(tp); }
		public static void Active(this Collision a, bool active) { a.gameObject.Active(active); }
		public static void Show(this Collision a) { a.gameObject.Show(); }
		public static void Hide(this Collision a) { a.gameObject.Hide(); }
		public static bool IsActive(this Collision a) { return a.gameObject.IsActive(); }
		public static bool Is<T>(this Collision a) { return a.gameObject.Is<T>(); }
		public static T Ac<T>(this Collision a) where T : Component { return a.gameObject.Ac<T>(); }
		public static T Gcic<T>(this Collision a) { return a.gameObject.Gcic<T>(); }
		public static T Gcip<T>(this Collision a) { return a.gameObject.Gcip<T>(); }
		public static T[] Gcs<T>(this Collision a) { return a.gameObject.Gcs<T>(); }
		public static T[] Gcsic<T>(this Collision a) { return a.gameObject.Gcsic<T>(); }
		public static T[] Gcsip<T>(this Collision a) { return a.gameObject.Gcsip<T>(); }
		public static List<T> Gca<T>(this Collision a) { return a.gameObject.Gca<T>(); }
		public static List<Transform> Childs(this Collision a) { return a.gameObject.Childs(); }
		public static List<GameObject> ChildGos(this Collision a) { return a.gameObject.ChildGos(); }
		public static List<T> Childs<T>(this Collision a) { return a.gameObject.Childs<T>(); }
		public static void DstChilds<T>(this Collision a, int sta = 0, int end = -1) where T : Object { a.gameObject.DstChilds<T>(sta, end); }
		public static void DstChilds(this Collision a, int sta = 0, int end = -1) { a.gameObject.DstChilds(sta, end); }
		public static Color GetCol(this Collision a, Color col = default) { return a.gameObject.GetCol(col); }
		public static void SetCol(this Collision a, Color col) { a.gameObject.SetCol(col); }
		public static void ColA(this Collision a, float alpha) { a.gameObject.ColA(alpha); }
		public static string GetText(this Collision a, string text = "") { return a.gameObject.GetText(text); }
		public static void SetText(this Collision a, string text) { a.gameObject.SetText(text); }
		public static bool Tag(this Collision a, string tag) { return a.gameObject.Tag(tag); }
		public static bool Tag(this Collision a, Tag tag) { return a.gameObject.Tag(tag); }
		public static Vector3 Tp(this Collision a) { return a.gameObject.Tp(); }
		public static void Tp(this Collision a, Vector3 v) { a.gameObject.Tp(v); }
		public static void Tp(this Collision a, float x, float y, float z) { a.gameObject.Tp(x, y, z); }
		public static void TpX(this Collision a, float x) { a.gameObject.TpX(x); }
		public static void TpY(this Collision a, float y) { a.gameObject.TpY(y); }
		public static void TpZ(this Collision a, float z) { a.gameObject.TpZ(z); }
		public static void TpXy(this Collision a, float x, float y) { a.gameObject.TpXy(x, y); }
		public static void TpXz(this Collision a, float x, float z) { a.gameObject.TpXz(x, z); }
		public static void TpYz(this Collision a, float y, float z) { a.gameObject.TpYz(y, z); }
		public static void TpD(this Collision a, Vector3 v) { a.gameObject.TpD(v); }
		public static void TpD(this Collision a, float x, float y, float z) { a.gameObject.TpD(x, y, z); }
		public static void TpDx(this Collision a, float x) { a.gameObject.TpDx(x); }
		public static void TpDy(this Collision a, float y) { a.gameObject.TpDy(y); }
		public static void TpDz(this Collision a, float z) { a.gameObject.TpDz(z); }
		public static void TpDxy(this Collision a, float x, float y) { a.gameObject.TpDxy(x, y); }
		public static void TpDxz(this Collision a, float x, float z) { a.gameObject.TpDxz(x, z); }
		public static void TpDyz(this Collision a, float y, float z) { a.gameObject.TpDyz(y, z); }
		public static Vector3 Tlp(this Collision a) { return a.gameObject.Tlp(); }
		public static void Tlp(this Collision a, Vector3 v) { a.gameObject.Tlp(v); }
		public static void Tlp(this Collision a, float x, float y, float z) { a.gameObject.Tlp(x, y, z); }
		public static void TlpX(this Collision a, float x) { a.gameObject.TlpX(x); }
		public static void TlpY(this Collision a, float y) { a.gameObject.TlpY(y); }
		public static void TlpZ(this Collision a, float z) { a.gameObject.TlpZ(z); }
		public static void TlpXy(this Collision a, float x, float y) { a.gameObject.TlpXy(x, y); }
		public static void TlpXz(this Collision a, float x, float z) { a.gameObject.TlpXz(x, z); }
		public static void TlpYz(this Collision a, float y, float z) { a.gameObject.TlpYz(y, z); }
		public static void TlpD(this Collision a, Vector3 v) { a.gameObject.TlpD(v); }
		public static void TlpD(this Collision a, float x, float y, float z) { a.gameObject.TlpD(x, y, z); }
		public static void TlpDx(this Collision a, float x) { a.gameObject.TlpDx(x); }
		public static void TlpDy(this Collision a, float y) { a.gameObject.TlpDy(y); }
		public static void TlpDz(this Collision a, float z) { a.gameObject.TlpDz(z); }
		public static void TlpDxy(this Collision a, float x, float y) { a.gameObject.TlpDxy(x, y); }
		public static void TlpDxz(this Collision a, float x, float z) { a.gameObject.TlpDxz(x, z); }
		public static void TlpDyz(this Collision a, float y, float z) { a.gameObject.TlpDyz(y, z); }
		public static Quaternion Tr(this Collision a) { return a.gameObject.Tr(); }
		public static void Tr(this Collision a, Quaternion v) { a.gameObject.Tr(v); }
		public static Quaternion Tlr(this Collision a) { return a.gameObject.Tlr(); }
		public static void Tlr(this Collision a, Quaternion v) { a.gameObject.Tlr(v); }
		public static Vector3 Ts(this Collision a) { return a.gameObject.Ts(); }
		public static void Ts(this Collision a, float x, float y, float z) { a.gameObject.Ts(x, y, z); }
		public static void TsX(this Collision a, float x) { a.gameObject.TsX(x); }
		public static void TsY(this Collision a, float y) { a.gameObject.TsY(y); }
		public static void TsZ(this Collision a, float z) { a.gameObject.TsZ(z); }
		public static void TsXy(this Collision a, float x, float y) { a.gameObject.TsXy(x, y); }
		public static void TsXz(this Collision a, float x, float z) { a.gameObject.TsXz(x, z); }
		public static void TsYz(this Collision a, float y, float z) { a.gameObject.TsYz(y, z); }
		public static void TsD(this Collision a, Vector3 v) { a.gameObject.TsD(v); }
		public static void TsD(this Collision a, float x, float y, float z) { a.gameObject.TsD(x, y, z); }
		public static void TsDx(this Collision a, float x) { a.gameObject.TsDx(x); }
		public static void TsDy(this Collision a, float y) { a.gameObject.TsDy(y); }
		public static void TsDz(this Collision a, float z) { a.gameObject.TsDz(z); }
		public static void TsDxy(this Collision a, float x, float y) { a.gameObject.TsDxy(x, y); }
		public static void TsDxz(this Collision a, float x, float z) { a.gameObject.TsDxz(x, z); }
		public static void TsDyz(this Collision a, float y, float z) { a.gameObject.TsDyz(y, z); }
		public static Vector3 Tls(this Collision a) { return a.gameObject.Tls(); }
		public static void Tls(this Collision a, Vector3 v) { a.gameObject.Tls(v); }
		public static void Tls(this Collision a, float x, float y, float z) { a.gameObject.Tls(x, y, z); }
		public static void TlsX(this Collision a, float x) { a.gameObject.TlsX(x); }
		public static void TlsY(this Collision a, float y) { a.gameObject.TlsY(y); }
		public static void TlsZ(this Collision a, float z) { a.gameObject.TlsZ(z); }
		public static void TlsXy(this Collision a, float x, float y) { a.gameObject.TlsXy(x, y); }
		public static void TlsXz(this Collision a, float x, float z) { a.gameObject.TlsXz(x, z); }
		public static void TlsYz(this Collision a, float y, float z) { a.gameObject.TlsYz(y, z); }
		public static void TlsD(this Collision a, Vector3 v) { a.gameObject.TlsD(v); }
		public static void TlsD(this Collision a, float x, float y, float z) { a.gameObject.TlsD(x, y, z); }
		public static void TlsDx(this Collision a, float x) { a.gameObject.TlsDx(x); }
		public static void TlsDy(this Collision a, float y) { a.gameObject.TlsDy(y); }
		public static void TlsDz(this Collision a, float z) { a.gameObject.TlsDz(z); }
		public static void TlsDxy(this Collision a, float x, float y) { a.gameObject.TlsDxy(x, y); }
		public static void TlsDxz(this Collision a, float x, float z) { a.gameObject.TlsDxz(x, z); }
		public static void TlsDyz(this Collision a, float y, float z) { a.gameObject.TlsDyz(y, z); }
		public static Vector3 Te(this Collision a) { return a.gameObject.Te(); }
		public static void Te(this Collision a, Vector3 v) { a.gameObject.Te(v); }
		public static void Te(this Collision a, float x, float y, float z) { a.gameObject.Te(x, y, z); }
		public static void TeX(this Collision a, float x) { a.gameObject.TeX(x); }
		public static void TeY(this Collision a, float y) { a.gameObject.TeY(y); }
		public static void TeZ(this Collision a, float z) { a.gameObject.TeZ(z); }
		public static void TeXy(this Collision a, float x, float y) { a.gameObject.TeXy(x, y); }
		public static void TeXz(this Collision a, float x, float z) { a.gameObject.TeXz(x, z); }
		public static void TeYz(this Collision a, float y, float z) { a.gameObject.TeYz(y, z); }
		public static void TeD(this Collision a, Vector3 v) { a.gameObject.TeD(v); }
		public static void TeD(this Collision a, float x, float y, float z) { a.gameObject.TeD(x, y, z); }
		public static void TeDx(this Collision a, float x) { a.gameObject.TeDx(x); }
		public static void TeDy(this Collision a, float y) { a.gameObject.TeDy(y); }
		public static void TeDz(this Collision a, float z) { a.gameObject.TeDz(z); }
		public static void TeDxy(this Collision a, float x, float y) { a.gameObject.TeDxy(x, y); }
		public static void TeDxz(this Collision a, float x, float z) { a.gameObject.TeDxz(x, z); }
		public static void TeDyz(this Collision a, float y, float z) { a.gameObject.TeDyz(y, z); }
		public static Vector3 Tle(this Collision a) { return a.gameObject.Tle(); }
		public static void Tle(this Collision a, Vector3 v) { a.gameObject.Tle(v); }
		public static void Tle(this Collision a, float x, float y, float z) { a.gameObject.Tle(x, y, z); }
		public static void TleX(this Collision a, float x) { a.gameObject.TleX(x); }
		public static void TleY(this Collision a, float y) { a.gameObject.TleY(y); }
		public static void TleZ(this Collision a, float z) { a.gameObject.TleZ(z); }
		public static void TleXy(this Collision a, float x, float y) { a.gameObject.TleXy(x, y); }
		public static void TleXz(this Collision a, float x, float z) { a.gameObject.TleXz(x, z); }
		public static void TleYz(this Collision a, float y, float z) { a.gameObject.TleYz(y, z); }
		public static void TleD(this Collision a, Vector3 v) { a.gameObject.TleD(v); }
		public static void TleD(this Collision a, float x, float y, float z) { a.gameObject.TleD(x, y, z); }
		public static void TleDx(this Collision a, float x) { a.gameObject.TleDx(x); }
		public static void TleDy(this Collision a, float y) { a.gameObject.TleDy(y); }
		public static void TleDz(this Collision a, float z) { a.gameObject.TleDz(z); }
		public static void TleDxy(this Collision a, float x, float y) { a.gameObject.TleDxy(x, y); }
		public static void TleDxz(this Collision a, float x, float z) { a.gameObject.TleDxz(x, z); }
		public static void TleDyz(this Collision a, float y, float z) { a.gameObject.TleDyz(y, z); }
		public static int Tcc(this Collision a) { return a.gameObject.Tcc(); }
		public static Vector3 TfDir(this Collision a, float x, float y, float z) { return a.gameObject.TfDir(x, y, z); }
		public static Vector3 TfDir(this Collision a, Vector3 dir) { return a.gameObject.TfDir(dir); }
		public static Vector3 TfInvDir(this Collision a, float x, float y, float z) { return a.gameObject.TfInvDir(x, y, z); }
		public static Vector3 TfInvDir(this Collision a, Vector3 dir) { return a.gameObject.TfInvDir(dir); }
		public static Vector3 TfPnt(this Collision a, float x, float y, float z) { return a.gameObject.TfPnt(x, y, z); }
		public static Vector3 TfPnt(this Collision a, Vector3 pnt) { return a.gameObject.TfPnt(pnt); }
		public static Vector3 TfInvPnt(this Collision a, float x, float y, float z) { return a.gameObject.TfInvPnt(x, y, z); }
		public static Vector3 TfInvPnt(this Collision a, Vector3 pnt) { return a.gameObject.TfInvPnt(pnt); }
		public static Vector3 TfVec(this Collision a, float x, float y, float z) { return a.gameObject.TfVec(x, y, z); }
		public static Vector3 TfVec(this Collision a, Vector3 vec) { return a.gameObject.TfVec(vec); }
		public static Vector3 TfInvVec(this Collision a, float x, float y, float z) { return a.gameObject.TfInvVec(x, y, z); }
		public static Vector3 TfInvVec(this Collision a, Vector3 vec) { return a.gameObject.TfInvVec(vec); }
		public static void TfLookAt(this Collision a, Transform tf) { a.gameObject.TfLookAt(tf); }
		public static void TfLookAt(this Collision a, Transform tf, Vector3 up) { a.gameObject.TfLookAt(tf, up); }
		public static void TfLookAt(this Collision a, Vector3 pos) { a.gameObject.TfLookAt(pos); }
		public static void TfLookAt(this Collision a, Vector3 pos, Vector3 up) { a.gameObject.TfLookAt(pos, up); }
		public static void TfRot(this Collision a, float x, float y, float z) { a.gameObject.TfRot(x, y, z); }
		public static void TfRot(this Collision a, float x, float y, float z, Space spc) { a.gameObject.TfRot(x, y, z, spc); }
		public static void TfRot(this Collision a, Vector3 rot) { a.gameObject.TfRot(rot); }
		public static void TfRot(this Collision a, Vector3 rot, Space spc) { a.gameObject.TfRot(rot, spc); }
		public static void TfRot(this Collision a, Vector3 axis, float ang) { a.gameObject.TfRot(axis, ang); }
		public static void TfRot(this Collision a, Vector3 axis, float ang, Space spc) { a.gameObject.TfRot(axis, ang, spc); }
		public static void TfRotAround(this Collision a, Vector3 pnt, Vector3 axis, float ang) { a.gameObject.TfRotAround(pnt, axis, ang); }
		public static void TfTra(this Collision a, float x, float y, float z) { a.gameObject.TfTra(x, y, z); }
		public static void TfTra(this Collision a, float x, float y, float z, Space spc) { a.gameObject.TfTra(x, y, z, spc); }
		public static void TfTra(this Collision a, float x, float y, float z, Transform tf) { a.gameObject.TfTra(x, y, z, tf); }
		public static void TfTra(this Collision a, Vector3 tra) { a.gameObject.TfTra(tra); }
		public static void TfTra(this Collision a, Vector3 tra, Space spc) { a.gameObject.TfTra(tra, spc); }
		public static void TfTra(this Collision a, Vector3 tra, Transform tf) { a.gameObject.TfTra(tra, tf); }
		public static Transform Fnd(this Collision a, string name) { return a.gameObject.Fnd(name); }
		public static GameObject FndGo(this Collision a, string name) { return a.gameObject.FndGo(name); }
		public static T Fnd<T>(this Collision a, string name) { return a.gameObject.Fnd<T>(name); }
		public static void SibIdx(this Collision a, int i) { a.gameObject.SibIdx(i); }
		public static int SibIdx(this Collision a) { return a.gameObject.SibIdx(); }
		public static void SibFist(this Collision a) { a.gameObject.SibFist(); }
		public static void SibLast(this Collision a) { a.gameObject.SibLast(); }
		public static void Par(this Collision a, Transform parTf) { a.gameObject.Par(parTf); }
		public static void Par(this Collision a, Transform parTf, bool isWorPosStays) { a.gameObject.Par(parTf, isWorPosStays); }
		public static Transform Child(this Collision a, params int[] childs) { return a.gameObject.Child(childs); }
		public static Transform ChildName(this Collision a, params string[] childs) { return a.gameObject.ChildName(childs); }
		public static Transform Child(this Collision a, string childs = "") { return a.gameObject.Child(childs); }
		public static GameObject ChildGo(this Collision a, params int[] childs) { return a.gameObject.ChildGo(childs); }
		public static GameObject ChildNameGo(this Collision a, params string[] childs) { return a.gameObject.ChildNameGo(childs); }
		public static GameObject ChildGo(this Collision a, string childs = "") { return a.gameObject.ChildGo(childs); }
		public static T Child<T>(this Collision a, params int[] childs) { return a.gameObject.Child<T>(childs); }
		public static T ChildName<T>(this Collision a, params string[] childs) { return a.gameObject.ChildName<T>(childs); }
		public static T Child<T>(this Collision a, string childs = "") { return a.gameObject.Child<T>(childs); }
		public static void ChildActive(this Collision a, bool active, params int[] childs) { a.gameObject.ChildActive(active, childs); }
		public static void ChildNameActive(this Collision a, bool active, params string[] childs) { a.gameObject.ChildNameActive(active, childs); }
		public static void ChildActive(this Collision a, bool active, string childs = "") { a.gameObject.ChildActive(active, childs); }
		public static void ChildHide(this Collision a, params int[] childs) { a.gameObject.ChildHide(childs); }
		public static void ChildNameHide(this Collision a, params string[] childs) { a.gameObject.ChildNameHide(childs); }
		public static void ChildHide(this Collision a, string childs = "") { a.gameObject.ChildHide(childs); }
		public static void ChildShow(this Collision a, params int[] childs) { a.gameObject.ChildShow(childs); }
		public static void ChildNameShow(this Collision a, params string[] childs) { a.gameObject.ChildNameShow(childs); }
		public static void ChildShow(this Collision a, string childs = "") { a.gameObject.ChildShow(childs); }
		public static Transform Par(this Collision a, int par = 0) { return a.gameObject.Par(par); }
		public static Transform ParName(this Collision a, string par) { return a.gameObject.ParName(par); }
		public static GameObject ParGo(this Collision a, int par = 0) { return a.gameObject.ParGo(par); }
		public static GameObject ParNameGo(this Collision a, string par) { return a.gameObject.ParNameGo(par); }
		public static T Par<T>(this Collision a, int par = 0) { return a.gameObject.Par<T>(par); }
		public static T ParName<T>(this Collision a, string par) { return a.gameObject.ParName<T>(par); }
		public static void ParActive(this Collision a, bool active, int par = 0) { a.gameObject.ParActive(active, par); }
		public static void ParNameActive(this Collision a, bool active, string par) { a.gameObject.ParNameActive(active, par); }
		public static void ParHide(this Collision a, int par = 0) { a.gameObject.ParHide(par); }
		public static void ParNameHide(this Collision a, string par) { a.gameObject.ParNameHide(par); }
		public static void ParShow(this Collision a, int par = 0) { a.gameObject.ParShow(par); }
		public static void ParNameShow(this Collision a, string par) { a.gameObject.ParNameShow(par); }
		public static Transform ParChild(this Collision a, int par = 0, params int[] childs) { return a.gameObject.ParChild(par, childs); }
		public static Transform ParChildName(this Collision a, string par, params string[] childs) { return a.gameObject.ParChildName(par, childs); }
		public static Transform ParChild(this Collision a, int par = 0, string childs = "") { return a.gameObject.ParChild(par, childs); }
		public static GameObject ParChildGo(this Collision a, int par = 0, params int[] childs) { return a.gameObject.ParChildGo(par, childs); }
		public static GameObject ParChildNameGo(this Collision a, string par, params string[] childs) { return a.gameObject.ParChildNameGo(par, childs); }
		public static GameObject ParChildGo(this Collision a, int par = 0, string childs = "") { return a.gameObject.ParChildGo(par, childs); }
		public static T ParChild<T>(this Collision a, int par = 0, params int[] childs) { return a.gameObject.ParChild<T>(par, childs); }
		public static T ParChildName<T>(this Collision a, string par, params string[] childs) { return a.gameObject.ParChildName<T>(par, childs); }
		public static T ParChild<T>(this Collision a, int par = 0, string childs = "") { return a.gameObject.ParChild<T>(par, childs); }
		public static void ParChildActive(this Collision a, bool active, int par = 0, params int[] childs) { a.gameObject.ParChildActive(active, par, childs); }
		public static void ParChildNameActive(this Collision a, bool active, string par, params string[] childs) { a.gameObject.ParChildNameActive(active, par, childs); }
		public static void ParChildActive(this Collision a, bool active, int par = 0, string childs = "") { a.gameObject.ParChildActive(active, par, childs); }
		public static void ParChildHide(this Collision a, int par = 0, params int[] childs) { a.gameObject.ParChildHide(par, childs); }
		public static void ParChildNameHide(this Collision a, string par, params string[] childs) { a.gameObject.ParChildNameHide(par, childs); }
		public static void ParChildHide(this Collision a, int par = 0, string childs = "") { a.gameObject.ParChildHide(par, childs); }
		public static void ParChildShow(this Collision a, int par = 0, params int[] childs) { a.gameObject.ParChildShow(par, childs); }
		public static void ParChildNameShow(this Collision a, string par, params string[] childs) { a.gameObject.ParChildNameShow(par, childs); }
		public static void ParChildShow(this Collision a, int par = 0, string childs = "") { a.gameObject.ParChildShow(par, childs); }
		public static Transform Sib(this Collision a, int sib = 0) { return a.gameObject.Sib(sib); }
		public static Transform SibName(this Collision a, string sib) { return a.gameObject.SibName(sib); }
		public static GameObject SibGo(this Collision a, int sib = 0) { return a.gameObject.SibGo(sib); }
		public static GameObject SibNameGo(this Collision a, string sib) { return a.gameObject.SibNameGo(sib); }
		public static T Sib<T>(this Collision a, int sib = 0) { return a.gameObject.Sib<T>(sib); }
		public static T SibName<T>(this Collision a, string sib) { return a.gameObject.SibName<T>(sib); }
		public static void SibActive(this Collision a, bool active, int sib = 0) { a.gameObject.SibActive(active, sib); }
		public static void SibNameActive(this Collision a, bool active, string sib) { a.gameObject.SibNameActive(active, sib); }
		public static void SibHide(this Collision a, int sib = 0) { a.gameObject.SibHide(sib); }
		public static void SibNameHide(this Collision a, string sib) { a.gameObject.SibNameHide(sib); }
		public static void SibShow(this Collision a, int sib = 0) { a.gameObject.SibShow(sib); }
		public static void SibNameShow(this Collision a, string sib) { a.gameObject.SibNameShow(sib); }
		public static Material RenMat(this Collision a) { return a.gameObject.RenMat(); }
		public static void RenMat(this Collision a, Material mat) { a.gameObject.RenMat(mat); }
		public static List<Material> RenMats(this Collision a) { return a.gameObject.RenMats(); }
		public static void RenMatCol(this Collision a, Color col) { a.gameObject.RenMatCol(col); }
		public static void RenMatCol(this Collision a, string name, Color col) { a.gameObject.RenMatCol(name, col); }
		public static Color RenMatCol(this Collision a) { return a.gameObject.RenMatCol(); }
		public static Color RenMatCol(this Collision a, string name) { return a.gameObject.RenMatCol(name); }
		public static Material MatFindName(this Collision a, string name) { return a.gameObject.MatFindName(name); }
		public static List<Material> MatList(this Collision a) { return a.gameObject.MatList(); }
		public static List<Material> MatListTransparent(this Collision a) { return a.gameObject.MatListTransparent(); }
		public static BoxCollider Bc(this Collision a, Vector3 sz, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) { return a.gameObject.Bc(sz, cen, isT, mat, tp); }
		public static SphereCollider Sc(this Collision a, float r, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) { return a.gameObject.Sc(r, cen, isT, mat, tp); }
		public static CapsuleCollider Cc(this Collision a, float r, float h = 1, int dir = 1, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) { return a.gameObject.Cc(r, h, dir, cen, isT, mat, tp); }
		public static void ColIsT(this Collision a, bool isT) { a.gameObject.ColIsT(isT); }
		public static bool ColIsT(this Collision a) { return a.gameObject.ColIsT(); }
		public static void ColMat(this Collision a, PhysicMaterial mat) { a.gameObject.ColMat(mat); }
		public static PhysicMaterial ColMat(this Collision a) { return a.gameObject.ColMat(); }
		public static void BcCen(this Collision a, Vector3 cen) { a.gameObject.BcCen(cen); }
		public static Vector3 BcCen(this Collision a) { return a.gameObject.BcCen(); }
		public static void BcSz(this Collision a, Vector3 sz) { a.gameObject.BcSz(sz); }
		public static Vector3 BcSz(this Collision a) { return a.gameObject.BcSz(); }
		public static void ScCen(this Collision a, Vector3 cen) { a.gameObject.ScCen(cen); }
		public static Vector3 ScCen(this Collision a) { return a.gameObject.ScCen(); }
		public static void ScR(this Collision a, float r) { a.gameObject.ScR(r); }
		public static float ScR(this Collision a) { return a.gameObject.ScR(); }
		public static void CcCen(this Collision a, Vector3 cen) { a.gameObject.CcCen(cen); }
		public static Vector3 CcCen(this Collision a) { return a.gameObject.CcCen(); }
		public static void CcR(this Collision a, float r) { a.gameObject.CcR(r); }
		public static float CcR(this Collision a) { return a.gameObject.CcR(); }
		public static void CcH(this Collision a, float h) { a.gameObject.CcH(h); }
		public static float CcH(this Collision a) { return a.gameObject.CcH(); }
		public static void CcDir(this Collision a, int dir) { a.gameObject.CcDir(dir); }
		public static int CcDir(this Collision a) { return a.gameObject.CcDir(); }
		public static void McCon(this Collision a, bool isCon) { a.gameObject.McCon(isCon); }
		public static bool McCon(this Collision a) { return a.gameObject.McCon(); }
		public static void McMesh(this Collision a, Mesh msh) { a.gameObject.McMesh(msh); }
		public static Mesh McMesh(this Collision a) { return a.gameObject.McMesh(); }
		public static void WcM(this Collision a, float m) { a.gameObject.WcM(m); }
		public static float WcM(this Collision a) { return a.gameObject.WcM(); }
		public static void WcR(this Collision a, float r) { a.gameObject.WcR(r); }
		public static float WcR(this Collision a) { return a.gameObject.WcR(); }
		public static void WcCen(this Collision a, Vector3 cen) { a.gameObject.WcCen(cen); }
		public static Vector3 WcCen(this Collision a) { return a.gameObject.WcCen(); }
		public static void TrlTm(this Collision a, float tm) { a.gameObject.TrlTm(tm); }
		public static float TrlTm(this Collision a) { return a.gameObject.TrlTm(); }
		public static void TrlMinVerDis(this Collision a, float minVerDis) { a.gameObject.TrlMinVerDis(minVerDis); }
		public static float TrlMinVerDis(this Collision a) { return a.gameObject.TrlMinVerDis(); }
		public static void TrlAutoDes(this Collision a, bool autoDes) { a.gameObject.TrlAutoDes(autoDes); }
		public static bool TrlAutoDes(this Collision a) { return a.gameObject.TrlAutoDes(); }
		public static void TrlEm(this Collision a, bool isEm) { a.gameObject.TrlEm(isEm); }
		public static bool TrlEm(this Collision a) { return a.gameObject.TrlEm(); }
		public static void TrlCg(this Collision a, Gradient cg) { a.gameObject.TrlCg(cg); }
		public static Gradient TrlCg(this Collision a) { return a.gameObject.TrlCg(); }
		public static void TrlNumCorVer(this Collision a, int numCorVer) { a.gameObject.TrlNumCorVer(numCorVer); }
		public static int TrlNumCorVer(this Collision a) { return a.gameObject.TrlNumCorVer(); }
		public static ParticleSystem.MainModule PsMain(this Collision a) { return a.gameObject.PsMain(); }
		public static void PsMainDur(this Collision a, float dur) { a.gameObject.PsMainDur(dur); }
		public static void PsMainLoop(this Collision a, bool loop) { a.gameObject.PsMainLoop(loop); }
		public static void PsMainPrewarm(this Collision a, bool prewarm) { a.gameObject.PsMainPrewarm(prewarm); }
		public static void PsMainStaDelay(this Collision a, ParticleSystem.MinMaxCurve delay, float mul) { a.gameObject.PsMainStaDelay(delay, mul); }
		public static void PsMainStaLt(this Collision a, ParticleSystem.MinMaxCurve lt, float mul) { a.gameObject.PsMainStaLt(lt, mul); }
		public static void PsMainStaSpd(this Collision a, ParticleSystem.MinMaxCurve spd, float mul) { a.gameObject.PsMainStaSpd(spd, mul); }
		public static void PsMainStaSz3D(this Collision a, bool sz) { a.gameObject.PsMainStaSz3D(sz); }
		public static void PsMainStaSz(this Collision a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSz(sz, mul); }
		public static void PsMainStaSzX(this Collision a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSzX(sz, mul); }
		public static void PsMainStaSzY(this Collision a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSzY(sz, mul); }
		public static void PsMainStaSzZ(this Collision a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSzZ(sz, mul); }
		public static void PsMainStaRot3D(this Collision a, bool rot) { a.gameObject.PsMainStaRot3D(rot); }
		public static void PsMainStaRot(this Collision a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRot(rot, mul); }
		public static void PsMainStaRotX(this Collision a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRotX(rot, mul); }
		public static void PsMainStaRotY(this Collision a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRotY(rot, mul); }
		public static void PsMainStaRotZ(this Collision a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRotZ(rot, mul); }
		public static void PsMainFlipRot(this Collision a, float rot) { a.gameObject.PsMainFlipRot(rot); }
		public static void PsMainStaC(this Collision a, ParticleSystem.MinMaxGradient c) { a.gameObject.PsMainStaC(c); }
		public static void PsMainGMod(this Collision a, ParticleSystem.MinMaxCurve gMod, float mul) { a.gameObject.PsMainGMod(gMod, mul); }
		public static void PsMainSimSpc(this Collision a, ParticleSystemSimulationSpace spc) { a.gameObject.PsMainSimSpc(spc); }
		public static void PsMainCusSimSpc(this Collision a, Transform tf) { a.gameObject.PsMainCusSimSpc(tf); }
		public static void PsMainSimSpd(this Collision a, float spd) { a.gameObject.PsMainSimSpd(spd); }
		public static void PsMainUseUnSclTm(this Collision a, bool isUse) { a.gameObject.PsMainUseUnSclTm(isUse); }
		public static void PsMainDt(this Collision a, bool isScl) { a.gameObject.PsMainDt(isScl); }
		public static void PsMainSclMd(this Collision a, ParticleSystemScalingMode md) { a.gameObject.PsMainSclMd(md); }
		public static void PsMainPlayOnAwake(this Collision a, bool playOnAwake) { a.gameObject.PsMainPlayOnAwake(playOnAwake); }
		public static void PsMainEmVelMd(this Collision a, ParticleSystemEmitterVelocityMode md) { a.gameObject.PsMainEmVelMd(md); }
		public static void PsMainMaxP(this Collision a, int maxP) { a.gameObject.PsMainMaxP(maxP); }
		public static void PsMainAutoRndSeed(this Collision a, bool isUse) { a.gameObject.PsMainAutoRndSeed(isUse); }
		public static void PsMainRndSeed(this Collision a, uint seed) { a.gameObject.PsMainRndSeed(seed); }
		public static void PsMainStopAct(this Collision a, ParticleSystemStopAction act) { a.gameObject.PsMainStopAct(act); }
		public static void PsMainCulMd(this Collision a, ParticleSystemCullingMode md) { a.gameObject.PsMainCulMd(md); }
		public static void PsMainRingBufMd(this Collision a, ParticleSystemRingBufferMode md) { a.gameObject.PsMainRingBufMd(md); }
		public static void PsMainRingBufLoopRng(this Collision a, Vector2 rng) { a.gameObject.PsMainRingBufLoopRng(rng); }
		public static ParticleSystem.EmissionModule PsEm(this Collision a) { return a.gameObject.PsEm(); }
		public static void PsEmRot(this Collision a, float tm) { a.gameObject.PsEmRot(tm); }
		public static void PsEmRot(this Collision a, ParticleSystem.MinMaxCurve tm, float mul) { a.gameObject.PsEmRot(tm, mul); }
		public static void PsEmRod(this Collision a, float dis) { a.gameObject.PsEmRod(dis); }
		public static void PsEmRod(this Collision a, ParticleSystem.MinMaxCurve dis, float mul) { a.gameObject.PsEmRod(dis, mul); }
		public static void PsEmSetBursts(this Collision a, params ParticleSystem.Burst[] bursts) { a.gameObject.PsEmSetBursts(bursts); }
		public static ParticleSystem.ShapeModule PsShp(this Collision a) { return a.gameObject.PsShp(); }
		public static ParticleSystem.VelocityOverLifetimeModule PsVol(this Collision a) { return a.gameObject.PsVol(); }
		public static void PsVolX(this Collision a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsVolX(x, mul); }
		public static void PsVolY(this Collision a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsVolY(y, mul); }
		public static void PsVolZ(this Collision a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsVolZ(z, mul); }
		public static void PsVolOrbX(this Collision a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsVolOrbX(x, mul); }
		public static void PsVolOrbY(this Collision a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsVolOrbY(y, mul); }
		public static void PsVolOrbZ(this Collision a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsVolOrbZ(z, mul); }
		public static void PsVolOrbOffX(this Collision a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsVolOrbOffX(x, mul); }
		public static void PsVolOrbOffY(this Collision a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsVolOrbOffY(y, mul); }
		public static void PsVolOrbOffZ(this Collision a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsVolOrbOffZ(z, mul); }
		public static void PsVolRadial(this Collision a, ParticleSystem.MinMaxCurve radial, float mul) { a.gameObject.PsVolRadial(radial, mul); }
		public static void PsVolSpdMod(this Collision a, ParticleSystem.MinMaxCurve mod, float mul) { a.gameObject.PsVolSpdMod(mod, mul); }
		public static void PsVolSpc(this Collision a, ParticleSystemSimulationSpace spc) { a.gameObject.PsVolSpc(spc); }
		public static ParticleSystem.LimitVelocityOverLifetimeModule PsLvol(this Collision a) { return a.gameObject.PsLvol(); }
		public static void PsLvolLim(this Collision a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLim(lim, mul); }
		public static void PsLvolLimX(this Collision a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLimX(lim, mul); }
		public static void PsLvolLimY(this Collision a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLimY(lim, mul); }
		public static void PsLvolLimZ(this Collision a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLimZ(lim, mul); }
		public static void PsLvolDrag(this Collision a, ParticleSystem.MinMaxCurve drag, float mul) { a.gameObject.PsLvolDrag(drag, mul); }
		public static void PsLvolSpc(this Collision a, ParticleSystemSimulationSpace spc) { a.gameObject.PsLvolSpc(spc); }
		public static void PsLvolDampen(this Collision a, float isDampen) { a.gameObject.PsLvolDampen(isDampen); }
		public static void PsLvolSepAxes(this Collision a, bool isAxes) { a.gameObject.PsLvolSepAxes(isAxes); }
		public static void PsLvolMulDragByPSz(this Collision a, bool isSz) { a.gameObject.PsLvolMulDragByPSz(isSz); }
		public static void PsLvolMulDragByPVel(this Collision a, bool isVel) { a.gameObject.PsLvolMulDragByPVel(isVel); }
		public static ParticleSystem.InheritVelocityModule PsIv(this Collision a) { return a.gameObject.PsIv(); }
		public static void PsIvMode(this Collision a, ParticleSystemInheritVelocityMode md) { a.gameObject.PsIvMode(md); }
		public static void PsIvCrv(this Collision a, ParticleSystem.MinMaxCurve crv, float mul) { a.gameObject.PsIvCrv(crv, mul); }
		public static ParticleSystem.ForceOverLifetimeModule PsFol(this Collision a) { return a.gameObject.PsFol(); }
		public static void PsFolX(this Collision a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsFolX(x, mul); }
		public static void PsFolY(this Collision a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsFolY(y, mul); }
		public static void PsFolZ(this Collision a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsFolZ(z, mul); }
		public static void PsFolSpc(this Collision a, ParticleSystemSimulationSpace spc) { a.gameObject.PsFolSpc(spc); }
		public static void PsFolRnd(this Collision a, bool isRnd) { a.gameObject.PsFolRnd(isRnd); }
		public static ParticleSystem.ColorOverLifetimeModule PsCol(this Collision a) { return a.gameObject.PsCol(); }
		public static void PsColC(this Collision a, ParticleSystem.MinMaxGradient c) { a.gameObject.PsColC(c); }
		public static ParticleSystem.ColorBySpeedModule PsCbs(this Collision a) { return a.gameObject.PsCbs(); }
		public static void PsCbsC(this Collision a, ParticleSystem.MinMaxGradient c) { a.gameObject.PsCbsC(c); }
		public static void PsCbsRng(this Collision a, Vector2 rng) { a.gameObject.PsCbsRng(rng); }
		public static ParticleSystem.SizeOverLifetimeModule PsSol(this Collision a) { return a.gameObject.PsSol(); }
		public static void PsSolSepAxes(this Collision a, bool isAxes) { a.gameObject.PsSolSepAxes(isAxes); }
		public static void PsSolSz(this Collision a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsSolSz(sz, mul); }
		public static void PsSolX(this Collision a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsSolX(x, mul); }
		public static void PsSolY(this Collision a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsSolY(y, mul); }
		public static void PsSolZ(this Collision a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsSolZ(z, mul); }
		public static ParticleSystem.SizeBySpeedModule PsSbs(this Collision a) { return a.gameObject.PsSbs(); }
		public static void PsSbsSepAxes(this Collision a, bool isAxes) { a.gameObject.PsSbsSepAxes(isAxes); }
		public static void PsSbsSz(this Collision a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsSbsSz(sz, mul); }
		public static void PsSbsX(this Collision a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsSbsX(x, mul); }
		public static void PsSbsY(this Collision a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsSbsY(y, mul); }
		public static void PsSbsZ(this Collision a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsSbsZ(z, mul); }
		public static void PsSbsRng(this Collision a, Vector2 rng) { a.gameObject.PsSbsRng(rng); }
		public static ParticleSystem.RotationOverLifetimeModule PsRol(this Collision a) { return a.gameObject.PsRol(); }
		public static void PsRolSepAxes(this Collision a, bool isAxes) { a.gameObject.PsRolSepAxes(isAxes); }
		public static void PsRolX(this Collision a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsRolX(x, mul); }
		public static void PsRolY(this Collision a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsRolY(y, mul); }
		public static void PsRolZ(this Collision a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsRolZ(z, mul); }
		public static ParticleSystem.RotationBySpeedModule PsRbs(this Collision a) { return a.gameObject.PsRbs(); }
		public static void PsRbsSepAxes(this Collision a, bool isAxes) { a.gameObject.PsRbsSepAxes(isAxes); }
		public static void PsRbsX(this Collision a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsRbsX(x, mul); }
		public static void PsRbsY(this Collision a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsRbsY(y, mul); }
		public static void PsRbsZ(this Collision a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsRbsZ(z, mul); }
		public static void PsRbsRng(this Collision a, Vector2 rng) { a.gameObject.PsRbsRng(rng); }
		public static ParticleSystem.ExternalForcesModule PsEf(this Collision a) { return a.gameObject.PsEf(); }
		public static void PsEfMulCrv(this Collision a, ParticleSystem.MinMaxCurve crv, float mul) { a.gameObject.PsEfMulCrv(crv, mul); }
		public static void PsEfInfFilter(this Collision a, ParticleSystemGameObjectFilter filter) { a.gameObject.PsEfInfFilter(filter); }
		public static void PsEfInfMask(this Collision a, LayerMask lm) { a.gameObject.PsEfInfMask(lm); }
		public static ParticleSystem.NoiseModule PsNoise(this Collision a) { return a.gameObject.PsNoise(); }
		public static ParticleSystem.CollisionModule PsCollision(this Collision a) { return a.gameObject.PsCollision(); }
		public static ParticleSystem.TriggerModule PsTrigger(this Collision a) { return a.gameObject.PsTrigger(); }
		public static ParticleSystem.SubEmittersModule PsSubEm(this Collision a) { return a.gameObject.PsSubEm(); }
		public static ParticleSystem.TextureSheetAnimationModule PsTexSheetAnim(this Collision a) { return a.gameObject.PsTexSheetAnim(); }
		public static ParticleSystem.LightsModule PsLights(this Collision a) { return a.gameObject.PsLights(); }
		public static ParticleSystem.TrailModule PsTrails(this Collision a) { return a.gameObject.PsTrails(); }
		public static ParticleSystem.CustomDataModule PsCusDat(this Collision a) { return a.gameObject.PsCusDat(); }
		public static void RbG(this Collision a, bool useGravity = true) { a.gameObject.RbG(useGravity); }
		public static void RbNoG(this Collision a) { a.gameObject.RbNoG(); }
		public static void RbK(this Collision a, bool isKinematic = true) { a.gameObject.RbK(isKinematic); }
		public static void RbNoK(this Collision a) { a.gameObject.RbNoK(); }
		public static void RbInt(this Collision a, int interpolate) { a.gameObject.RbInt(interpolate); }
		public static void RbInt(this Collision a, RigidbodyInterpolation interpolate) { a.gameObject.RbInt(interpolate); }
		public static void RbInt0(this Collision a) { a.gameObject.RbInt0(); }
		public static void RbIntInt(this Collision a) { a.gameObject.RbIntInt(); }
		public static void RbIntExt(this Collision a) { a.gameObject.RbIntExt(); }
		public static void RbColDet(this Collision a, int collisionDetection) { a.gameObject.RbColDet(collisionDetection); }
		public static void RbColDet(this Collision a, CollisionDetectionMode collisionDetection) { a.gameObject.RbColDet(collisionDetection); }
		public static void RbColDetDis(this Collision a) { a.gameObject.RbColDetDis(); }
		public static void RbColDetCon(this Collision a) { a.gameObject.RbColDetCon(); }
		public static void RbColDetConD(this Collision a) { a.gameObject.RbColDetConD(); }
		public static void RbColDetConS(this Collision a) { a.gameObject.RbColDetConS(); }
		public static void RbCon(this Collision a, bool isFrzPosX = false, bool isFrzPosY = false, bool isFrzPosZ = false, bool isFrzRotX = false, bool isFrzRotY = false, bool isFrzRotZ = false) { a.gameObject.RbCon(isFrzPosX, isFrzPosY, isFrzPosZ, isFrzRotX, isFrzRotY, isFrzRotZ); }
		public static void RbCon0(this Collision a) { a.gameObject.RbCon0(); }
		public static void RbConFrzPos(this Collision a) { a.gameObject.RbConFrzPos(); }
		public static void RbConFrzRot(this Collision a) { a.gameObject.RbConFrzRot(); }
		public static void RbConFrzAll(this Collision a) { a.gameObject.RbConFrzAll(); }
		public static void RbAv0(this Collision a) { a.gameObject.RbAv0(); }
		public static void RbAv(this Collision a, Vector3 v) { a.gameObject.RbAv(v); }
		public static void RbAvx(this Collision a, float x) { a.gameObject.RbAvx(x); }
		public static void RbAvy(this Collision a, float y) { a.gameObject.RbAvy(y); }
		public static void RbAvz(this Collision a, float z) { a.gameObject.RbAvz(z); }
		public static void RbAvxy(this Collision a, float x, float y) { a.gameObject.RbAvxy(x, y); }
		public static void RbAvxz(this Collision a, float x, float z) { a.gameObject.RbAvxz(x, z); }
		public static void RbAvyz(this Collision a, float y, float z) { a.gameObject.RbAvyz(y, z); }
		public static void RbAv(this Collision a, float x, float y, float z) { a.gameObject.RbAv(x, y, z); }
		public static void RbV0(this Collision a) { a.gameObject.RbV0(); }
		public static void RbV(this Collision a, Vector3 v) { a.gameObject.RbV(v); }
		public static void RbVx(this Collision a, float x) { a.gameObject.RbVx(x); }
		public static void RbVy(this Collision a, float y) { a.gameObject.RbVy(y); }
		public static void RbVz(this Collision a, float z) { a.gameObject.RbVz(z); }
		public static void RbVxy(this Collision a, float x, float y) { a.gameObject.RbVxy(x, y); }
		public static void RbVxz(this Collision a, float x, float z) { a.gameObject.RbVxz(x, z); }
		public static void RbVyz(this Collision a, float y, float z) { a.gameObject.RbVyz(y, z); }
		public static void RbV(this Collision a, float x, float y, float z) { a.gameObject.RbV(x, y, z); }
		public static T Gc<T>(this Collision2D a) { return a.gameObject.Gc<T>(); }
		public static T Gc<T>(this Collision2D a, GcTp tp) where T : Component { return a.gameObject.Gc<T>(tp); }
		public static Text Txt(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Txt(tp); }
		public static Button Btn(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Btn(tp); }
		public static Image Img(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Img(tp); }
		public static Toggle Tgl(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Tgl(tp); }
		public static Slider Sld(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Sld(tp); }
		public static Dropdown Drp(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Drp(tp); }
		public static InputField Inp(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Inp(tp); }
		public static ColorPicker Cp(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Cp(tp); }
		public static TextMesh Tm(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Tm(tp); }
		public static TextMeshPro Tmp(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Tmp(tp); }
		public static Rigidbody Rb(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Rb(tp); }
		public static Rigidbody2D Rb2(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Rb2(tp); }
		public static Renderer Ren(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Ren(tp); }
		public static MeshFilter Mf(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Mf(tp); }
		public static MeshRenderer Mr(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Mr(tp); }
		public static Collider Col(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Col(tp); }
		public static BoxCollider Bc(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Bc(tp); }
		public static SphereCollider Sc(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Sc(tp); }
		public static CapsuleCollider Cc(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Cc(tp); }
		public static MeshCollider Mc(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Mc(tp); }
		public static WheelCollider Wc(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Wc(tp); }
		public static ParticleSystem Ps(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Ps(tp); }
		public static ParticleSystemRenderer Psr(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Psr(tp); }
		public static TrailRenderer Trl(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Trl(tp); }
		public static Animator An(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.An(tp); }
		public static RectTransform Rt(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Rt(tp); }
		public static LineRenderer Lr(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Lr(tp); }
		public static Shadow Shadow(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Shadow(tp); }
		public static Outline Outline(this Collision2D a, GcTp tp = GcTp.Get) { return a.gameObject.Outline(tp); }
		public static void Active(this Collision2D a, bool active) { a.gameObject.Active(active); }
		public static void Show(this Collision2D a) { a.gameObject.Show(); }
		public static void Hide(this Collision2D a) { a.gameObject.Hide(); }
		public static bool IsActive(this Collision2D a) { return a.gameObject.IsActive(); }
		public static bool Is<T>(this Collision2D a) { return a.gameObject.Is<T>(); }
		public static T Ac<T>(this Collision2D a) where T : Component { return a.gameObject.Ac<T>(); }
		public static T Gcic<T>(this Collision2D a) { return a.gameObject.Gcic<T>(); }
		public static T Gcip<T>(this Collision2D a) { return a.gameObject.Gcip<T>(); }
		public static T[] Gcs<T>(this Collision2D a) { return a.gameObject.Gcs<T>(); }
		public static T[] Gcsic<T>(this Collision2D a) { return a.gameObject.Gcsic<T>(); }
		public static T[] Gcsip<T>(this Collision2D a) { return a.gameObject.Gcsip<T>(); }
		public static List<T> Gca<T>(this Collision2D a) { return a.gameObject.Gca<T>(); }
		public static List<Transform> Childs(this Collision2D a) { return a.gameObject.Childs(); }
		public static List<GameObject> ChildGos(this Collision2D a) { return a.gameObject.ChildGos(); }
		public static List<T> Childs<T>(this Collision2D a) { return a.gameObject.Childs<T>(); }
		public static void DstChilds<T>(this Collision2D a, int sta = 0, int end = -1) where T : Object { a.gameObject.DstChilds<T>(sta, end); }
		public static void DstChilds(this Collision2D a, int sta = 0, int end = -1) { a.gameObject.DstChilds(sta, end); }
		public static Color GetCol(this Collision2D a, Color col = default) { return a.gameObject.GetCol(col); }
		public static void SetCol(this Collision2D a, Color col) { a.gameObject.SetCol(col); }
		public static void ColA(this Collision2D a, float alpha) { a.gameObject.ColA(alpha); }
		public static string GetText(this Collision2D a, string text = "") { return a.gameObject.GetText(text); }
		public static void SetText(this Collision2D a, string text) { a.gameObject.SetText(text); }
		public static bool Tag(this Collision2D a, string tag) { return a.gameObject.Tag(tag); }
		public static bool Tag(this Collision2D a, Tag tag) { return a.gameObject.Tag(tag); }
		public static Vector3 Tp(this Collision2D a) { return a.gameObject.Tp(); }
		public static void Tp(this Collision2D a, Vector3 v) { a.gameObject.Tp(v); }
		public static void Tp(this Collision2D a, float x, float y, float z) { a.gameObject.Tp(x, y, z); }
		public static void TpX(this Collision2D a, float x) { a.gameObject.TpX(x); }
		public static void TpY(this Collision2D a, float y) { a.gameObject.TpY(y); }
		public static void TpZ(this Collision2D a, float z) { a.gameObject.TpZ(z); }
		public static void TpXy(this Collision2D a, float x, float y) { a.gameObject.TpXy(x, y); }
		public static void TpXz(this Collision2D a, float x, float z) { a.gameObject.TpXz(x, z); }
		public static void TpYz(this Collision2D a, float y, float z) { a.gameObject.TpYz(y, z); }
		public static void TpD(this Collision2D a, Vector3 v) { a.gameObject.TpD(v); }
		public static void TpD(this Collision2D a, float x, float y, float z) { a.gameObject.TpD(x, y, z); }
		public static void TpDx(this Collision2D a, float x) { a.gameObject.TpDx(x); }
		public static void TpDy(this Collision2D a, float y) { a.gameObject.TpDy(y); }
		public static void TpDz(this Collision2D a, float z) { a.gameObject.TpDz(z); }
		public static void TpDxy(this Collision2D a, float x, float y) { a.gameObject.TpDxy(x, y); }
		public static void TpDxz(this Collision2D a, float x, float z) { a.gameObject.TpDxz(x, z); }
		public static void TpDyz(this Collision2D a, float y, float z) { a.gameObject.TpDyz(y, z); }
		public static Vector3 Tlp(this Collision2D a) { return a.gameObject.Tlp(); }
		public static void Tlp(this Collision2D a, Vector3 v) { a.gameObject.Tlp(v); }
		public static void Tlp(this Collision2D a, float x, float y, float z) { a.gameObject.Tlp(x, y, z); }
		public static void TlpX(this Collision2D a, float x) { a.gameObject.TlpX(x); }
		public static void TlpY(this Collision2D a, float y) { a.gameObject.TlpY(y); }
		public static void TlpZ(this Collision2D a, float z) { a.gameObject.TlpZ(z); }
		public static void TlpXy(this Collision2D a, float x, float y) { a.gameObject.TlpXy(x, y); }
		public static void TlpXz(this Collision2D a, float x, float z) { a.gameObject.TlpXz(x, z); }
		public static void TlpYz(this Collision2D a, float y, float z) { a.gameObject.TlpYz(y, z); }
		public static void TlpD(this Collision2D a, Vector3 v) { a.gameObject.TlpD(v); }
		public static void TlpD(this Collision2D a, float x, float y, float z) { a.gameObject.TlpD(x, y, z); }
		public static void TlpDx(this Collision2D a, float x) { a.gameObject.TlpDx(x); }
		public static void TlpDy(this Collision2D a, float y) { a.gameObject.TlpDy(y); }
		public static void TlpDz(this Collision2D a, float z) { a.gameObject.TlpDz(z); }
		public static void TlpDxy(this Collision2D a, float x, float y) { a.gameObject.TlpDxy(x, y); }
		public static void TlpDxz(this Collision2D a, float x, float z) { a.gameObject.TlpDxz(x, z); }
		public static void TlpDyz(this Collision2D a, float y, float z) { a.gameObject.TlpDyz(y, z); }
		public static Quaternion Tr(this Collision2D a) { return a.gameObject.Tr(); }
		public static void Tr(this Collision2D a, Quaternion v) { a.gameObject.Tr(v); }
		public static Quaternion Tlr(this Collision2D a) { return a.gameObject.Tlr(); }
		public static void Tlr(this Collision2D a, Quaternion v) { a.gameObject.Tlr(v); }
		public static Vector3 Ts(this Collision2D a) { return a.gameObject.Ts(); }
		public static void Ts(this Collision2D a, float x, float y, float z) { a.gameObject.Ts(x, y, z); }
		public static void TsX(this Collision2D a, float x) { a.gameObject.TsX(x); }
		public static void TsY(this Collision2D a, float y) { a.gameObject.TsY(y); }
		public static void TsZ(this Collision2D a, float z) { a.gameObject.TsZ(z); }
		public static void TsXy(this Collision2D a, float x, float y) { a.gameObject.TsXy(x, y); }
		public static void TsXz(this Collision2D a, float x, float z) { a.gameObject.TsXz(x, z); }
		public static void TsYz(this Collision2D a, float y, float z) { a.gameObject.TsYz(y, z); }
		public static void TsD(this Collision2D a, Vector3 v) { a.gameObject.TsD(v); }
		public static void TsD(this Collision2D a, float x, float y, float z) { a.gameObject.TsD(x, y, z); }
		public static void TsDx(this Collision2D a, float x) { a.gameObject.TsDx(x); }
		public static void TsDy(this Collision2D a, float y) { a.gameObject.TsDy(y); }
		public static void TsDz(this Collision2D a, float z) { a.gameObject.TsDz(z); }
		public static void TsDxy(this Collision2D a, float x, float y) { a.gameObject.TsDxy(x, y); }
		public static void TsDxz(this Collision2D a, float x, float z) { a.gameObject.TsDxz(x, z); }
		public static void TsDyz(this Collision2D a, float y, float z) { a.gameObject.TsDyz(y, z); }
		public static Vector3 Tls(this Collision2D a) { return a.gameObject.Tls(); }
		public static void Tls(this Collision2D a, Vector3 v) { a.gameObject.Tls(v); }
		public static void Tls(this Collision2D a, float x, float y, float z) { a.gameObject.Tls(x, y, z); }
		public static void TlsX(this Collision2D a, float x) { a.gameObject.TlsX(x); }
		public static void TlsY(this Collision2D a, float y) { a.gameObject.TlsY(y); }
		public static void TlsZ(this Collision2D a, float z) { a.gameObject.TlsZ(z); }
		public static void TlsXy(this Collision2D a, float x, float y) { a.gameObject.TlsXy(x, y); }
		public static void TlsXz(this Collision2D a, float x, float z) { a.gameObject.TlsXz(x, z); }
		public static void TlsYz(this Collision2D a, float y, float z) { a.gameObject.TlsYz(y, z); }
		public static void TlsD(this Collision2D a, Vector3 v) { a.gameObject.TlsD(v); }
		public static void TlsD(this Collision2D a, float x, float y, float z) { a.gameObject.TlsD(x, y, z); }
		public static void TlsDx(this Collision2D a, float x) { a.gameObject.TlsDx(x); }
		public static void TlsDy(this Collision2D a, float y) { a.gameObject.TlsDy(y); }
		public static void TlsDz(this Collision2D a, float z) { a.gameObject.TlsDz(z); }
		public static void TlsDxy(this Collision2D a, float x, float y) { a.gameObject.TlsDxy(x, y); }
		public static void TlsDxz(this Collision2D a, float x, float z) { a.gameObject.TlsDxz(x, z); }
		public static void TlsDyz(this Collision2D a, float y, float z) { a.gameObject.TlsDyz(y, z); }
		public static Vector3 Te(this Collision2D a) { return a.gameObject.Te(); }
		public static void Te(this Collision2D a, Vector3 v) { a.gameObject.Te(v); }
		public static void Te(this Collision2D a, float x, float y, float z) { a.gameObject.Te(x, y, z); }
		public static void TeX(this Collision2D a, float x) { a.gameObject.TeX(x); }
		public static void TeY(this Collision2D a, float y) { a.gameObject.TeY(y); }
		public static void TeZ(this Collision2D a, float z) { a.gameObject.TeZ(z); }
		public static void TeXy(this Collision2D a, float x, float y) { a.gameObject.TeXy(x, y); }
		public static void TeXz(this Collision2D a, float x, float z) { a.gameObject.TeXz(x, z); }
		public static void TeYz(this Collision2D a, float y, float z) { a.gameObject.TeYz(y, z); }
		public static void TeD(this Collision2D a, Vector3 v) { a.gameObject.TeD(v); }
		public static void TeD(this Collision2D a, float x, float y, float z) { a.gameObject.TeD(x, y, z); }
		public static void TeDx(this Collision2D a, float x) { a.gameObject.TeDx(x); }
		public static void TeDy(this Collision2D a, float y) { a.gameObject.TeDy(y); }
		public static void TeDz(this Collision2D a, float z) { a.gameObject.TeDz(z); }
		public static void TeDxy(this Collision2D a, float x, float y) { a.gameObject.TeDxy(x, y); }
		public static void TeDxz(this Collision2D a, float x, float z) { a.gameObject.TeDxz(x, z); }
		public static void TeDyz(this Collision2D a, float y, float z) { a.gameObject.TeDyz(y, z); }
		public static Vector3 Tle(this Collision2D a) { return a.gameObject.Tle(); }
		public static void Tle(this Collision2D a, Vector3 v) { a.gameObject.Tle(v); }
		public static void Tle(this Collision2D a, float x, float y, float z) { a.gameObject.Tle(x, y, z); }
		public static void TleX(this Collision2D a, float x) { a.gameObject.TleX(x); }
		public static void TleY(this Collision2D a, float y) { a.gameObject.TleY(y); }
		public static void TleZ(this Collision2D a, float z) { a.gameObject.TleZ(z); }
		public static void TleXy(this Collision2D a, float x, float y) { a.gameObject.TleXy(x, y); }
		public static void TleXz(this Collision2D a, float x, float z) { a.gameObject.TleXz(x, z); }
		public static void TleYz(this Collision2D a, float y, float z) { a.gameObject.TleYz(y, z); }
		public static void TleD(this Collision2D a, Vector3 v) { a.gameObject.TleD(v); }
		public static void TleD(this Collision2D a, float x, float y, float z) { a.gameObject.TleD(x, y, z); }
		public static void TleDx(this Collision2D a, float x) { a.gameObject.TleDx(x); }
		public static void TleDy(this Collision2D a, float y) { a.gameObject.TleDy(y); }
		public static void TleDz(this Collision2D a, float z) { a.gameObject.TleDz(z); }
		public static void TleDxy(this Collision2D a, float x, float y) { a.gameObject.TleDxy(x, y); }
		public static void TleDxz(this Collision2D a, float x, float z) { a.gameObject.TleDxz(x, z); }
		public static void TleDyz(this Collision2D a, float y, float z) { a.gameObject.TleDyz(y, z); }
		public static int Tcc(this Collision2D a) { return a.gameObject.Tcc(); }
		public static Vector3 TfDir(this Collision2D a, float x, float y, float z) { return a.gameObject.TfDir(x, y, z); }
		public static Vector3 TfDir(this Collision2D a, Vector3 dir) { return a.gameObject.TfDir(dir); }
		public static Vector3 TfInvDir(this Collision2D a, float x, float y, float z) { return a.gameObject.TfInvDir(x, y, z); }
		public static Vector3 TfInvDir(this Collision2D a, Vector3 dir) { return a.gameObject.TfInvDir(dir); }
		public static Vector3 TfPnt(this Collision2D a, float x, float y, float z) { return a.gameObject.TfPnt(x, y, z); }
		public static Vector3 TfPnt(this Collision2D a, Vector3 pnt) { return a.gameObject.TfPnt(pnt); }
		public static Vector3 TfInvPnt(this Collision2D a, float x, float y, float z) { return a.gameObject.TfInvPnt(x, y, z); }
		public static Vector3 TfInvPnt(this Collision2D a, Vector3 pnt) { return a.gameObject.TfInvPnt(pnt); }
		public static Vector3 TfVec(this Collision2D a, float x, float y, float z) { return a.gameObject.TfVec(x, y, z); }
		public static Vector3 TfVec(this Collision2D a, Vector3 vec) { return a.gameObject.TfVec(vec); }
		public static Vector3 TfInvVec(this Collision2D a, float x, float y, float z) { return a.gameObject.TfInvVec(x, y, z); }
		public static Vector3 TfInvVec(this Collision2D a, Vector3 vec) { return a.gameObject.TfInvVec(vec); }
		public static void TfLookAt(this Collision2D a, Transform tf) { a.gameObject.TfLookAt(tf); }
		public static void TfLookAt(this Collision2D a, Transform tf, Vector3 up) { a.gameObject.TfLookAt(tf, up); }
		public static void TfLookAt(this Collision2D a, Vector3 pos) { a.gameObject.TfLookAt(pos); }
		public static void TfLookAt(this Collision2D a, Vector3 pos, Vector3 up) { a.gameObject.TfLookAt(pos, up); }
		public static void TfRot(this Collision2D a, float x, float y, float z) { a.gameObject.TfRot(x, y, z); }
		public static void TfRot(this Collision2D a, float x, float y, float z, Space spc) { a.gameObject.TfRot(x, y, z, spc); }
		public static void TfRot(this Collision2D a, Vector3 rot) { a.gameObject.TfRot(rot); }
		public static void TfRot(this Collision2D a, Vector3 rot, Space spc) { a.gameObject.TfRot(rot, spc); }
		public static void TfRot(this Collision2D a, Vector3 axis, float ang) { a.gameObject.TfRot(axis, ang); }
		public static void TfRot(this Collision2D a, Vector3 axis, float ang, Space spc) { a.gameObject.TfRot(axis, ang, spc); }
		public static void TfRotAround(this Collision2D a, Vector3 pnt, Vector3 axis, float ang) { a.gameObject.TfRotAround(pnt, axis, ang); }
		public static void TfTra(this Collision2D a, float x, float y, float z) { a.gameObject.TfTra(x, y, z); }
		public static void TfTra(this Collision2D a, float x, float y, float z, Space spc) { a.gameObject.TfTra(x, y, z, spc); }
		public static void TfTra(this Collision2D a, float x, float y, float z, Transform tf) { a.gameObject.TfTra(x, y, z, tf); }
		public static void TfTra(this Collision2D a, Vector3 tra) { a.gameObject.TfTra(tra); }
		public static void TfTra(this Collision2D a, Vector3 tra, Space spc) { a.gameObject.TfTra(tra, spc); }
		public static void TfTra(this Collision2D a, Vector3 tra, Transform tf) { a.gameObject.TfTra(tra, tf); }
		public static Transform Fnd(this Collision2D a, string name) { return a.gameObject.Fnd(name); }
		public static GameObject FndGo(this Collision2D a, string name) { return a.gameObject.FndGo(name); }
		public static T Fnd<T>(this Collision2D a, string name) { return a.gameObject.Fnd<T>(name); }
		public static void SibIdx(this Collision2D a, int i) { a.gameObject.SibIdx(i); }
		public static int SibIdx(this Collision2D a) { return a.gameObject.SibIdx(); }
		public static void SibFist(this Collision2D a) { a.gameObject.SibFist(); }
		public static void SibLast(this Collision2D a) { a.gameObject.SibLast(); }
		public static void Par(this Collision2D a, Transform parTf) { a.gameObject.Par(parTf); }
		public static void Par(this Collision2D a, Transform parTf, bool isWorPosStays) { a.gameObject.Par(parTf, isWorPosStays); }
		public static Transform Child(this Collision2D a, params int[] childs) { return a.gameObject.Child(childs); }
		public static Transform ChildName(this Collision2D a, params string[] childs) { return a.gameObject.ChildName(childs); }
		public static Transform Child(this Collision2D a, string childs = "") { return a.gameObject.Child(childs); }
		public static GameObject ChildGo(this Collision2D a, params int[] childs) { return a.gameObject.ChildGo(childs); }
		public static GameObject ChildNameGo(this Collision2D a, params string[] childs) { return a.gameObject.ChildNameGo(childs); }
		public static GameObject ChildGo(this Collision2D a, string childs = "") { return a.gameObject.ChildGo(childs); }
		public static T Child<T>(this Collision2D a, params int[] childs) { return a.gameObject.Child<T>(childs); }
		public static T ChildName<T>(this Collision2D a, params string[] childs) { return a.gameObject.ChildName<T>(childs); }
		public static T Child<T>(this Collision2D a, string childs = "") { return a.gameObject.Child<T>(childs); }
		public static void ChildActive(this Collision2D a, bool active, params int[] childs) { a.gameObject.ChildActive(active, childs); }
		public static void ChildNameActive(this Collision2D a, bool active, params string[] childs) { a.gameObject.ChildNameActive(active, childs); }
		public static void ChildActive(this Collision2D a, bool active, string childs = "") { a.gameObject.ChildActive(active, childs); }
		public static void ChildHide(this Collision2D a, params int[] childs) { a.gameObject.ChildHide(childs); }
		public static void ChildNameHide(this Collision2D a, params string[] childs) { a.gameObject.ChildNameHide(childs); }
		public static void ChildHide(this Collision2D a, string childs = "") { a.gameObject.ChildHide(childs); }
		public static void ChildShow(this Collision2D a, params int[] childs) { a.gameObject.ChildShow(childs); }
		public static void ChildNameShow(this Collision2D a, params string[] childs) { a.gameObject.ChildNameShow(childs); }
		public static void ChildShow(this Collision2D a, string childs = "") { a.gameObject.ChildShow(childs); }
		public static Transform Par(this Collision2D a, int par = 0) { return a.gameObject.Par(par); }
		public static Transform ParName(this Collision2D a, string par) { return a.gameObject.ParName(par); }
		public static GameObject ParGo(this Collision2D a, int par = 0) { return a.gameObject.ParGo(par); }
		public static GameObject ParNameGo(this Collision2D a, string par) { return a.gameObject.ParNameGo(par); }
		public static T Par<T>(this Collision2D a, int par = 0) { return a.gameObject.Par<T>(par); }
		public static T ParName<T>(this Collision2D a, string par) { return a.gameObject.ParName<T>(par); }
		public static void ParActive(this Collision2D a, bool active, int par = 0) { a.gameObject.ParActive(active, par); }
		public static void ParNameActive(this Collision2D a, bool active, string par) { a.gameObject.ParNameActive(active, par); }
		public static void ParHide(this Collision2D a, int par = 0) { a.gameObject.ParHide(par); }
		public static void ParNameHide(this Collision2D a, string par) { a.gameObject.ParNameHide(par); }
		public static void ParShow(this Collision2D a, int par = 0) { a.gameObject.ParShow(par); }
		public static void ParNameShow(this Collision2D a, string par) { a.gameObject.ParNameShow(par); }
		public static Transform ParChild(this Collision2D a, int par = 0, params int[] childs) { return a.gameObject.ParChild(par, childs); }
		public static Transform ParChildName(this Collision2D a, string par, params string[] childs) { return a.gameObject.ParChildName(par, childs); }
		public static Transform ParChild(this Collision2D a, int par = 0, string childs = "") { return a.gameObject.ParChild(par, childs); }
		public static GameObject ParChildGo(this Collision2D a, int par = 0, params int[] childs) { return a.gameObject.ParChildGo(par, childs); }
		public static GameObject ParChildNameGo(this Collision2D a, string par, params string[] childs) { return a.gameObject.ParChildNameGo(par, childs); }
		public static GameObject ParChildGo(this Collision2D a, int par = 0, string childs = "") { return a.gameObject.ParChildGo(par, childs); }
		public static T ParChild<T>(this Collision2D a, int par = 0, params int[] childs) { return a.gameObject.ParChild<T>(par, childs); }
		public static T ParChildName<T>(this Collision2D a, string par, params string[] childs) { return a.gameObject.ParChildName<T>(par, childs); }
		public static T ParChild<T>(this Collision2D a, int par = 0, string childs = "") { return a.gameObject.ParChild<T>(par, childs); }
		public static void ParChildActive(this Collision2D a, bool active, int par = 0, params int[] childs) { a.gameObject.ParChildActive(active, par, childs); }
		public static void ParChildNameActive(this Collision2D a, bool active, string par, params string[] childs) { a.gameObject.ParChildNameActive(active, par, childs); }
		public static void ParChildActive(this Collision2D a, bool active, int par = 0, string childs = "") { a.gameObject.ParChildActive(active, par, childs); }
		public static void ParChildHide(this Collision2D a, int par = 0, params int[] childs) { a.gameObject.ParChildHide(par, childs); }
		public static void ParChildNameHide(this Collision2D a, string par, params string[] childs) { a.gameObject.ParChildNameHide(par, childs); }
		public static void ParChildHide(this Collision2D a, int par = 0, string childs = "") { a.gameObject.ParChildHide(par, childs); }
		public static void ParChildShow(this Collision2D a, int par = 0, params int[] childs) { a.gameObject.ParChildShow(par, childs); }
		public static void ParChildNameShow(this Collision2D a, string par, params string[] childs) { a.gameObject.ParChildNameShow(par, childs); }
		public static void ParChildShow(this Collision2D a, int par = 0, string childs = "") { a.gameObject.ParChildShow(par, childs); }
		public static Transform Sib(this Collision2D a, int sib = 0) { return a.gameObject.Sib(sib); }
		public static Transform SibName(this Collision2D a, string sib) { return a.gameObject.SibName(sib); }
		public static GameObject SibGo(this Collision2D a, int sib = 0) { return a.gameObject.SibGo(sib); }
		public static GameObject SibNameGo(this Collision2D a, string sib) { return a.gameObject.SibNameGo(sib); }
		public static T Sib<T>(this Collision2D a, int sib = 0) { return a.gameObject.Sib<T>(sib); }
		public static T SibName<T>(this Collision2D a, string sib) { return a.gameObject.SibName<T>(sib); }
		public static void SibActive(this Collision2D a, bool active, int sib = 0) { a.gameObject.SibActive(active, sib); }
		public static void SibNameActive(this Collision2D a, bool active, string sib) { a.gameObject.SibNameActive(active, sib); }
		public static void SibHide(this Collision2D a, int sib = 0) { a.gameObject.SibHide(sib); }
		public static void SibNameHide(this Collision2D a, string sib) { a.gameObject.SibNameHide(sib); }
		public static void SibShow(this Collision2D a, int sib = 0) { a.gameObject.SibShow(sib); }
		public static void SibNameShow(this Collision2D a, string sib) { a.gameObject.SibNameShow(sib); }
		public static Material RenMat(this Collision2D a) { return a.gameObject.RenMat(); }
		public static void RenMat(this Collision2D a, Material mat) { a.gameObject.RenMat(mat); }
		public static List<Material> RenMats(this Collision2D a) { return a.gameObject.RenMats(); }
		public static void RenMatCol(this Collision2D a, Color col) { a.gameObject.RenMatCol(col); }
		public static void RenMatCol(this Collision2D a, string name, Color col) { a.gameObject.RenMatCol(name, col); }
		public static Color RenMatCol(this Collision2D a) { return a.gameObject.RenMatCol(); }
		public static Color RenMatCol(this Collision2D a, string name) { return a.gameObject.RenMatCol(name); }
		public static Material MatFindName(this Collision2D a, string name) { return a.gameObject.MatFindName(name); }
		public static List<Material> MatList(this Collision2D a) { return a.gameObject.MatList(); }
		public static List<Material> MatListTransparent(this Collision2D a) { return a.gameObject.MatListTransparent(); }
		public static BoxCollider Bc(this Collision2D a, Vector3 sz, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) { return a.gameObject.Bc(sz, cen, isT, mat, tp); }
		public static SphereCollider Sc(this Collision2D a, float r, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) { return a.gameObject.Sc(r, cen, isT, mat, tp); }
		public static CapsuleCollider Cc(this Collision2D a, float r, float h = 1, int dir = 1, Vector3 cen = default, bool isT = false, PhysicMaterial mat = null, GcTp tp = GcTp.Get) { return a.gameObject.Cc(r, h, dir, cen, isT, mat, tp); }
		public static void ColIsT(this Collision2D a, bool isT) { a.gameObject.ColIsT(isT); }
		public static bool ColIsT(this Collision2D a) { return a.gameObject.ColIsT(); }
		public static void ColMat(this Collision2D a, PhysicMaterial mat) { a.gameObject.ColMat(mat); }
		public static PhysicMaterial ColMat(this Collision2D a) { return a.gameObject.ColMat(); }
		public static void BcCen(this Collision2D a, Vector3 cen) { a.gameObject.BcCen(cen); }
		public static Vector3 BcCen(this Collision2D a) { return a.gameObject.BcCen(); }
		public static void BcSz(this Collision2D a, Vector3 sz) { a.gameObject.BcSz(sz); }
		public static Vector3 BcSz(this Collision2D a) { return a.gameObject.BcSz(); }
		public static void ScCen(this Collision2D a, Vector3 cen) { a.gameObject.ScCen(cen); }
		public static Vector3 ScCen(this Collision2D a) { return a.gameObject.ScCen(); }
		public static void ScR(this Collision2D a, float r) { a.gameObject.ScR(r); }
		public static float ScR(this Collision2D a) { return a.gameObject.ScR(); }
		public static void CcCen(this Collision2D a, Vector3 cen) { a.gameObject.CcCen(cen); }
		public static Vector3 CcCen(this Collision2D a) { return a.gameObject.CcCen(); }
		public static void CcR(this Collision2D a, float r) { a.gameObject.CcR(r); }
		public static float CcR(this Collision2D a) { return a.gameObject.CcR(); }
		public static void CcH(this Collision2D a, float h) { a.gameObject.CcH(h); }
		public static float CcH(this Collision2D a) { return a.gameObject.CcH(); }
		public static void CcDir(this Collision2D a, int dir) { a.gameObject.CcDir(dir); }
		public static int CcDir(this Collision2D a) { return a.gameObject.CcDir(); }
		public static void McCon(this Collision2D a, bool isCon) { a.gameObject.McCon(isCon); }
		public static bool McCon(this Collision2D a) { return a.gameObject.McCon(); }
		public static void McMesh(this Collision2D a, Mesh msh) { a.gameObject.McMesh(msh); }
		public static Mesh McMesh(this Collision2D a) { return a.gameObject.McMesh(); }
		public static void WcM(this Collision2D a, float m) { a.gameObject.WcM(m); }
		public static float WcM(this Collision2D a) { return a.gameObject.WcM(); }
		public static void WcR(this Collision2D a, float r) { a.gameObject.WcR(r); }
		public static float WcR(this Collision2D a) { return a.gameObject.WcR(); }
		public static void WcCen(this Collision2D a, Vector3 cen) { a.gameObject.WcCen(cen); }
		public static Vector3 WcCen(this Collision2D a) { return a.gameObject.WcCen(); }
		public static void TrlTm(this Collision2D a, float tm) { a.gameObject.TrlTm(tm); }
		public static float TrlTm(this Collision2D a) { return a.gameObject.TrlTm(); }
		public static void TrlMinVerDis(this Collision2D a, float minVerDis) { a.gameObject.TrlMinVerDis(minVerDis); }
		public static float TrlMinVerDis(this Collision2D a) { return a.gameObject.TrlMinVerDis(); }
		public static void TrlAutoDes(this Collision2D a, bool autoDes) { a.gameObject.TrlAutoDes(autoDes); }
		public static bool TrlAutoDes(this Collision2D a) { return a.gameObject.TrlAutoDes(); }
		public static void TrlEm(this Collision2D a, bool isEm) { a.gameObject.TrlEm(isEm); }
		public static bool TrlEm(this Collision2D a) { return a.gameObject.TrlEm(); }
		public static void TrlCg(this Collision2D a, Gradient cg) { a.gameObject.TrlCg(cg); }
		public static Gradient TrlCg(this Collision2D a) { return a.gameObject.TrlCg(); }
		public static void TrlNumCorVer(this Collision2D a, int numCorVer) { a.gameObject.TrlNumCorVer(numCorVer); }
		public static int TrlNumCorVer(this Collision2D a) { return a.gameObject.TrlNumCorVer(); }
		public static ParticleSystem.MainModule PsMain(this Collision2D a) { return a.gameObject.PsMain(); }
		public static void PsMainDur(this Collision2D a, float dur) { a.gameObject.PsMainDur(dur); }
		public static void PsMainLoop(this Collision2D a, bool loop) { a.gameObject.PsMainLoop(loop); }
		public static void PsMainPrewarm(this Collision2D a, bool prewarm) { a.gameObject.PsMainPrewarm(prewarm); }
		public static void PsMainStaDelay(this Collision2D a, ParticleSystem.MinMaxCurve delay, float mul) { a.gameObject.PsMainStaDelay(delay, mul); }
		public static void PsMainStaLt(this Collision2D a, ParticleSystem.MinMaxCurve lt, float mul) { a.gameObject.PsMainStaLt(lt, mul); }
		public static void PsMainStaSpd(this Collision2D a, ParticleSystem.MinMaxCurve spd, float mul) { a.gameObject.PsMainStaSpd(spd, mul); }
		public static void PsMainStaSz3D(this Collision2D a, bool sz) { a.gameObject.PsMainStaSz3D(sz); }
		public static void PsMainStaSz(this Collision2D a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSz(sz, mul); }
		public static void PsMainStaSzX(this Collision2D a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSzX(sz, mul); }
		public static void PsMainStaSzY(this Collision2D a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSzY(sz, mul); }
		public static void PsMainStaSzZ(this Collision2D a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsMainStaSzZ(sz, mul); }
		public static void PsMainStaRot3D(this Collision2D a, bool rot) { a.gameObject.PsMainStaRot3D(rot); }
		public static void PsMainStaRot(this Collision2D a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRot(rot, mul); }
		public static void PsMainStaRotX(this Collision2D a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRotX(rot, mul); }
		public static void PsMainStaRotY(this Collision2D a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRotY(rot, mul); }
		public static void PsMainStaRotZ(this Collision2D a, ParticleSystem.MinMaxCurve rot, float mul) { a.gameObject.PsMainStaRotZ(rot, mul); }
		public static void PsMainFlipRot(this Collision2D a, float rot) { a.gameObject.PsMainFlipRot(rot); }
		public static void PsMainStaC(this Collision2D a, ParticleSystem.MinMaxGradient c) { a.gameObject.PsMainStaC(c); }
		public static void PsMainGMod(this Collision2D a, ParticleSystem.MinMaxCurve gMod, float mul) { a.gameObject.PsMainGMod(gMod, mul); }
		public static void PsMainSimSpc(this Collision2D a, ParticleSystemSimulationSpace spc) { a.gameObject.PsMainSimSpc(spc); }
		public static void PsMainCusSimSpc(this Collision2D a, Transform tf) { a.gameObject.PsMainCusSimSpc(tf); }
		public static void PsMainSimSpd(this Collision2D a, float spd) { a.gameObject.PsMainSimSpd(spd); }
		public static void PsMainUseUnSclTm(this Collision2D a, bool isUse) { a.gameObject.PsMainUseUnSclTm(isUse); }
		public static void PsMainDt(this Collision2D a, bool isScl) { a.gameObject.PsMainDt(isScl); }
		public static void PsMainSclMd(this Collision2D a, ParticleSystemScalingMode md) { a.gameObject.PsMainSclMd(md); }
		public static void PsMainPlayOnAwake(this Collision2D a, bool playOnAwake) { a.gameObject.PsMainPlayOnAwake(playOnAwake); }
		public static void PsMainEmVelMd(this Collision2D a, ParticleSystemEmitterVelocityMode md) { a.gameObject.PsMainEmVelMd(md); }
		public static void PsMainMaxP(this Collision2D a, int maxP) { a.gameObject.PsMainMaxP(maxP); }
		public static void PsMainAutoRndSeed(this Collision2D a, bool isUse) { a.gameObject.PsMainAutoRndSeed(isUse); }
		public static void PsMainRndSeed(this Collision2D a, uint seed) { a.gameObject.PsMainRndSeed(seed); }
		public static void PsMainStopAct(this Collision2D a, ParticleSystemStopAction act) { a.gameObject.PsMainStopAct(act); }
		public static void PsMainCulMd(this Collision2D a, ParticleSystemCullingMode md) { a.gameObject.PsMainCulMd(md); }
		public static void PsMainRingBufMd(this Collision2D a, ParticleSystemRingBufferMode md) { a.gameObject.PsMainRingBufMd(md); }
		public static void PsMainRingBufLoopRng(this Collision2D a, Vector2 rng) { a.gameObject.PsMainRingBufLoopRng(rng); }
		public static ParticleSystem.EmissionModule PsEm(this Collision2D a) { return a.gameObject.PsEm(); }
		public static void PsEmRot(this Collision2D a, float tm) { a.gameObject.PsEmRot(tm); }
		public static void PsEmRot(this Collision2D a, ParticleSystem.MinMaxCurve tm, float mul) { a.gameObject.PsEmRot(tm, mul); }
		public static void PsEmRod(this Collision2D a, float dis) { a.gameObject.PsEmRod(dis); }
		public static void PsEmRod(this Collision2D a, ParticleSystem.MinMaxCurve dis, float mul) { a.gameObject.PsEmRod(dis, mul); }
		public static void PsEmSetBursts(this Collision2D a, params ParticleSystem.Burst[] bursts) { a.gameObject.PsEmSetBursts(bursts); }
		public static ParticleSystem.ShapeModule PsShp(this Collision2D a) { return a.gameObject.PsShp(); }
		public static ParticleSystem.VelocityOverLifetimeModule PsVol(this Collision2D a) { return a.gameObject.PsVol(); }
		public static void PsVolX(this Collision2D a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsVolX(x, mul); }
		public static void PsVolY(this Collision2D a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsVolY(y, mul); }
		public static void PsVolZ(this Collision2D a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsVolZ(z, mul); }
		public static void PsVolOrbX(this Collision2D a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsVolOrbX(x, mul); }
		public static void PsVolOrbY(this Collision2D a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsVolOrbY(y, mul); }
		public static void PsVolOrbZ(this Collision2D a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsVolOrbZ(z, mul); }
		public static void PsVolOrbOffX(this Collision2D a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsVolOrbOffX(x, mul); }
		public static void PsVolOrbOffY(this Collision2D a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsVolOrbOffY(y, mul); }
		public static void PsVolOrbOffZ(this Collision2D a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsVolOrbOffZ(z, mul); }
		public static void PsVolRadial(this Collision2D a, ParticleSystem.MinMaxCurve radial, float mul) { a.gameObject.PsVolRadial(radial, mul); }
		public static void PsVolSpdMod(this Collision2D a, ParticleSystem.MinMaxCurve mod, float mul) { a.gameObject.PsVolSpdMod(mod, mul); }
		public static void PsVolSpc(this Collision2D a, ParticleSystemSimulationSpace spc) { a.gameObject.PsVolSpc(spc); }
		public static ParticleSystem.LimitVelocityOverLifetimeModule PsLvol(this Collision2D a) { return a.gameObject.PsLvol(); }
		public static void PsLvolLim(this Collision2D a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLim(lim, mul); }
		public static void PsLvolLimX(this Collision2D a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLimX(lim, mul); }
		public static void PsLvolLimY(this Collision2D a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLimY(lim, mul); }
		public static void PsLvolLimZ(this Collision2D a, ParticleSystem.MinMaxCurve lim, float mul) { a.gameObject.PsLvolLimZ(lim, mul); }
		public static void PsLvolDrag(this Collision2D a, ParticleSystem.MinMaxCurve drag, float mul) { a.gameObject.PsLvolDrag(drag, mul); }
		public static void PsLvolSpc(this Collision2D a, ParticleSystemSimulationSpace spc) { a.gameObject.PsLvolSpc(spc); }
		public static void PsLvolDampen(this Collision2D a, float isDampen) { a.gameObject.PsLvolDampen(isDampen); }
		public static void PsLvolSepAxes(this Collision2D a, bool isAxes) { a.gameObject.PsLvolSepAxes(isAxes); }
		public static void PsLvolMulDragByPSz(this Collision2D a, bool isSz) { a.gameObject.PsLvolMulDragByPSz(isSz); }
		public static void PsLvolMulDragByPVel(this Collision2D a, bool isVel) { a.gameObject.PsLvolMulDragByPVel(isVel); }
		public static ParticleSystem.InheritVelocityModule PsIv(this Collision2D a) { return a.gameObject.PsIv(); }
		public static void PsIvMode(this Collision2D a, ParticleSystemInheritVelocityMode md) { a.gameObject.PsIvMode(md); }
		public static void PsIvCrv(this Collision2D a, ParticleSystem.MinMaxCurve crv, float mul) { a.gameObject.PsIvCrv(crv, mul); }
		public static ParticleSystem.ForceOverLifetimeModule PsFol(this Collision2D a) { return a.gameObject.PsFol(); }
		public static void PsFolX(this Collision2D a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsFolX(x, mul); }
		public static void PsFolY(this Collision2D a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsFolY(y, mul); }
		public static void PsFolZ(this Collision2D a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsFolZ(z, mul); }
		public static void PsFolSpc(this Collision2D a, ParticleSystemSimulationSpace spc) { a.gameObject.PsFolSpc(spc); }
		public static void PsFolRnd(this Collision2D a, bool isRnd) { a.gameObject.PsFolRnd(isRnd); }
		public static ParticleSystem.ColorOverLifetimeModule PsCol(this Collision2D a) { return a.gameObject.PsCol(); }
		public static void PsColC(this Collision2D a, ParticleSystem.MinMaxGradient c) { a.gameObject.PsColC(c); }
		public static ParticleSystem.ColorBySpeedModule PsCbs(this Collision2D a) { return a.gameObject.PsCbs(); }
		public static void PsCbsC(this Collision2D a, ParticleSystem.MinMaxGradient c) { a.gameObject.PsCbsC(c); }
		public static void PsCbsRng(this Collision2D a, Vector2 rng) { a.gameObject.PsCbsRng(rng); }
		public static ParticleSystem.SizeOverLifetimeModule PsSol(this Collision2D a) { return a.gameObject.PsSol(); }
		public static void PsSolSepAxes(this Collision2D a, bool isAxes) { a.gameObject.PsSolSepAxes(isAxes); }
		public static void PsSolSz(this Collision2D a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsSolSz(sz, mul); }
		public static void PsSolX(this Collision2D a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsSolX(x, mul); }
		public static void PsSolY(this Collision2D a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsSolY(y, mul); }
		public static void PsSolZ(this Collision2D a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsSolZ(z, mul); }
		public static ParticleSystem.SizeBySpeedModule PsSbs(this Collision2D a) { return a.gameObject.PsSbs(); }
		public static void PsSbsSepAxes(this Collision2D a, bool isAxes) { a.gameObject.PsSbsSepAxes(isAxes); }
		public static void PsSbsSz(this Collision2D a, ParticleSystem.MinMaxCurve sz, float mul) { a.gameObject.PsSbsSz(sz, mul); }
		public static void PsSbsX(this Collision2D a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsSbsX(x, mul); }
		public static void PsSbsY(this Collision2D a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsSbsY(y, mul); }
		public static void PsSbsZ(this Collision2D a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsSbsZ(z, mul); }
		public static void PsSbsRng(this Collision2D a, Vector2 rng) { a.gameObject.PsSbsRng(rng); }
		public static ParticleSystem.RotationOverLifetimeModule PsRol(this Collision2D a) { return a.gameObject.PsRol(); }
		public static void PsRolSepAxes(this Collision2D a, bool isAxes) { a.gameObject.PsRolSepAxes(isAxes); }
		public static void PsRolX(this Collision2D a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsRolX(x, mul); }
		public static void PsRolY(this Collision2D a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsRolY(y, mul); }
		public static void PsRolZ(this Collision2D a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsRolZ(z, mul); }
		public static ParticleSystem.RotationBySpeedModule PsRbs(this Collision2D a) { return a.gameObject.PsRbs(); }
		public static void PsRbsSepAxes(this Collision2D a, bool isAxes) { a.gameObject.PsRbsSepAxes(isAxes); }
		public static void PsRbsX(this Collision2D a, ParticleSystem.MinMaxCurve x, float mul) { a.gameObject.PsRbsX(x, mul); }
		public static void PsRbsY(this Collision2D a, ParticleSystem.MinMaxCurve y, float mul) { a.gameObject.PsRbsY(y, mul); }
		public static void PsRbsZ(this Collision2D a, ParticleSystem.MinMaxCurve z, float mul) { a.gameObject.PsRbsZ(z, mul); }
		public static void PsRbsRng(this Collision2D a, Vector2 rng) { a.gameObject.PsRbsRng(rng); }
		public static ParticleSystem.ExternalForcesModule PsEf(this Collision2D a) { return a.gameObject.PsEf(); }
		public static void PsEfMulCrv(this Collision2D a, ParticleSystem.MinMaxCurve crv, float mul) { a.gameObject.PsEfMulCrv(crv, mul); }
		public static void PsEfInfFilter(this Collision2D a, ParticleSystemGameObjectFilter filter) { a.gameObject.PsEfInfFilter(filter); }
		public static void PsEfInfMask(this Collision2D a, LayerMask lm) { a.gameObject.PsEfInfMask(lm); }
		public static ParticleSystem.NoiseModule PsNoise(this Collision2D a) { return a.gameObject.PsNoise(); }
		public static ParticleSystem.CollisionModule PsCollision(this Collision2D a) { return a.gameObject.PsCollision(); }
		public static ParticleSystem.TriggerModule PsTrigger(this Collision2D a) { return a.gameObject.PsTrigger(); }
		public static ParticleSystem.SubEmittersModule PsSubEm(this Collision2D a) { return a.gameObject.PsSubEm(); }
		public static ParticleSystem.TextureSheetAnimationModule PsTexSheetAnim(this Collision2D a) { return a.gameObject.PsTexSheetAnim(); }
		public static ParticleSystem.LightsModule PsLights(this Collision2D a) { return a.gameObject.PsLights(); }
		public static ParticleSystem.TrailModule PsTrails(this Collision2D a) { return a.gameObject.PsTrails(); }
		public static ParticleSystem.CustomDataModule PsCusDat(this Collision2D a) { return a.gameObject.PsCusDat(); }
		public static void RbG(this Collision2D a, bool useGravity = true) { a.gameObject.RbG(useGravity); }
		public static void RbNoG(this Collision2D a) { a.gameObject.RbNoG(); }
		public static void RbK(this Collision2D a, bool isKinematic = true) { a.gameObject.RbK(isKinematic); }
		public static void RbNoK(this Collision2D a) { a.gameObject.RbNoK(); }
		public static void RbInt(this Collision2D a, int interpolate) { a.gameObject.RbInt(interpolate); }
		public static void RbInt(this Collision2D a, RigidbodyInterpolation interpolate) { a.gameObject.RbInt(interpolate); }
		public static void RbInt0(this Collision2D a) { a.gameObject.RbInt0(); }
		public static void RbIntInt(this Collision2D a) { a.gameObject.RbIntInt(); }
		public static void RbIntExt(this Collision2D a) { a.gameObject.RbIntExt(); }
		public static void RbColDet(this Collision2D a, int collisionDetection) { a.gameObject.RbColDet(collisionDetection); }
		public static void RbColDet(this Collision2D a, CollisionDetectionMode collisionDetection) { a.gameObject.RbColDet(collisionDetection); }
		public static void RbColDetDis(this Collision2D a) { a.gameObject.RbColDetDis(); }
		public static void RbColDetCon(this Collision2D a) { a.gameObject.RbColDetCon(); }
		public static void RbColDetConD(this Collision2D a) { a.gameObject.RbColDetConD(); }
		public static void RbColDetConS(this Collision2D a) { a.gameObject.RbColDetConS(); }
		public static void RbCon(this Collision2D a, bool isFrzPosX = false, bool isFrzPosY = false, bool isFrzPosZ = false, bool isFrzRotX = false, bool isFrzRotY = false, bool isFrzRotZ = false) { a.gameObject.RbCon(isFrzPosX, isFrzPosY, isFrzPosZ, isFrzRotX, isFrzRotY, isFrzRotZ); }
		public static void RbCon0(this Collision2D a) { a.gameObject.RbCon0(); }
		public static void RbConFrzPos(this Collision2D a) { a.gameObject.RbConFrzPos(); }
		public static void RbConFrzRot(this Collision2D a) { a.gameObject.RbConFrzRot(); }
		public static void RbConFrzAll(this Collision2D a) { a.gameObject.RbConFrzAll(); }
		public static void RbAv0(this Collision2D a) { a.gameObject.RbAv0(); }
		public static void RbAv(this Collision2D a, Vector3 v) { a.gameObject.RbAv(v); }
		public static void RbAvx(this Collision2D a, float x) { a.gameObject.RbAvx(x); }
		public static void RbAvy(this Collision2D a, float y) { a.gameObject.RbAvy(y); }
		public static void RbAvz(this Collision2D a, float z) { a.gameObject.RbAvz(z); }
		public static void RbAvxy(this Collision2D a, float x, float y) { a.gameObject.RbAvxy(x, y); }
		public static void RbAvxz(this Collision2D a, float x, float z) { a.gameObject.RbAvxz(x, z); }
		public static void RbAvyz(this Collision2D a, float y, float z) { a.gameObject.RbAvyz(y, z); }
		public static void RbAv(this Collision2D a, float x, float y, float z) { a.gameObject.RbAv(x, y, z); }
		public static void RbV0(this Collision2D a) { a.gameObject.RbV0(); }
		public static void RbV(this Collision2D a, Vector3 v) { a.gameObject.RbV(v); }
		public static void RbVx(this Collision2D a, float x) { a.gameObject.RbVx(x); }
		public static void RbVy(this Collision2D a, float y) { a.gameObject.RbVy(y); }
		public static void RbVz(this Collision2D a, float z) { a.gameObject.RbVz(z); }
		public static void RbVxy(this Collision2D a, float x, float y) { a.gameObject.RbVxy(x, y); }
		public static void RbVxz(this Collision2D a, float x, float z) { a.gameObject.RbVxz(x, z); }
		public static void RbVyz(this Collision2D a, float y, float z) { a.gameObject.RbVyz(y, z); }
		public static void RbV(this Collision2D a, float x, float y, float z) { a.gameObject.RbV(x, y, z); }
	}
}