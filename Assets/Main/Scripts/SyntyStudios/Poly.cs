using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

namespace Polygon {
    public partial class Poly : Mb {
        public enum Tile { O, FL, FR, BL, BR }
        public static int DirI(char c) { return "<^>v".IndexOf(c.L()); }
        public static int DirI(string d, int i) { return i < 0 || i >= d.Length ? 9 : DirI(d[i]); }
        public static Vector2Int DirV2I(int d) { return V2I.V(d == 0 ? -1 : d == 2 ? 1 : 0, d == 3 ? -1 : d == 1 ? 1 : 0); }
        public static Vector2Int DirV2I(char c) { return DirV2I(DirI(c)); }
        public static Vector3 DirV3(int d) { return V3.Xz(d == 0 ? -1 : d == 2 ? 1 : 0, d == 3 ? -1 : d == 1 ? 1 : 0); }
        public static Vector3 DirV3(char c) { return DirV3(DirI(c)); }
        public static int DirCor(int d1, int d2) {
            int d = d2 - d1;
            return d == 1 || d == -3 ? 1 : d == -1 || d == 3 ? -1 : 0;
        }
        public static bool IsFill(string data) {
            string[] datas = data.RgxSplitSpc();
            Vector2Int p = V2I.O;
            for (int i = 0; i < datas.Length; i++)
                p += DirV2I(datas[i][0]) * datas[i].Sub(1).I();
            return p == V2I.O;
        }
        public static bool IsXz0(Vector3 p) { return M.Is0(p.x) && M.Is0(p.z); }
        public static int DirCor(char c1, char c2) { return DirCor(DirI(c1), DirI(c2)); }
        public static GameObject CrtPfGo(GameObject pf, Tf tf, Transform parTf, Material mat) {
            GameObject go = Ins(pf, tf.p, tf.q, parTf);
            go.Tls(tf.s);
            if (mat)
                go.RenMat(mat);
            return go;
        }
        public static Transform NewGoTf(string name, Tf tf, Transform parTf) {
            GameObject go = new GameObject(name);
            go.Tlp(tf.p);
            go.Tle(tf.r);
            go.Tls(tf.s);
            go.Par(parTf);
            return go.transform;
        }
        public static Tile TileRev(Tile t, bool isRevX = true, bool isRevZ = true) {
            return isRevX && isRevZ ? (t == Tile.FL ? Tile.BR : t == Tile.FR ? Tile.BL : t == Tile.BL ? Tile.FR : t == Tile.BR ? Tile.FL : Tile.O) :
                isRevX ? (t == Tile.FL ? Tile.FR : t == Tile.FR ? Tile.FL : t == Tile.BL ? Tile.BR : t == Tile.BR ? Tile.BL : Tile.O) :
                isRevZ ? (t == Tile.FL ? Tile.BL : t == Tile.FR ? Tile.BR : t == Tile.BL ? Tile.FL : t == Tile.BR ? Tile.FR : Tile.O) : t;
        }
        public static Tf TileTf(Tile t, float sz, float ry = 0) {
            return Tf.PR((t == Tile.FL ? V3.nzp : t == Tile.FR ? V3.pzp : t == Tile.BL ? V3.nzn : t == Tile.BR ? V3.pzn : V3.O) * (sz / 2), ry);
        }
        public static Tf TileRevTf(Tile t, float sz, float ry = 0, bool isRevX = true, bool isRevZ = true) {
            return TileTf(TileRev(t, isRevX, isRevZ), sz, ry);
        }
        public static void CrtTile(GameObject pf, Vector3 p, float ry, Vector3 p2, float ry2, Tile t = Tile.O, float sz = 5, Transform parTf = null, Material mat = null) {
            CrtPfGo(pf, Tf.PR(p, ry) * Tf.PR(p2, ry2) * TileRevTf(t, sz), parTf, mat);
        }
    }
}