﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using TMPro;

public class Character : Mb {
    public LeaderBoardData data;
    HWP hwp = null;
    TextMeshPro followTmp;
    public void Init() {
        tf.Par(Bs.I.transform);
    }
    public void UpdatePlace(int place) { }
    public void UpdateColor(Color col) { }
    // default functions
    public void CreateName(LeaderBoardData lbd) {
        UpdateColor(lbd.col);
        data = lbd;
        if (Gc.I.followType != FollowType.None) {
            hwp = go.Gc<HWP>();
            if (hwp) {
                hwp.info.arrow.size = Screen.width * 0.06f;
                HWPManager.I.TextStyle.fontSize = M.RoundI(Screen.width * 0.06f);
                HWPManager.I.TextStyle.contentOffset = V2.u * -(Screen.width * 0.06f * 1.6f);
                hwp.info.text = Gc.I.isNameUppercase ? data.name.ToUpper() : data.name;
                hwp.info.color = data.col;
                hwp.info.character = this;
            }
            if (Gc.I.followType == FollowType.Follow || Gc.I.followType == FollowType.FollowPointer) {
                Follow follow = Crt.Pf<Follow>(A.LoadGo("Main/FollowName"), new Tf(Tp), Gc.I.transform);
                follow.followTf = transform;
                followTmp = follow.Child<TextMeshPro>(0);
                if (followTmp) {
                    followTmp.text = Gc.I.isNameUppercase ? data.name.ToUpper() : data.name;
                    followTmp.color = data.col;
                }
            }
        }
    }
    public void RemoveName() {
        if (Gc.I.followType != FollowType.None) {
            hwp.info.text = "";
            hwp.info.arrow.size = 0;
            if ((Gc.I.followType == FollowType.Follow || Gc.I.followType == FollowType.FollowPointer) && followTmp)
                followTmp.text = "";
        }
    }
}