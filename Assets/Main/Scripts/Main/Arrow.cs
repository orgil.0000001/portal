﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Arrow : Mb {
    [HideInInspector]
    public bool isShot = false;
    public float pwr = 0;
    [HideInInspector]
    public Transform shotPntTf;
    void Start() {
    }
    void Update() {
        if (shotPntTf)
            shotPntTf.position = Tp;
    }
    public void Shot(float pwr) {
        P = Gc.I.tf;
        isShot = true;
        this.pwr = pwr;
        rb.V(F * pwr);
        Dst(go, 5);
    }
}