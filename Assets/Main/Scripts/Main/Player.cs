﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using System.IO;
using System.Linq;

public class Player : Character {
    public static Player I {
        get {
            if (_.Null())
                _ = GameObject.FindObjectOfType<Player>();
            return _;
        }
    }
    static Player _;
    Pipe pipe = null;
    Vector3 pos;
    bool isMv = false, isSta = false, isPortal = false;
    public float pwr1, pwr2;
    float jumpY = -4;
    int diamondCnt = 0;
    [HideInInspector]
    public Animator an;
    private void Awake() {
        _ = this;
    }
    public void Reset() {
    }
    void Start() {
    }
    void Update() {
        if (IsPlaying) {
            if (IsMbD) {
                mp = Mp;
            }
            if (IsMbU) {
                if (V3.Dis(mp, Mp) > 10 && !isMv && (isSta ? rb.velocity.y > 0 && Tp.y > -1 : true)) {
                    float ang = Ang.LookF(mp, Mp);
                    Vector3 dir = (315 < ang || ang < 45 ? V3.f : ang < 135 ? V3.r : ang < 225 ? V3.b : V3.l) * Ls.I.spc;
                    if (Physics.Raycast(Tp.Y(1) + dir, V3.d, 10, Lm.Floor | Lm.Pipe)) {
                        isMv = true;
                        pos = Tp.Pzp() + dir;
                        if (!isSta) {
                            Jump();
                            isSta = true;
                            an.Play("JumpUp");
                        }
                    }
                }
            }
            if (isMv) {
                Tp = V3.Move(Tp.Pzp(), pos, Dt * 10).Y(Tp.y);
            }
            if (Tp.y < jumpY) {
                Tp = Ls.I.Portal(pipe).Tp.Y(jumpY);
                Jump();
            }
        }
    }
    void Jump() {
        rb.V(V3.Y(isSta ? pwr2 : pwr1));
    }
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.Pipe)) {
            isMv = false;
            pipe = other.Gc<Pipe>();
        } else if (other.Tag(Tag.Diamond)) {
            Destroy(other.gameObject);
            diamondCnt++;
            Gc.Diamond++;
            Cc.I.HudDiamond(Gc.Diamond);
            Cc.I.HudLevelBar(Gc.Level, diamondCnt.F() / Ls.I.diamondGos.Count);
        }
    }
    private void OnCollisionEnter(Collision collision) {
        if (collision.Tag(Tag.Floor)) {
            an.Play("JumpDown");
            if (diamondCnt == Ls.I.diamondGos.Count)
                Gc.I.LevelCompleted();
            else
                Gc.I.GameOver();
        }
    }
}