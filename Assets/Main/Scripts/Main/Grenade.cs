using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using Polygon;

public class Grenade : Mb {
    [HideInInspector]
    public bool isShot = false;
    PolyFX expFx;
    float dstTime;
    void Start() {
    }
    public void Shot(PolyFX expFx, float dstTime) {
        this.expFx = expFx;
        this.dstTime = dstTime;
        isShot = true;
    }
    private void OnCollisionEnter(Collision other) {
        if (isShot) {
            GameObject expGo = Ins(Polygon.Poly.GetFxPf(expFx), Tp, Q.O, Gc.I.tf);
            Dst(expGo, dstTime);
            Dst(go);
        }
    }
}