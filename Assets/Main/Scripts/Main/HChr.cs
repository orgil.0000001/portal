using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class CusData {
    public List<int> data, prices;
    public List<GameObject> gos = new List<GameObject>();
    public CusData(params int[] prices) {
        this.prices = prices.List();
    }
}

public enum ChrTp { Basic, Soldier, Thief, Boy, Girl, Swimmer, Zombie }
public enum GlassesTp { None, Black, White }
public enum ColTp { None, Red, Orange, Yellow, Green, Blue, Pink }
public enum CusTp { Chr, Cloth, Face }

public class HChr : Singleton<HChr> {
    public GameObject characters, clothes, faces;
    public List<Material> chrMats;
    public List<Material> faceMats;
    public Color btnDefC, btnDefC2, btnC, btnC2, btnSelC, btnSelC2;
    CusData chr = new CusData(
        0, 500, 1000,
        100, 110, 120, 130, 140, 150,
        200, 210, 220, 230, 240, 250,
        300, 310, 320, 330, 340, 350,
        400, 410, 420, 430, 440, 450
    ), cloth = new CusData(
        50, 60,
        100, 110, 120, 130, 140, 150,
        100, 110, 120, 130, 140, 150,
        200, 210, 220, 230, 240, 250,
        300, 310, 320, 330, 340, 350,
        100, 110, 120, 130, 140, 150
    ), face = new CusData(
        100, 110, 120, 130, 140,
        150, 160, 170, 180, 190,
        200, 210, 220, 230, 240,
        250, 260, 270, 280, 290,
        300, 310, 320, 330, 340
    );
    private void Start() {
        // character
        for (int i = 1; i <= 5; i++)
            for (int j = 0, n = i == 1 ? 3 : 6; j < n; j++) {
                GameObject cGo = characters.ChildGo(i, 1, 0, j);
                int k = chr.gos.Count;
                chr.gos.Add(cGo);
                cGo.ChildGo(2).Btn().onClick.AddListener(() => BtnClk(CusTp.Chr, k));
            }
        if (Data.Characters.S().IsNe())
            Data.Characters.SetList(List(2).Add(0.ListN(26)));
        chr.data = Data.Characters.ListI();
        Upd(chr);
        // cloth
        for (int i = 1; i <= 6; i++)
            for (int j = 0, n = i == 1 ? 2 : 6; j < n; j++) {
                GameObject cGo = clothes.ChildGo(i, 1, 0, j);
                int k = cloth.gos.Count;
                cloth.gos.Add(cGo);
                cGo.ChildGo(2).Btn().onClick.AddListener(() => BtnClk(CusTp.Cloth, k));
            }
        if (Data.Clothes.S().IsNe())
            Data.Clothes.SetList(0.ListN(32));
        cloth.data = Data.Clothes.ListI();
        Upd(cloth);
        // face
        for (int i = 0; i < 25; i++) {
            GameObject cGo = faces.ChildGo(1, i);
            int k = face.gos.Count;
            face.gos.Add(cGo);
            cGo.ChildGo(2).Btn().onClick.AddListener(() => BtnClk(CusTp.Face, k));
        }
        if (Data.Faces.S().IsNe())
            Data.Faces.SetList(0.ListN(25));
        face.data = Data.Faces.ListI();
        Upd(face);
        UpdChr();
    }
    public void ShowCharacters() { characters.Show(); }
    public void HideCharacters() { characters.Hide(); UpdChr(); }
    public void ShowClothes() { clothes.Show(); }
    public void HideClothes() { clothes.Hide(); UpdChr(); }
    public void ShowFaces() { faces.Show(); }
    public void HideFaces() { faces.Hide(); UpdChr(); }
    public void UpdChr() {
        int chrIdx = chr.data.IndexOf(2), capIdx = cloth.data.Rng(2, 6).IndexOf(2);
        ChrTp chrTp = (ChrTp)(chrIdx < 3 ? chrIdx : (chrIdx - 3) / 6 + 3);
        ColTp chrCt = (ColTp)(chrIdx < 3 ? 0 : (chrIdx - 3) % 6 + 1);
        GlassesTp glassesTp = (GlassesTp)(cloth.data.Rng(0, 2).IndexOf(2) + 1);
        bool isCap = capIdx >= 0;
        ColTp hatCt = (ColTp)(isCap ? capIdx + 1 : cloth.data.Rng(8, 6).IndexOf(2) + 1);
        ColTp headphonesCt = (ColTp)(cloth.data.Rng(14, 6).IndexOf(2) + 1);
        ColTp jerseyCt = (ColTp)(cloth.data.Rng(20, 6).IndexOf(2) + 1);
        ColTp glovesCt = (ColTp)(cloth.data.Rng(26, 6).IndexOf(2) + 1);
        int fc = face.data.IndexOf(2) + 1;
        Chr(chrTp, chrCt, jerseyCt, glovesCt, headphonesCt, glassesTp, isCap, hatCt, fc);
    }
    List<List<int>> idxs = List(List(3, 2, 4, 5, 0, 1), List(4, 3, 5, 0, 1, 2), List(1, 0, 2, 3, 4, 5), List(2, 1, 3, 4, 5, 0));
    public void Chr(ChrTp chrTp, ColTp chrCt, ColTp jerseyCt, ColTp glovesCt, ColTp headphonesCt, GlassesTp glassesTp, bool isCap, ColTp hatCt, int face) {
        GameObject go = Player.I.go.ChildGo(0), chrGo = go.ChildGo((int)chrTp), spnGo = chrGo.ChildGo(1, 2, 0, 0);
        Player.I.an = chrGo.An();
        go.Childs().ForEach(x => x.Hide());
        Show(chrGo, true, (int)chrTp >= 3 && chrCt != ColTp.None, (int)chrTp >= 3 && chrCt != ColTp.None ? chrMats[idxs[(int)chrTp - 3][(int)chrCt - 1]] : null, true); // Character
        Show(chrGo.ChildGo(2), 2, jerseyCt, true, true); // Jersey
        Show(spnGo.ChildGo(0, 0, 0, 0, 0), 2, glovesCt); // Gloves
        Show(spnGo.ChildGo(2, 0, 0, 0, 0), 2, glovesCt);
        Show(spnGo.ChildGo(1, 0, 4), 0, headphonesCt); // Headphones
        spnGo.ChildGo(1, 0, 1).Active(glassesTp == GlassesTp.Black); // Glasses
        spnGo.ChildGo(1, 0, 2).Active(glassesTp == GlassesTp.White);
        Show(spnGo.ChildGo(1, 0, 0), 1, hatCt, isCap); // Hat
        Show(spnGo.ChildGo(1, 0, 5), 0, hatCt, !isCap);
        Show(spnGo.ChildGo(1, 0, 3), face > 0, face > 0, face > 0 ? faceMats[face - 1] : null); // Face
    }
    void Show(GameObject go, int idx, ColTp ct, bool isShow = true, bool isChild = false) { Show(go, isShow = isShow && ct != ColTp.None, isShow, isShow ? chrMats[idxs[idx][(int)ct - 1]] : null, isChild); }
    void Show(GameObject go, bool isShow, bool isMat, Material mat, bool isChild = false) {
        go.Active(isShow);
        if (isMat)
            (isChild ? go.ChildGo(0) : go).RenMat(mat);
    }
    public void BtnClk(CusTp tp, int i) {
        CusData cus = tp == CusTp.Chr ? chr : tp == CusTp.Cloth ? cloth : face;
        Data key = tp == CusTp.Chr ? Data.Characters : tp == CusTp.Cloth ? Data.Clothes : Data.Faces;
        if (cus.data[i] == 0 && cus.prices[i] <= Gc.Diamond || cus.data[i] == 1) {
            int staIdx = tp == CusTp.Cloth ? i < 2 ? 0 : i < 14 ? 2 : 14 + (i - 14) / 6 * 6 : 0;
            int sel = (tp == CusTp.Cloth ? cus.data.Rng(staIdx, i < 2 ? 2 : i < 14 ? 12 : 6) : cus.data).IndexOf(2);
            if (sel >= 0) {
                cus.data[staIdx + sel] = 1;
                Upd(cus, staIdx + sel);
            }
            if (cus.data[i] == 0) {
                Gc.Diamond -= cus.prices[i];
                Cc.I.HudDiamond(Gc.Diamond);
            }
            cus.data[i] = 2;
            Upd(cus, i);
            key.SetList(cus.data);
        } else if (tp != CusTp.Chr && cus.data[i] == 2) {
            cus.data[i] = 1;
            Upd(cus, i);
            key.SetList(cus.data);
        }
    }
    void Upd(CusData data) {
        for (int i = 0; i < data.gos.Count; i++)
            Upd(data, i);
    }
    void Upd(CusData data, int i) {
        GameObject btnGo = data.gos[i].ChildGo(2);
        btnGo.Img().color = data.data[i] == 0 ? btnDefC : data.data[i] == 1 ? btnC : btnSelC;
        btnGo.Gc<ShadowButton>().effectColor = data.data[i] == 0 ? btnDefC2 : data.data[i] == 1 ? btnC2 : btnSelC2;
        btnGo.Child(0).Txt().text = "" + data.prices[i];
    }
}
