﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum TutorialType { Line, Infinity, TapToPlay, InputName }

public class Cc : Singleton<Cc> { // Canvas Controller
    [Header("Panels")]
    public GameObject menu;
    public GameObject hud, pause;
    public GameOver gameOverComplete, gameOverUnCompleted;
    public NewBest newBest;
    public LevelCompleted levelCompleted;
    [Header("Hud items")]
    public HudTime hudTime;
    public FormatTxt hudKills, hudScore, hudBest, hudDiamond, hudCoin, hudLife;
    public LevelBar hudLevelBar;
    public LeaderBoard hudLeaderBoard;
    GameState tmpGs;
    InputField playerNameInp;
    float time = 0;
    private void Start() {
        Show(menu);
        Hide(hud);
        Hide(gameOverComplete);
        Hide(gameOverUnCompleted);
        Hide(newBest);
        Hide(levelCompleted);
        Hide(pause);
        ReadPlayerName();
    }
    private void Update() {
        if (IsPlaying) {
            if (hudLeaderBoard)
                HudLeaderBoard(LeaderBoardData.GetDatas());
            if (hudTime) {
                time += Dt;
                if (Gc.GameTime - time > 0)
                    HudTime(Gc.GameTime - time);
                else {
                    HudTime(0);
                    Gc.I.GameOver();
                }
            }
        }
    }
    void ReadPlayerName() {
        GameObject playerNameGo = GameObject.Find("PlayerName");
        if (playerNameGo) {
            playerNameInp = playerNameGo.Inp();
            if (playerNameInp)
                playerNameInp.text = Gc.PlayerName = Data.PlayerName.S();
        }
    }
    public void HudScore() { FtData(hudScore, Gc.Score); }
    public void HudScore(float score) { FtData(hudScore, score); }
    public void HudBest() { FtData(hudBest, Gc.Best); }
    public void HudBest(float best) { FtData(hudBest, best); }
    public void HudCoin() { FtData(hudCoin, Gc.Coin); }
    public void HudCoin(int coin) { FtData(hudCoin, coin); }
    public void HudDiamond() { FtData(hudDiamond, Gc.Diamond); }
    public void HudDiamond(int diamond) { FtData(hudDiamond, diamond); }
    public void HudKills() { FtData(hudKills, Gc.Kills); }
    public void HudKills(int kills) { FtData(hudKills, kills); }
    public void HudLife() { FtData(hudLife, Gc.Life); }
    public void HudLife(int life) { FtData(hudLife, life); }
    public void HudTime() { if (hudTime) hudTime.Data(time); }
    public void HudTime(float time) { if (hudTime) hudTime.Data(time); }
    public void HudLevelBar(int level = 1, float scoreT = 0, float bestT = 0, string txt = "", int stage = 1, bool isNxtLvlCol = true) { if (hudLevelBar) hudLevelBar.Data(level, scoreT, bestT, txt, stage, isNxtLvlCol); }
    public void HudLeaderBoard(List<LeaderBoardData> datas = null) { if (hudLeaderBoard) hudLeaderBoard.Data(datas); }
    public void ShowHud() {
        Hide(menu);
        Show(hud);
    }
    public void GameOver(float time = 0, float score = 0, float best = 0, int level = 1, List<LeaderBoardData> datas = null, bool isComplete = true) { StaCor(GameOverCor(time, score, best, level, datas, isComplete)); }
    IEnumerator GameOverCor(float time, float score, float best, int level, List<LeaderBoardData> datas, bool isComplete) {
        yield return Wf.S(time);
        if (isComplete) {
            if (gameOverComplete) {
                Hide(hud);
                Show(gameOverComplete);
                gameOverComplete.Data(score, best, level, datas);
            }
        } else {
            if (gameOverUnCompleted) {
                Hide(hud);
                Show(gameOverUnCompleted);
                gameOverUnCompleted.Data(score, best, level, datas);
            }
        }
    }
    public void NewBest(float time = 0, float score = 0, int level = 1, List<LeaderBoardData> datas = null) { StaCor(NewBestCor(time, score, level, datas)); }
    public IEnumerator NewBestCor(float time, float score, int level, List<LeaderBoardData> datas) {
        yield return Wf.S(time);
        if (newBest) {
            Hide(hud);
            Show(newBest);
            newBest.Data(score, level, datas);
        }
    }
    public void LevelCompleted(float time = 0, int level = 1, List<LeaderBoardData> datas = null) { StaCor(LevelCompletedCor(time, level, datas)); }
    IEnumerator LevelCompletedCor(float time, int level, List<LeaderBoardData> datas) {
        yield return Wf.S(time);
        if (levelCompleted) {
            Hide(hud);
            Show(levelCompleted);
            levelCompleted.Data(level, datas);
        }
    }
    public void Pause(float time = 0) { StaCor(PauseCor(time)); }
    IEnumerator PauseCor(float time) {
        yield return Wf.S(time);
        if (pause) {
            Show(pause);
            Time.timeScale = 0;
            tmpGs = Gc.State;
            Gc.State = GameState.Pause;
        }
    }
    public void Resume(float time = 0) { StaCor(ResumeCor(time)); }
    IEnumerator ResumeCor(float time) {
        yield return Wf.S(time);
        if (pause) {
            Hide(pause);
            Time.timeScale = 1;
            Gc.State = tmpGs;
        }
    }
    public void Replay(float time = 0) { StaCor(ReplayCor(time)); }
    IEnumerator ReplayCor(float time) {
        yield return Wf.S(time);
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void Play() {
        if (playerNameInp)
            Gc.PlayerName = playerNameInp.text;
        Data.PlayerName.Set(Gc.PlayerName);
        LeaderBoardData.SetPlayerData(Gc.PlayerName);
        Gc.I.Play();
    }
    void FtData(FormatTxt txt, float data) { if (txt) txt.Data(data); }
    void Hide(GameObject a) { if (a) a.Hide(); }
    void Show(GameObject a) { if (a) a.Show(); }
    void Hide(Component a) { if (a) a.Hide(); }
    void Show(Component a) { if (a) a.Show(); }
}