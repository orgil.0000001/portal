﻿// #define USE_FB_SDK
// #define USE_GA_SDK
// #define USE_ADJUST_SDK

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

#if USE_FB_SDK
using Facebook.Unity;
#endif

#if USE_ADJUST_SDK
using com.adjust.sdk;
#endif

public enum GameState { Starting, Playing, Pause, LevelCompleted, GameOver, Settings, Shifting }
public enum Data { IsWin, Score, Best, Level, Diamond, Coin, PlayerName, IsVib, LevelData, Characters, Clothes, Faces }
public enum FollowType { None, Hud, HudPointer, HudPointerOff, Follow, FollowPointer, FollowPointerOff }

public class Gc : Singleton<Gc> { // Game Controller
    public static GameState State;
    public static bool IsWin => Data.IsWin.B();
    public static float Score, GameTime;
    public static float Best => Data.Best.F();
    public static int Level, Diamond, Coin, Kills, Life, PlayerSkinIdx;
    public static string PlayerName;
    public static bool IsVib;
    // Data-д хадгалж болох төрөл: bool, int, float, string, Vector2, Vector2Int, Vector3, Vector3Int, Vector4, Color
    public static Dictionary<Data, object> Datas = new Dictionary<Data, object>() { { Data.IsWin, true }, { Data.Score, 0f }, { Data.Best, 0f }, { Data.Level, 1 }, { Data.Diamond, 0 }, { Data.Coin, 0 }, { Data.PlayerName, "Player" }, { Data.IsVib, true }, { Data.LevelData, "" }, { Data.Characters, "" }, { Data.Clothes, "" }, { Data.Faces, "" } };
#if USE_FB_SDK
    static bool IsFbInit = false;
#endif
    [Header("UI")]
    public bool isStartButton = false;
    public bool isNewBest = true, isPauseButton = false;
    [Header("Game")]
    public float gameTime = 120;
    public float gameOverWaitTime = 0, levelCompletedWaitTime = 0;
    [Header("LeaderBoard")]
    public bool isLbhBgColConst = true;
    public Color LbhPlayerCol = new Color32(255, 255, 255, 100), LbhBotCol = new Color32(0, 0, 0, 100);
    public bool isLbgNameUseCol = true;
    public Color LbgPlayerCol = new Color32(255, 255, 255, 100), LbgBotCol = C.O;
    [Header("Character")]
    public int showFollowLive = 3;
    public FollowType followType;
    [DrawIf("followType", typeof(int), (int)FollowType.None, CompTp.NotEqual)]
    public bool isNameUppercase = false;
    public Color playerCol = new Color32(244, 67, 54, 255);
    public List<Color> botCols = new List<Color>() { new Color32(233, 30, 99, 255), new Color32(156, 39, 176, 255), new Color32(103, 58, 183, 255), new Color32(63, 81, 181, 255), new Color32(33, 150, 243, 255), new Color32(3, 169, 244, 255), new Color32(0, 188, 212, 255), new Color32(0, 150, 136, 255), new Color32(76, 175, 80, 255), new Color32(139, 195, 74, 255), new Color32(205, 220, 57, 255), new Color32(255, 235, 59, 255), new Color32(255, 193, 7, 255), new Color32(255, 152, 0, 255), new Color32(255, 87, 34, 255), new Color32(121, 85, 72, 255), new Color32(158, 158, 158, 255), new Color32(96, 125, 139, 255) };
    [Header("Player Settings Change")]
    public string companyName = "Aravt Games";
    public string productName = "Game Name", version = "1", bundleIdentifier = "", companyWebSite = "aravtgames.tk";
    [Header("Screenshot")]
    public string screenshotName = "icon";
    void Awake() {
#if USE_GA_SDK
        GameAnalyticsSDK.GameAnalytics.Initialize();
#endif
        Application.targetFrameRate = 60;
        if (Application.isEditor) {
            A.SetGameViewScale();
            IO.CheckCrtDir("Build");
            if (!IO.IsFileExists(".gitignore"))
                IO.CrtFile(".gitignore", IO.ReadFile(IO.ToolsPath + "gitignore"));
            // ProjectSettings / Editor / Version Control / Mode-г Visible Meta Files болгох
            if (!IO.ReadFile(IO.PsPath + "EditorSettings.asset").Contains("Visible Meta Files"))
                IO.SetMetaLine("ProjectSettings/EditorSettings.asset", "m_ExternalVersionControlSupport", "m_ExternalVersionControlSupport: Visible Meta Files");
            IO.TagLyr();
            IO.ExtEnum();
            IO.ExtGo();
        }
    }
    void Start() {
#if USE_ADJUST_SDK && UNITY_IOS
        InitAdjust("YOUR_IOS_APP_TOKEN_HERE");
#elif USE_ADJUST_SDK && UNITY_ANDROID
        InitAdjust("YOUR_ANDROID_APP_TOKEN_HERE");
#endif
        InitFb();
        State = GameState.Starting;
        // update data
        GameTime = gameTime;
        Score = Data.Score.F();
        Coin = Data.Coin.I();
        Diamond = Data.Diamond.I();
        Level = Data.Level.I();
        IsVib = Data.IsVib.B();
        Kills = 0;
        Life = 1;
        // show ui
        Cc.I.HudScore(Score);
        Cc.I.HudBest(Best);
        Cc.I.HudCoin(Coin);
        Cc.I.HudDiamond(Diamond);
        Cc.I.HudKills(Kills);
        Cc.I.HudLife(Life);
        Cc.I.HudLevelBar(Level);
        Es.I.Init();
        Ls.I.Init();
    }
    void InitFb() {
#if USE_FB_SDK
        if (IsFbInit)
            return;
        if (!FB.IsInitialized) {
            FB.Init(FbCallback);
        } else {
            FbCallback();
        }
#endif
    }
    void FbCallback() {
#if USE_FB_SDK
        if (FB.IsInitialized) {
            FB.ActivateApp();
            IsFbInit = true;
        }
#endif
    }
    private void InitAdjust(string adjustAppToken) {
#if USE_ADJUST_SDK
        var adjustConfig = new AdjustConfig(
            adjustAppToken,
            AdjustEnvironment.Production, // AdjustEnvironment.Sandbox to test in dashboard
            true
        );
        adjustConfig.setLogLevel(AdjustLogLevel.Info); // AdjustLogLevel.Suppress to disable logs
        adjustConfig.setSendInBackground(true);
        new GameObject("Adjust").AddComponent<Adjust>(); // do not remove or rename
        // Adjust.addSessionCallbackParameter("foo", "bar"); // if requested to set session-level parameters
        //adjustConfig.setAttributionChangedDelegate((adjustAttribution) => {
        //  Debug.LogFormat("Adjust Attribution Callback: ", adjustAttribution.trackerName);
        //});
        Adjust.start(adjustConfig);
#endif
    }
    void Update() {
        if (IsStarting && !isStartButton && IsMbD)
            Play();
        if (IsPause && !isPauseButton && IsMbD)
            Cc.I.Resume();
    }
    public void Play() {
        Player.I.Reset();
        State = GameState.Playing;
        Player.I.mp = Mp;
        if (Level == 1) {
            Ls.I.lvl1Go.Show();
            Dst(Ls.I.lvl1Go, Ls.I.lvlTxtDstTm);
        } else if (Level == 2) {
            Ls.I.lvl2Go.Show();
            Dst(Ls.I.lvl2Go, Ls.I.lvlTxtDstTm);
        }
        Cc.I.ShowHud();
    }
    public void GameOver(bool isComplete = true) { GameOver(isComplete, LeaderBoardData.GetDatas()); }
    public void GameOver(bool isComplete, List<LeaderBoardData> datas) {
        if (IsPlaying) {
            LeaderBoardData.RemoveNames();
            State = GameState.GameOver;
            Data.IsWin.Set(false);
            Data.Score.Set(0f);
            Data.Diamond.Set(Diamond);
            Data.Coin.Set(Coin);
            if (Best < Score) {
                Data.Best.Set(Score);
                if (isNewBest)
                    Cc.I.NewBest(gameOverWaitTime, Score, Level, datas);
                else
                    Cc.I.GameOver(gameOverWaitTime, Score, Best, Level, datas, isComplete);
            } else
                Cc.I.GameOver(gameOverWaitTime, Score, Best, Level, datas, isComplete);
        }
    }
    public void LevelCompleted() { LevelCompleted(LeaderBoardData.GetDatas()); }
    public void LevelCompleted(List<LeaderBoardData> datas) {
        if (IsPlaying) {
            State = GameState.LevelCompleted;
            Level++;
            Data.IsWin.Set(true);
            Data.Score.Set(Score);
            Data.Diamond.Set(Diamond);
            Data.Coin.Set(Coin);
            Data.Level.Set(Level);
            Cc.I.LevelCompleted(levelCompletedWaitTime, Level - 1, datas);
        }
    }
    void OnApplicationQuit() {
        Time.timeScale = 1;
        Data.Score.Set(0f);
        Data.Diamond.Set(Diamond);
        Data.Coin.Set(Coin);
    }
    public void VarTimeSet<T, V>(T obj, string name, V value, float time) { StaCor(VarTimeSetCor(obj, name, value, time)); }
    IEnumerator VarTimeSetCor<T, V>(T obj, string name, V value, float time) {
        System.Reflection.FieldInfo f = A.Field<T>(obj, name);
        f.SetValue(obj, new VarTime<V>(((VarTime<V>)f.GetValue(obj)).val, Time.time));
        yield return Wf.S(time + 0.001f);
        if (((VarTime<V>)f.GetValue(obj)).t + time <= Time.time)
            f.SetValue(obj, new VarTime<V>(value, Time.time));
    }
    public void AnimTxtDst(GameObject go, List<Fo> lis, bool isSmt = true, string word = "", bool isPerfect = true) { StaCor(AnimTxtDstCor(go, lis, isSmt, word, isPerfect)); }
    public IEnumerator AnimTxtDstCor(GameObject go, List<Fo> lis, bool isSmt = true, string word = "", bool isPerfect = true) {
        go.ChildGo(0).SetText(word == "" ? Rnd.List(isPerfect ? A.PerfectWords : A.GoodWords) : word);
        yield return AnimCor(go, lis, isSmt);
        Dst(go);
    }
    public void Anim(GameObject go, List<Fo> lis, bool isSmt = true) { StaCor(AnimCor(go, lis, isSmt)); }
    public IEnumerator AnimCor(GameObject go, List<Fo> lis, bool isSmt = true) {
        List<string> paths = new List<string>();
        List<List<Fo>> objs = new List<List<Fo>>();
        int maxFrm = 0;
        for (int i = 0; i < lis.Count; i++) {
            if (lis[i].frm > maxFrm)
                maxFrm = lis[i].frm;
            if (!paths.Contains(lis[i].childs)) {
                paths.Add(lis[i].childs);
                objs.Add(new List<Fo>() { lis[i] });
            } else
                objs[paths.IndexOf(lis[i].childs)].Add(lis[i]);
        }
        List<GameObject> gos = new List<GameObject>();
        List<List<Fo>> objs2 = new List<List<Fo>>();
        for (int i = 0; i < objs.Count; i++) {
            objs[i].Sort((a, b) => a.frm.CompareTo(b.frm));
            GameObject frmGo = go.ChildGo(objs[i][0].childs);
            gos.Add(frmGo);
            Fo obj = objs[i][0].frm == 0 ? GoFo(frmGo, objs[i][0]) : GoFo(frmGo, 0, objs[i][0].childs);
            List<Fo> tmp = List(obj);
            for (int j = (objs[i][0].frm == 0).I(); j < objs[i].Count; j++)
                tmp.Add(obj = FoFo(obj, objs[i][j]));
            if (objs[i].Last().frm < maxFrm) {
                Fo last = tmp.Last();
                last.frm = maxFrm;
                tmp.Add(last);
            }
            List<Fo> tmp2 = new List<Fo>();
            System.Func<float, float, float, float> lerp, angLerp;
            if (isSmt) {
                lerp = (a, b, t) => M.Smt(a, b, t);
                angLerp = (a, b, t) => Ang.Smt(a, b, t);
            } else {
                lerp = (a, b, t) => M.Lerp(a, b, t);
                angLerp = (a, b, t) => Ang.Lerp(a, b, t);
            }
            for (int j = 1; j < tmp.Count; j++) {
                for (int k = 0, m = tmp[j].frm - tmp[j - 1].frm, n = m + (j == tmp.Count - 1).I(); k < n; k++) {
                    float f = k.F() / m;
                    tmp2.Add(new Fo(
                        tmp[j - 1].frm + k, tmp[0].childs,
                        lerp(tmp[j - 1].px, tmp[j].px, f), lerp(tmp[j - 1].py, tmp[j].py, f), lerp(tmp[j - 1].pz, tmp[j].pz, f),
                        angLerp(tmp[j - 1].rx, tmp[j].rx, f), angLerp(tmp[j - 1].ry, tmp[j].ry, f), angLerp(tmp[j - 1].rz, tmp[j].rz, f),
                        lerp(tmp[j - 1].sx, tmp[j].sx, f), lerp(tmp[j - 1].sy, tmp[j].sy, f), lerp(tmp[j - 1].sz, tmp[j].sz, f),
                        lerp(tmp[j - 1].cr, tmp[j].cr, f), lerp(tmp[j - 1].cg, tmp[j].cg, f),
                        lerp(tmp[j - 1].cb, tmp[j].cb, f), lerp(tmp[j - 1].ca, tmp[j].ca, f)
                    ));
                }
            }
            objs2.Add(tmp2);
        }
        for (int i = 0; i <= maxFrm; i++) {
            for (int j = 0; j < gos.Count; j++) {
                gos[j].Tlp(objs2[j][i].px, objs2[j][i].py, objs2[j][i].pz);
                gos[j].Tle(objs2[j][i].rx, objs2[j][i].ry, objs2[j][i].rz);
                gos[j].Tls(objs2[j][i].sx, objs2[j][i].sy, objs2[j][i].sz);
                gos[j].SetCol(C.Rgba(objs2[j][i].cr, objs2[j][i].cg, objs2[j][i].cb, objs2[j][i].ca));
            }
            yield return Wf.f1;
        }
    }
    Fo GoFo(GameObject a, int frm = 0, string path = "") { return new Fo(frm, path, a.Tlp().x, a.Tlp().y, a.Tlp().z, a.Tle().x, a.Tle().y, a.Tle().z, a.Tls().x, a.Tls().y, a.Tls().z, a.GetCol().r, a.GetCol().g, a.GetCol().b, a.GetCol().a); }
    Fo GoFo(GameObject a, Fo b) { return new Fo(b.frm, b.childs, b.px > 9999 ? a.Tlp().x : b.px, b.py > 9999 ? a.Tlp().y : b.py, b.pz > 9999 ? a.Tlp().z : b.pz, b.rx > 9999 ? a.Tle().x : b.rx, b.ry > 9999 ? a.Tle().y : b.ry, b.rz > 9999 ? a.Tle().z : b.rz, b.sx > 9999 ? a.Tls().x : b.sx, b.sy > 9999 ? a.Tls().y : b.sy, b.sz > 9999 ? a.Tls().z : b.sz, b.cr > 9999 ? a.GetCol().r : b.cr, b.cg > 9999 ? a.GetCol().g : b.cg, b.cb > 9999 ? a.GetCol().b : b.cb, b.ca > 9999 ? a.GetCol().a : b.ca); }
    Fo FoFo(Fo a, Fo b) { return new Fo(b.frm, b.childs, b.px > 9999 ? a.px : b.px, b.py > 9999 ? a.py : b.py, b.pz > 9999 ? a.pz : b.pz, b.rx > 9999 ? a.rx : b.rx, b.ry > 9999 ? a.ry : b.ry, b.rz > 9999 ? a.rz : b.rz, b.sx > 9999 ? a.sx : b.sx, b.sy > 9999 ? a.sy : b.sy, b.sz > 9999 ? a.sz : b.sz, b.cr > 9999 ? a.cr : b.cr, b.cg > 9999 ? a.cg : b.cg, b.cb > 9999 ? a.cb : b.cb, b.ca > 9999 ? a.ca : b.ca); }
}