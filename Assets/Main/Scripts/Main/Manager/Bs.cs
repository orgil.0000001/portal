﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Bs : Singleton<Bs> { // Bot Spawner
    public int n = 0;
    public List<Character> characters;
    Bot botPf;
    void Awake() {
        botPf = A.Load<Bot>("Main/Bot");
        characters.Add(A.FOsOT<Character>());
        CreateBots(n);
    }
    void Start() { }
    void Update() { }
    public void CreateBots(int n) {
        Vector3 pos = V3.O, rot = V3.O;
        for (int i = 0; i < n; i++)
            Create(pos, rot);
    }
    public void UpdatePlace() {
        for (int i = 0; i < characters.Count; i++)
            characters[i].UpdatePlace(i + 1);
    }
    public Bot Create(Vector3 pos, Vector3 rot) {
        Bot bot = Ins(botPf, pos, Q.E(rot), transform);
        characters.Add(bot);
        return bot;
    }
    public void GetCharacter() { }
}