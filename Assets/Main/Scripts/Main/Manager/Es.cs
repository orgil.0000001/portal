﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Es : Singleton<Es> { // Environment Spawner
    public void Init() {
    }
    public void ColumnsCol(Material columnMat, Material bgMat, Color columnCol, Color bgUCol, Color bgDCol, Vector2 fogY) {
        columnMat.color = columnCol;
        columnMat.SetFloat("_HeightFogStart", fogY.x);
        columnMat.SetFloat("_HeightFogEnd", fogY.y);
        columnMat.SetColor("_HeightFogColor", bgDCol);
        bgMat.color = bgUCol;
        bgMat.SetColor("_HeightFogColor", bgDCol);
    }
    public void CrtColumns(GameObject columnPf, Transform columnsTf, List<float> xs, float dx, Vector2 y, Vector2 sz, Vector2 spc, Vector2 fogY, float len) {
        for (int i = 0; i < xs.Count; i++)
            for (float z = 0; z < len;) {
                float colSz = sz.Rnd(), colX = xs[i].RndD(dx), colY = y.Rnd();
                GameObject columnGo = Ins(columnPf, V3.V(colX, (colY + fogY.x) / 2f, z + colSz), Q.O, columnsTf);
                columnGo.Tls(V3.V(colSz, colY - fogY.x, colSz));
                z += colSz + spc.Rnd();
            }
    }
    public void CrtBlds(string path, float roadW, float l) {
        List<BoxCollider> bldPfs = Resources.LoadAll<BoxCollider>(path).List();
        for (int i = 0; i < 2; i++) {
            bool isR = i == 1;
            List<GameObject> lis = new List<GameObject>();
            for (int j = 0; (lis.IsEmpty() ? true : lis.Last().Tp().z < l) && j < 100; j++) {
                BoxCollider bc = bldPfs.Rnd();
                float lstZ = lis.IsEmpty() ? 0 : lis.Last().Tp().z + lis.Last().BcSz().x / 2 + lis.Last().BcCen().x * isR.Sign();
                Vector3 pos = V3.V((roadW / 2 + bc.size.z / 2 + bc.center.z) * isR.Sign(), bc.size.y / 2 - bc.center.y, lstZ + bc.size.x / 2 - bc.center.x * isR.Sign());
                GameObject bldGo = Ins(bc.gameObject, pos, Q.Y(isR ? -90 : 90), tf);
                lis.Add(bldGo);
            }
        }
    }
}