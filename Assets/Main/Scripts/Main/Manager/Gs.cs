﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using UnityEngine.UI;
using System;

[System.Serializable]
public class PlayerPrefsData {
    public Data key;
    public GameObject go;
}

public class Gs : Singleton<Gs> { // Game Settings
    public List<PlayerPrefsData> datas;
    GameObject panelSettings;
    GameState tmpState;
    void Awake() {
        panelSettings = go.ChildGo(0);
        panelSettings.Hide();
    }
    int ctr = 0;
    float t = 0;
    private void Update() {
        t += Dt;
        if (t > 0.5f)
            t = ctr = 0;
        if (IsMbD && t < 0.5f && Mp.x < 200 && Mp.y < 200) {
            t = 0;
            ctr++;
            if (ctr == 5) {
                ctr = 0;
                Settings();
            }
        }
    }
    public void Clear() {
        PlayerPrefs.DeleteAll();
        datas.ForEach(x => ResetData(x.key, x.go));
    }
    public void Show() {
        Time.timeScale = 0;
        panelSettings.Show();
        tmpState = Gc.State;
        Gc.State = GameState.Settings;
        datas.ForEach(x => ShowData(x.key, x.go));
    }
    public void Save() { datas.ForEach(x => SaveData(x.key, x.go)); }
    public void Done() {
        Time.timeScale = 1;
        panelSettings.Hide();
        Gc.State = tmpState;
    }
    public void Settings() {
        if (panelSettings.activeSelf)
            Done();
        else
            Show();
    }
    void SetVecGo(GameObject go, params float[] vals) {
        for (int i = 0; i < vals.Length; i++)
            go.Child<InputField>(i).text = "" + vals[i];
    }
    void ResetData(Data key, GameObject go) {
        System.Reflection.FieldInfo f = A.Field<Gc>(Gc.I, key.tS());
        Type type = Gc.Datas[key].GetType();
        if (go.name == "ColorPicker")
            f.SetValue(Gc.I, go.Cp().Color = key.C());
        else if (go.name == "Dropdown")
            f.SetValue(Gc.I, go.Drp().value = key.I());
        else if (go.name == "InputField") {
            InputField inp = go.Inp();
            if (type.IsType<float>())
                f.SetValue(Gc.I, GetValSetInp<float>(inp, key));
            else if (type.IsType<int>())
                f.SetValue(Gc.I, GetValSetInp<int>(inp, key));
            else if (type.IsType<string>())
                f.SetValue(Gc.I, GetValSetInp<string>(inp, key));
        } else if (go.name == "Slider")
            f.SetValue(Gc.I, go.Sld().value = key.F());
        else if (go.name == "Toggle")
            f.SetValue(Gc.I, go.Tgl().isOn = key.B());
        else if (go.name == "Vector") {
            if (type.IsType<Vector2>()) {
                Vector2 v = key.V2();
                SetVecGo(go, v.x, v.y);
                f.SetValue(Gc.I, v);
            } else if (type.IsType<Vector2Int>()) {
                Vector2Int v = key.V2I();
                SetVecGo(go, v.x, v.y);
                f.SetValue(Gc.I, v);
            } else if (type.IsType<Vector3>()) {
                Vector3 v = key.V3();
                SetVecGo(go, v.x, v.y, v.z);
                f.SetValue(Gc.I, v);
            } else if (type.IsType<Vector3Int>()) {
                Vector3Int v = key.V3I();
                SetVecGo(go, v.x, v.y, v.z);
                f.SetValue(Gc.I, v);
            } else if (type.IsType<Vector4>()) {
                Vector4 v = key.V4();
                SetVecGo(go, v.x, v.y, v.z, v.w);
                f.SetValue(Gc.I, v);
            }
        }
    }
    T GetValSetInp<T>(InputField inp, Data key) {
        T val = key.Get<T>();
        inp.text = "" + val;
        return val;
    }
    void ShowData(Data key, GameObject go) {
        System.Reflection.FieldInfo f = A.Field<Gc>(Gc.I, key.tS());
        Type type = Gc.Datas[key].GetType();
        if (go.name == "ColorPicker")
            go.Cp().Color = (Color)f.GetValue(Gc.I);
        else if (go.name == "Dropdown")
            go.Drp().value = (int)f.GetValue(Gc.I);
        else if (go.name == "InputField") {
            InputField inp = go.Inp();
            if (type.IsType<int>())
                inp.text = "" + (int)f.GetValue(Gc.I);
            else if (type.IsType<float>())
                inp.text = "" + (float)f.GetValue(Gc.I);
            else if (type.IsType<string>())
                inp.text = "" + (string)f.GetValue(Gc.I);
        } else if (go.name == "Slider")
            go.Sld().value = (float)f.GetValue(Gc.I);
        else if (go.name == "Toggle")
            go.Tgl().isOn = (bool)f.GetValue(Gc.I);
        else if (go.name == "Vector") {
            if (type.IsType<Vector2>()) {
                Vector2 v = (Vector2)f.GetValue(Gc.I);
                SetVecGo(go, v.x, v.y);
            } else if (type.IsType<Vector2Int>()) {
                Vector2Int v = (Vector2Int)f.GetValue(Gc.I);
                SetVecGo(go, v.x, v.y);
            } else if (type.IsType<Vector3>()) {
                Vector3 v = (Vector3)f.GetValue(Gc.I);
                SetVecGo(go, v.x, v.y, v.z);
            } else if (type.IsType<Vector3Int>()) {
                Vector3Int v = (Vector3Int)f.GetValue(Gc.I);
                SetVecGo(go, v.x, v.y, v.z);
            } else if (type.IsType<Vector4>()) {
                Vector4 v = (Vector4)f.GetValue(Gc.I);
                SetVecGo(go, v.x, v.y, v.z, v.w);
            }
        }
    }
    float InpVal(GameObject go, int i) { return go.Child<InputField>(i).text.F(); }
    void SaveData(Data key, GameObject go) {
        System.Reflection.FieldInfo f = A.Field<Gc>(Gc.I, key.tS());
        Type type = Gc.Datas[key].GetType();
        if (go.name == "ColorPicker")
            f.SetValue(Gc.I, go.Cp().Color);
        else if (go.name == "Dropdown")
            f.SetValue(Gc.I, go.Drp().value);
        else if (go.name == "InputField") {
            string text = go.Inp().text;
            if (type.IsType<int>())
                f.SetValue(Gc.I, text.I());
            else if (type.IsType<float>())
                f.SetValue(Gc.I, text.F());
            else if (type.IsType<string>())
                f.SetValue(Gc.I, text);
        } else if (go.name == "Slider")
            f.SetValue(Gc.I, go.Sld().value);
        else if (go.name == "Toggle")
            f.SetValue(Gc.I, go.Tgl().isOn);
        else if (go.name == "Vector") {
            if (type.IsType<Vector2>())
                f.SetValue(Gc.I, new Vector2(InpVal(go, 0), InpVal(go, 1)));
            else if (type.IsType<Vector2Int>())
                f.SetValue(Gc.I, new Vector2(InpVal(go, 0), InpVal(go, 1)).V2I());
            else if (type.IsType<Vector3>())
                f.SetValue(Gc.I, new Vector3(InpVal(go, 0), InpVal(go, 1), InpVal(go, 2)));
            else if (type.IsType<Vector3Int>())
                f.SetValue(Gc.I, new Vector3(InpVal(go, 0), InpVal(go, 1), InpVal(go, 2)).V3I());
            else if (type.IsType<Vector4>())
                f.SetValue(Gc.I, new Vector4(InpVal(go, 0), InpVal(go, 1), InpVal(go, 2), InpVal(go, 3)));
        }
        key.Set(f.GetValue(Gc.I));
    }
}