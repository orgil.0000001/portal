﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

[System.Serializable]
public class Level {
    public Color bgC, floorC, cubeC;
    [TextArea]
    public string data;
};

public class Ls : Singleton<Ls> { // Level Spawner
    public GameObject lvl1Go, lvl2Go;
    public float lvlTxtDstTm = 2;
    public Pipe pipePf;
    public GameObject cubePf, diamondPf;
    public List<Color> pipeCols;
    public List<Level> levels;
    public bool useLvl = false;
    public int lvl = 1;
    int lvlCnt = 15, rndLvlMin = 1, rndLvlMax = 15;
    int Lvl => GetLvl(Gc.Level, levels.Count, 1, levels.Count);
    int LvlIdx => Lvl - 1;
    public Vector2 floorSz = V2.V(5, 5), pipeSz = V2.V(1, 0.8f);
    public float h = 10, pipeSpc = 1;
    [HideInInspector]
    public float spc;
    List<List<Pipe>> pipes = new List<List<Pipe>>();
    List<GameObject> cubeGos = new List<GameObject>();
    [HideInInspector]
    public List<GameObject> diamondGos = new List<GameObject>();
    Level l;
    Transform pipesTf, cubesTf, diamondsTf;
    public void Init() {
        if (useLvl)
            Gc.Level = lvl;
        LoadLevel();
        LeaderBoardData.SetDatas();
    }
    public void LoadLevel() {
        pipesTf = go.Child(2);
        cubesTf = go.Child(3);
        diamondsTf = go.Child(4);
        l = levels[LvlIdx];
        Camera.main.backgroundColor = l.bgC;
        GameObject floorStaGo = go.ChildGo(0), floorEndGo = go.ChildGo(1);
        pipeCols.ForEach(x => pipes.Add(new List<Pipe>()));
        Vector3 flrSz = floorSz.Xz().Y(h);
        string[] arr = l.data.Split('\n');
        Vector2Int n = V2I.Y(arr.Length);
        List<List<int>> cols = new List<List<int>>();
        for (int i = 0; i < arr.Length; i++) {
            List<int> col = arr[i].RgxSplitSpc().Parse(x => int.Parse(x)).List();
            if (col.Count > n.x)
                n.x = col.Count;
            cols.Insert(0, col);
        }
        floorStaGo.Tls(flrSz);
        floorStaGo.Tp(V3.V(pipeSz.x - floorSz.x, -h, pipeSz.x - floorSz.y) / 2);
        UpdCol(floorStaGo, l.floorC);
        floorEndGo.Tls(flrSz);
        spc = pipeSz.x + pipeSpc;
        floorEndGo.Tp(V3.Xz(n.x + 1, n.y - 1) * spc + V3.V(floorSz.x - pipeSz.x, -h, floorSz.y - pipeSz.x) / 2);
        UpdCol(floorEndGo, l.floorC);
        Vector3 min = V3.posInf, max = V3.negInf;
        for (int i = 0; i < cols.Count; i++)
            for (int j = 0; j < cols[i].Count; j++)
                if (cols[i][j] != 0) {
                    Vector3 pos = V3.Xz(j + 1, i) * spc;
                    min = V3.Min(min, pos);
                    max = V3.Max(max, pos);
                    pipes[M.Abs(cols[i][j]) - 1].Add(CrtPipe(pos, pipeCols[M.Abs(cols[i][j]) - 1]));
                    if (cols[i][j] < 0)
                        CrtDiamond(pos);
                }
        Cm.I.Tp = (min + max) / 2 + Cm.I.B * (max.x - min.x + spc * 3) * 2.4f;
        Vector3 cubePos = V3.Xz((min.x + max.x) / 2, max.z + floorSz.y + spc);
        Vector3 lim = V3.V((max.x - min.x) / 2 + floorSz.x * 2, spc / 2, spc * 10);
        for (int i = 0; i < 50; i++)
            CrtCube(cubePos + Rnd.Pos(lim.Nnz(), lim), V3.V(Rnd.Rng(0.6f, 1.5f) * pipeSz.x).Y(h));
    }
    public void CrtDiamond(Vector3 pos) {
        diamondGos.Add(Ins(diamondPf, pos, Q.O, diamondsTf));
    }
    public void CrtCube(Vector3 pos, Vector3 scl) {
        GameObject cubeGo = Ins(cubePf, pos - V3.Y(scl.y / 2), Q.O, cubesTf);
        cubeGo.Tls(scl);
        cubeGos.Add(cubeGo);
        UpdCol(cubeGo, l.cubeC);
    }
    public Pipe Portal(Pipe pipe) {
        List<Pipe> lis = pipes.Find(x => x.Contains(pipe));
        return lis.IndexOf(pipe) == 0 ? lis[1] : lis[0];
    }
    public Pipe CrtPipe(Vector3 pos, Color c) {
        Pipe pipe = Ins(pipePf, pos, Q.O, pipesTf);
        UpdCol(pipe.go, c);
        Mesh mesh = null;
        Msh.Init(ref mesh, pipe.go);
        Msh.Pipe(ref mesh, 20, pipeSz.x / 2, pipeSz.y / 2, h, true, false);
        Msh.UpdTf(ref mesh, Tf.P(V3.Y(-h / 2)));
        return pipe;
    }
    public void UpdCol(GameObject go, Color c) {
        go.RenMatCol(c);
        go.RenMatCol("_FogColor", l.bgC);
    }
    int GetLvl(int lvl, int lvlCnt, int rnd1, int rnd2) {
        if (Data.LevelData.S().IsNe())
            Data.LevelData.SetList(A.ListAp(1, 1, lvlCnt));
        List<int> lis = Data.LevelData.ListI();
        if (lvl <= lis.Count) {
            return lis[lvl - 1];
        } else {
            int prvLvl = lis.Last();
            for (int i = lis.Count + 1; i <= lvl; i++) {
                prvLvl = prvLvl == rnd1 ? Rnd.RngIn(rnd1 + 1, rnd2) : prvLvl == rnd2 ? Rnd.RngIn(rnd1, rnd2 - 1) : Rnd.P((prvLvl - rnd1).F() / (rnd2 - rnd1)) ? Rnd.RngIn(rnd1, prvLvl - 1) : Rnd.RngIn(prvLvl + 1, rnd2);
                lis.Add(prvLvl);
            }
            Data.LevelData.SetList(lis);
            return prvLvl;
        }
    }
}
// ░ ▒ ▓ █ ▄ ▀ ■
// ┌ ┬ ┐ ─ ╔ ╦ ╗ ═
// ├ ┼ ┤ │ ╠ ╬ ╣ ║
// └ ┴ ┘   ╚ ╩ ╝
// ⁰¹²³⁴⁵⁶⁷⁸⁹ ⁻⁺⁼⁽⁾ ⁱⁿ superscript
// ₀₁₂₃₄₅₆₇₈₉ ₋₊₌₍₎⨧ ᵣᵤₐᵢⱼₓₑᵥₒₔ ᵪᵧᵦᵨᵩ subscript
// _ ¯ ~ ≡ ‗ ¦ ¨ ¬ · |
// ñ Ñ @ ¿ ? ¡ ! : / \ frequently-used
// á é í ó ú Á É Í Ó Ú vowels acute accent
// ä ë ï ö ü Ä Ë Ï Ö Ü vowels with diaresis
// ½ ¼ ¾ ¹ ³ ² ƒ ± × ÷ mathematical symbols
// $ £ ¥ ¢ ¤ ® © ª º ° commercial / trade symbols
// " ' ( ) [ ] { } « » quotes and parenthesis